function set_system_event() {
    system_event = [
        {
            date: "",
            name: "Переведена в Новая, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Принята в работу, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Помечена выполненой, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
              date: "",
            name: "Отклонена инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Закрыта инженером " + root.user.fio,
            alert: "",
            see: true
        },
    ]
}

var order_type = ["Техническое обслуживание",
                    "Вентиляция и кондиционирование",
                    "Система охраны и безопасности"]

var order_status = ["Новая","В работе","Выполнена","Отклонёна нами","Принята заказчиком"];