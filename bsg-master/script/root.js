var root = {
	user: {},
	options: {
		limit: 10,
		update_time_order: 300000, // 300000 - 5 минут
	},
	load_data: {
		order: false,
	},
	set_user: function(data) {
		this.user = data;
		localStorage.setItem('key', data.hash);
		order_ctrl.default_request_data.master_key = data.hash;
		order_ctrl.request_data.master_key = data.hash;
		set_system_event();
		this.app_entry();
	},
	send: function(url, data, callback) {
		$.ajax({
		  	type: "POST",
		  	url: '../server/' + url + '.php',
		  	dataType: "json",
		  	data: JSON.stringify(data), 
		  	success: callback
		});
	},
	app_entry: function() {
        this.pre_load();
	},
	out_user: function() {
		localStorage.removeItem('key');
		window.location.reload();
	},
	key: function(val) {
		var key = CryptoJS.MD5(val);
        return key.toString(CryptoJS.enc.Base64);
	},
	edit_doc: function(name, elem, callback) {
		var data = {
			name: name,
			elem: elem
		}
		this.send("edit_doc", data, callback);
	},
	delete_doc: function(name, id, callback) {
		var data = {
			name: name,
			id: id
		}
		this.send("delete_doc", data, callback);
	},
	get_collection: function(data, callback) {
		this.send("get_collection", data, callback);
	},
	pre_load: function() {
		order_ctrl.load('load');		
	},
	load: function() {
		if(this.load_data.order == true) {
			start_router();
		}
	},
	get_orders: function(data, callback) {
		this.send("get_order", data, callback);
	}, 
}