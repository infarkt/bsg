var animate = {
	init_sidebar: function() {
		$('#sidebar-exp').click(function(){		
			if ($('#wrapper').hasClass('sidebar')){
				$('#wrapper').removeClass('sidebar');
				$('#body-overlay').fadeOut('160');
			}else{
				$('#body-overlay').fadeIn('160');
				$('#wrapper').addClass('sidebar');
			}
			return false;
		});

		$("#sidebar .drop-area>li").click(function(){
			$('#wrapper').removeClass('sidebar');
			$('#body-overlay').fadeOut('160');
			/*$('#sidebar .selected').removeClass('selected');
			
			if ($('ul:visible', this).size()){
				$('ul:visible', this).slideUp('fast').removeClass('active');
			}else{
				$('#sidebar .drop-area>li>ul:visible').slideUp('fast').closest('li').removeClass('active');
				$(this).addClass('selected');
				$('ul:hidden', this).slideDown('fast').addClass('active');
			}*/
		});
		
		/*$("#sidebar .drop-area>li").click(function(){
			$('#sidebar .selected').removeClass('selected');
			
			if ($('ul:visible', this).size()){
				$('ul:visible', this).slideUp('fast').removeClass('active');
			}else{
				$('#sidebar .drop-area>li>ul:visible').slideUp('fast').closest('li').removeClass('active');
				$(this).addClass('selected');
				$('ul:hidden', this).slideDown('fast').addClass('active');
			}
		});*/

		$("#body-overlay").click(function () { 
			$('#sidebar-exp').click();
		});
	},
};;var format = {
	format_tel: function(num) {
		if(num != undefined) {
			num = num.replace(/\D/g, "");
	        if(num.length <= 11) {
	            switch(num.length) {
	                case 0:
	                    var tel = "+7";
	                        break;
	                case 1:
	                    var tel = "+7";
	                        break;
	                case 2:
	                    var tel = "+7 " + num[1];
	                        break;
	                case 3:
	                    var tel = "+7 " + num[1] + num[2];
	                        break;
	                case 4:
	                    var tel = "+7 " + num[1] + num[2] + num[3];
	                        break;
	                case 5:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4];
	                        break;    
	                case 6:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5];
	                        break;  
	                case 7:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6];
	                        break;   
	                case 8:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7];
	                        break; 
	                case 9:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8];
	                        break; 
	                case 10:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9];
	                        break;
	                case 11:
	                	var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9] + num[10];
	                        break;
	            }
	        } else {
	           var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9] + num[10];
	        }
	        return tel;
    	}
	},
	sum: function(val) {
		return val.replace(/\D/g, "");
	},
    eng: function(val) {
        return val.replace(/\W/g, "");
    },
	date_ms: function() {
		var date = new Date();
		var date_ms = date.getTime();
		return date_ms;
	},
	start_date_ms: function() {
		var date = new Date();
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		date.setMilliseconds(0);
		var date_ms = date.getTime();
		return date_ms;
	},
	stop_date_ms: function() {
		var date = new Date();
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		date.setMilliseconds(0);
		var date_ms = date.getTime();
		return date_ms += 86400000;
	},
	mouth_text_to_num: function(mouth_str) {
		var mouth = 0;
		if (mouth_str == "Января"){mouth = 0}
        if (mouth_str == "Февраля"){mouth = 1}
        if (mouth_str == "Марта"){mouth = 2}
        if (mouth_str == "Апреля"){mouth = 3}
        if (mouth_str == "Мая"){mouth = 4}
        if (mouth_str == "Июня"){mouth = 5}
        if (mouth_str == "Июля"){mouth = 6}
        if (mouth_str == "Августа"){mouth = 7}
        if (mouth_str == "Сентября"){mouth = 8}
        if (mouth_str == "Октября"){mouth = 9}
        if (mouth_str == "Ноября"){mouth = 10}
        if (mouth_str == "Декабря"){mouth = 11}
        return mouth;
    },
    mouth_num_to_text: function(mouth) {
		var mouth_str = '';
	    if (mouth == 0){mouth_str = "Январь"}
        if (mouth == 1){mouth_str = "Февраль"}
        if (mouth == 2){mouth_str = "Март"}
        if (mouth == 3){mouth_str = "Апрель"}
        if (mouth == 4){mouth_str = "Май"}
        if (mouth == 5){mouth_str = "Июнь"}
        if (mouth == 6){mouth_str = "Июль"}
        if (mouth == 7){mouth_str = "Август"}
        if (mouth == 8){mouth_str = "Сентябрь"}
        if (mouth == 9){mouth_str = "Октябрь"}
        if (mouth == 10){mouth_str = "Ноябрь"}
        if (mouth == 11){mouth_str = "Декабрь"} 
        return mouth_str;
    },
	get_year_month: function() {
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth();
		return {year: year, month: month};
	},
	ms_to_date: function(date) {
        if (date == "0" || date == "" || date == null) {
            return "";
        } else {
            var ms_date = new Date();
            ms_date.setTime(date);
            var minutes = ms_date.getMinutes().toString();
            if(minutes < 10) {
            	minutes = 0 + minutes;
            }
            var hours = ms_date.getHours().toString();
            if(hours < 10) {
            	hours = 0 + hours;
            }
            var date = ms_date.getDate();
            var mouth = ms_date.getMonth();
            var mouth_str = "";
                if (mouth == 0){mouth_str = "Января"}
                if (mouth == 1){mouth_str = "Февраля"}
                if (mouth == 2){mouth_str = "Марта"}
                if (mouth == 3){mouth_str = "Апреля"}
                if (mouth == 4){mouth_str = "Мая"}
                if (mouth == 5){mouth_str = "Июня"}
                if (mouth == 6){mouth_str = "Июля"}
                if (mouth == 7){mouth_str = "Августа"}
                if (mouth == 8){mouth_str = "Сентября"}
                if (mouth == 9){mouth_str = "Октября"}
                if (mouth == 10){mouth_str = "Ноября"}
                if (mouth == 11){mouth_str = "Декабря"} 
            var full_year = ms_date.getFullYear().toString();
        	var year = full_year.substring(2);
            return [minutes, hours, date, mouth_str, year];
        }
    },
    format_text_date: function(data) {
        var date = data.replace(/\D/g, "");
        if(date.length <= 7) {
            switch(date.length) {
                case 0:
                    date = "";
                        break;
                case 1:
                    date = date[0];
                        break;
                case 2:
                    date = date[0] + date[1];
                        break;
                case 3:
                    date = date[0] + date[1] + "/" + date[2];
                        break;
                case 4:
                    date = date[0] + date[1] + "/" + date[2] + date[3];
                        break;
                case 5:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4];
                        break;
                case 6:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5];
                        break;
                case 7:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6];
                        break;
                case 7:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6] + date[7];
                        break;
            }   
        } else {
            date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6] + date[7];
        }            
        return date;
    },
    sql_to_date_time: function(date) {
        if (date == "0" || date == "" || date == null) {
            return "";
        } else {
            var ms_date = new Date();
            ms_date.setTime(date);
            var date = ms_date.getDate();
            var mouth = ms_date.getMonth();
            var mouth_str = "";
                if (mouth == 0){mouth_str = "Января"}
                if (mouth == 1){mouth_str = "Февраля"}
                if (mouth == 2){mouth_str = "Марта"}
                if (mouth == 3){mouth_str = "Апреля"}
                if (mouth == 4){mouth_str = "Мая"}
                if (mouth == 5){mouth_str = "Июня"}
                if (mouth == 6){mouth_str = "Июля"}
                if (mouth == 7){mouth_str = "Августа"}
                if (mouth == 8){mouth_str = "Сентября"}
                if (mouth == 10){mouth_str = "Ноября"}
                if (mouth == 9){mouth_str = "Октября"}
                if (mouth == 11){mouth_str = "Декабря"} 
            var year = ms_date.getFullYear();
            var hours = ms_date.getHours();
            if (hours < 10) { 
                hours = "0" + hours; 
            }
            var minutes = ms_date.getMinutes();
            if (minutes < 10) { 
                minutes = "0" + minutes; 
            }
            return(hours +":"+ minutes +" "+ date +" "+ mouth_str +" "+ year);
        }
    }
}

;function set_system_event() {
    system_event = [
        {
            date: "",
            name: "Переведена в Новая, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Принята в работу, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Помечена выполненой, инженером " + root.user.fio,
            alert: "",
            see: true
        },{
              date: "",
            name: "Отклонена инженером " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Закрыта инженером " + root.user.fio,
            alert: "",
            see: true
        },
    ]
}

var order_type = ["Техническое обслуживание",
                    "Вентиляция и кондиционирование",
                    "Система охраны и безопасности"]

var order_status = ["Новая","В работе","Выполнена","Отклонёна нами","Принята заказчиком"];;var root = {
	user: {},
	options: {
		limit: 10,
		update_time_order: 300000, // 300000 - 5 минут
	},
	load_data: {
		order: false,
	},
	set_user: function(data) {
		this.user = data;
		localStorage.setItem('key', data.hash);
		order_ctrl.default_request_data.master_key = data.hash;
		order_ctrl.request_data.master_key = data.hash;
		set_system_event();
		this.app_entry();
	},
	send: function(url, data, callback) {
		$.ajax({
		  	type: "POST",
		  	url: '../server/' + url + '.php',
		  	dataType: "json",
		  	data: JSON.stringify(data), 
		  	success: callback
		});
	},
	app_entry: function() {
        this.pre_load();
	},
	out_user: function() {
		localStorage.removeItem('key');
		window.location.reload();
	},
	key: function(val) {
		var key = CryptoJS.MD5(val);
        return key.toString(CryptoJS.enc.Base64);
	},
	edit_doc: function(name, elem, callback) {
		var data = {
			name: name,
			elem: elem
		}
		this.send("edit_doc", data, callback);
	},
	delete_doc: function(name, id, callback) {
		var data = {
			name: name,
			id: id
		}
		this.send("delete_doc", data, callback);
	},
	get_collection: function(data, callback) {
		this.send("get_collection", data, callback);
	},
	pre_load: function() {
		order_ctrl.load('load');		
	},
	load: function() {
		if(this.load_data.order == true) {
			start_router();
		}
	},
	get_orders: function(data, callback) {
		this.send("get_order", data, callback);
	}, 
};var select = {
	role: ["Администратор", "Инженер"],
}