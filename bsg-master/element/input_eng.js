// Поле только английский
InputEng = React.createClass({
    change: function(event) {
        event.target.value = format.eng(event.target.value);
        this.props.bind(event);
    },
    render: function() {
        return (
            <div className="input-f-wrapper">
                <label>{this.props.head}</label>
                <input type="text" 
                        name={this.props.name}
                        onChange={this.change} 
                        value={this.props.val}/>
            </div>
        );
    }
});

/*<InputEng
    head="Дата заключения договора"
    name="contract_date"
    val={this.state.data.contract_date}
    bind={this.inputBind}/>

    */