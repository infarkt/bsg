Auth = React.createClass({displayName: "Auth",
    getInitialState: function() {
        return {error_view: "none",
                login: "",
                pass: ""};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    set_view_error: function(val) {
        this.setState({error_view: val});
    },
    login: function() {
        var _set_view_error = this.set_view_error;
        root.send("login", this.state, function(result) {
            if(result != 0) {
                _set_view_error('none');
                root.set_user(JSON.parse(result.doc));
            } else {
                _set_view_error('block');
            }
        });
    },
    render: function() {
        return (
            React.createElement("div", {id: "login"}, 
                React.createElement("div", {className: "logo"}, 
                    React.createElement("a", {href: "index.html"}, 
                        React.createElement("img", {alt: "", src: "../styles/images/logo-crm.png"})
                    )
                ), 
                React.createElement("div", {className: "wrapper-login"}, 
                    React.createElement("input", {className: "user", 
                            type: "text", 
                            name: "login", 
                            onChange: this.inputBind, 
                            value: this.state.login}), 
                    React.createElement("input", {className: "password", 
                            type: "password", 
                            name: "pass", 
                            onChange: this.inputBind, 
                            value: this.state.pass})
                ), 
                React.createElement("div", {className: "message", style: {display: this.state.error_view}}, React.createElement("h4", null, "Ошибка!")), 
                React.createElement("a", {className: "login-button", onClick: this.login}, "Вход"), 
                React.createElement("span", null, 
                    React.createElement("a", {href: "#"})
                )
            )
        );
    } 
});
Menu = React.createClass({displayName: "Menu",
    getInitialState: function() {
    return  {
                class_menu: {Заявки:'active item'},
                user: root.user.login
            };
    },
    setClass: function(event) {
        this.state.class_menu = {Заявки:'item'}
        this.state.class_menu[event.target.text] = "active item";
        this.forceUpdate();
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("nav", {className: "navbar navbar-default", role: "navigation"}, 
                    React.createElement("div", {className: "navbar-header"}, 
                        React.createElement("a", {className: "navbar-brand"}, 
                            React.createElement("img", {alt: "", src: "../styles/images/logo.png"}), 
                            React.createElement("span", {id: "sidebar-exp"})
                        )
                    ), 
                    React.createElement("div", {className: "collapse navbar-collapse navbar-ex1-collapse"}, 
                        React.createElement("ul", {className: "nav navbar-nav navbar-right header-drop-right"}, 
                            React.createElement("li", {className: "dropdown user-dropdown-one"}, 
                                React.createElement("a", {className: "dropdown-toggle", "data-toggle": "dropdown", href: "#"}, 
                                    this.state.user, 
                                    React.createElement("img", {className: "img-user", alt: "", src: "../styles/images/user-img.png"}), 
                                    React.createElement("b", {className: "caret"})
                                ), 
                                React.createElement("ul", {className: "dropdown-menu user-dropdown-two"}, 
                                    React.createElement("li", {onClick: this.logOut, className: "log-out"}, 
                                        React.createElement("a", null, "Выход")
                                    )
                                )
                            )
                        )
                    )
                ), 
                React.createElement("div", {id: "wrapper", className: ""}, 
                    React.createElement("div", {id: "body-overlay"}), 
                    React.createElement("div", {id: "sidebar"}, 
                        React.createElement("ul", {className: "drop-area"}, 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/order"}, "Заявки")
                            )
                        )
                    ), 
                    React.createElement("div", {id: "wrap"}, 
                        React.createElement("div", {className: "container"}, 
                        
                        React.createElement(RouteHandler, null)
                        )
                    )
                ), 
                React.createElement("div", {id: "footer"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-6 left"}, 
                            React.createElement("ul", null, 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Раз"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: ""}, "Два"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Три")
                                )
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6 right"}, 
                            React.createElement("span", {className: "pull-right"}, 
                                "Разработка", 
                                React.createElement("a", {href: "ordo-crm.ru"}, 
                                    React.createElement("strong", null, " Ordo-CRM ")
                                ), 
                                "2015"
                            )
                        )
                    )
                )
            )
        );
    }
});   


/*
    <ul className="header-notifications pull-right">
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>
*/

/*
Суб меню
<li class="">
UI Elements
<ul style="display: none;" class="">
<li>
<span class="icn icon-190"></span>
<a href="ui-sliders.html">UI Sliders</a>
</li>

*/ 

/*
поиск
<div class="search-block clearfix">
<span></span>
<input type="text" placeholder="Are you looking for something?">
</div>

*/


/*<ul className="header-notifications pull-right">
                            <li>
                                <a className="clock" href="#"></a>
                            </li>
                            <li>
                                <a className="task" href="#"></a>
                            </li>
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>*/

/*
<div id="main" className="z-depth-1">
              <div id="header">
              </div>
              <div className="ui inverted db1 menu">
                <a onClick={this.setClass} className={this.state.class_menu["Заявки"]} href="#/order">Заявки</a>
                <a onClick={this.setClass} className={this.state.class_menu["Отчёты"]}>Отчёты</a>
                <a onClick={this.setClass} className={this.state.class_menu["Справочники"]} href="#/lists">Справочники</a>
                <div className="right menu">
                    <div className="item blue">{this.state.user}</div>
                    <div onClick={this.logOut} 
                        className="item piont"><i className="power icon orange"></i></div>
                </div>
              </div>
              {}
              <RouteHandler/>
            </div>
*/
Order = React.createClass({displayName: "Order",
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(OrderMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        React.createElement(OrderList, null)
                    )
                )
            )    
        )
    }
});



OrderMenu = React.createClass({displayName: "OrderMenu",
    getInitialState: function() {
        return {filter: "all",
                new_order: order_ctrl.data_count.new_order_count};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        order_ctrl.update(val);
    },
    setCount: function(val) {
        this.setState({new_order: val});
    },
    componentDidMount: function() {
        order_ctrl.setNewCount = this.setCount;
    },
    render: function() {
        var self = this;
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "ФИЛЬТР"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "new"), 
                            className: this.state.filter == "new" ? "new filter_active" : "new"}, 
                            React.createElement("span", null, "Новые"), React.createElement("span", {className: "count"}, this.state.new_order), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "job"), 
                            className: this.state.filter == "job" ? "car filter_active" : "car"}, 
                            React.createElement("span", null, "В работе"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "ок"), 
                            className: this.state.filter == "ок" ? "ckeck filter_active" : "ckeck"}, 
                            React.createElement("span", null, "Выполненые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "reject"), 
                            className: this.state.filter == "reject" ? "attention filter_active" : "attention"}, 
                            React.createElement("span", null, "Отклонённые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "close"), 
                            className: this.state.filter == "close" ? "close_0 filter_active" : "close_0"}, 
                            React.createElement("span", null, "Закрытые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "all"), 
                            className: this.state.filter == "all" ? "all filter_active" : "all"}, 
                            React.createElement("span", null, "Все"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});


OrderList = React.createClass({displayName: "OrderList",
    getInitialState: function() {
        return {data: order_ctrl.store,
                request: jQuery.extend(true, {}, order_ctrl.default_request_data),
                display: jQuery.extend(true, {}, order_ctrl.display_step),
                order_status: order_status};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        order_ctrl.update("change_limit");
    },
    stepPrev: function() {
        order_ctrl.update("step_prev");
    },
    stepNext: function() {
        order_ctrl.update("step_next");
    },
    add: function() {
        
    },
    searchBind: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
        order_ctrl.update("search");
    },
    edit: function(elem) {
        
    },
    componentDidMount: function() {
        order_ctrl.setViewList = this.update;
        order_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Заявки ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", href: "#/new_order", onClick: this.add}, "Добавить")
                ), 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-6"}, 
                        React.createElement("div", {className: "input-f-wrapper"}, 
                            React.createElement("div", {className: "wrap-company-input"}, 
                                React.createElement("input", {type: "text", 
                                        name: "company", 
                                        placeholder: "Поиск по компании", 
                                        onChange: this.searchBind, 
                                        value: this.state.request.company}), 
                                React.createElement("a", null)
                            )
                        )
                    )
                ), 
                this.state.data.data.map(function(elem) {
                    return (
                        React.createElement(OrderElem, {key: elem.id, data: elem})
                    )
                }), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "Вперёд")
                    )
                )
            ) 
        )
    }
});



OrderElem = React.createClass({displayName: "OrderElem",
    getInitialState: function() {
        return {data: this.props.data,
                saveView: "none"};
    },
    bindStep: function(val) {
        this.state.data.doc.primary.status = val;
        // Системная история
        system_event[val].date = format.date_ms();
        this.state.data.doc.system_history.push(system_event[val]);
        // Уведомления
        switch(val) {
            case 3:
                root.send_mail(this.state.data.doc.primary.mail, "Ваша заявка №" + this.state.data.doc.system.key + " отклонена!");
                    break;
        }
        this.saveView();
    },
    saveView: function() {
        this.state.saveView = "";
        this.forceUpdate();
    },
    noteBind: function(event) {
        this.state.data.doc.master.master_note = event.target.value;
        this.saveView();
    },
    elemEdit: function() {
        order_ctrl.edit_order(this.state.data, function(data){
            if(data == 1) {
                order_ctrl.update('edit');
            }
        });
        this.setState({saveView: "none"});
    },
    setElem: function(elem) {
        this.setState({data: elem});
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var doc = this.state.data.doc, self = this;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "timeline-post"}, 
                    React.createElement("ul", null, 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 0), 
                            className: doc.primary.status == 0 ? "orange_active" : ""}, React.createElement("a", null, "Новая")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 1), 
                            className: doc.primary.status == 1 ? "green_active" : ""}, React.createElement("a", null, "В работе")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 2), 
                            className: doc.primary.status == 2 ? "blue_active" : ""}, React.createElement("a", null, "Выполнена")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 3), 
                            className: doc.primary.status == 3 ? "red_active" : ""}, React.createElement("a", null, "Отклонёна")), 
                        React.createElement("li", {className: doc.primary.status == 4 ? "blue_active" : ""}, React.createElement("a", null, "Закрыта")), 
                        React.createElement("li", {className: "right m"}, React.createElement("a", {className: "white_t"}, format.sql_to_date_time(doc.system.add_date)))
                    ), 
                    React.createElement("div", {className: "info-block"}, 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view"}, doc.system.key), 
                            React.createElement("div", {className: "text_view"}, doc.primary.company), 
                            React.createElement("div", {className: "text_view"}, doc.primary.persone), 
                            React.createElement("div", {className: "text_view green_h_t"}, doc.primary.phone)
                        ), 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view"}, doc.primary.type), 
                            React.createElement("div", {className: "text_view"}, doc.primary.address), 
                            React.createElement("div", {className: "text_view orange_h_t"}, doc.primary.reason)
                        ), 
                        React.createElement("div", {className: "textarea-i-wrapper"}, 
                            React.createElement("textarea", {name: "nrd_note", 
                                onChange: this.noteBind, 
                                value: doc.master.master_note})
                        )
                    ), 
                    React.createElement("div", {className: "options clearfix"}, 
                        React.createElement("ul", null, 
                            React.createElement("li", null, React.createElement("a", {className: "info"}))
                        ), 
                        React.createElement("a", {style: {display: this.state.saveView}, 
                            onClick: this.elemEdit}, "Сохранить изменения!")
                    )
                )
            )
        )
    }
});

var Router = ReactRouter; // or var Router = ReactRouter; in browsers
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

AppMain = React.createClass({displayName: "AppMain",
    render: function() {
        return (
            React.createElement(Menu, null)
        )
    }
});

var routes = (
  React.createElement(Route, {name: "app", path: "/", handler: AppMain}, 
    React.createElement(Route, {name: "order", path: "/order", handler: Order}), 
    React.createElement(DefaultRoute, {handler: Order})
  )
);

$(document).ready(function () {
    start();
});

function start() {
    see_cookie();
}

function auth() {
    React.render(React.createElement(Auth, null), document.body);
}

function see_cookie() {
    var key = localStorage.getItem('key');
    if(key == null) {
        auth(); 
    } else {
        root.send("auto_login", {"hash": key}, function(result) {
            if(result != 0) {
                root.set_user(JSON.parse(result.doc));
            } else {
                auth();
            }
        });
    }
}

function start_router() {
    Router.run(routes, function (Handler) {
        React.render(React.createElement(Handler, null), document.body);
    });
}