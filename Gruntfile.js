module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Парсим
    react: {
        combo: {
            files: {
                'element/all_element.js': ['element/input_check_group.js', 
                                                'element/input_eng.js', 
                                                'element/input_kladr.js',
                                                'element/input_pass_icon.js', 
                                                'element/input_phone.js', 
                                                'element/input_select.js', 
                                                'element/input_sum.js', 
                                                'element/input_text_date.js', 
                                                'element/input_text_icon.js', 
                                                'element/next_reminder_date.js'],
                'bsg-order/view/all_view.js': ['bsg-order/view/auth.js',
                                            'bsg-order/view/menu.js',
                                            'bsg-order/view/new_order.js',
                                            'bsg-order/view/order.js',
                                            'bsg-order/view/users.js',   
                                            'bsg-order/ctrl/router.js'],       
                'bsg-main/view/all_view.js': ['bsg-main/view/auth.js',
                                            'bsg-main/view/company.js',
                                            'bsg-main/view/company_menu.js',
                                            'bsg-main/view/guide.js',
                                            'bsg-main/view/lists.js',   
                                            'bsg-main/view/menu.js',  
                                            'bsg-main/view/sourse.js',   
                                            'bsg-main/view/users.js', 
                                            'bsg-main/view/new_order.js', 
                                            'bsg-main/view/all_order.js', 
                                            'bsg-main/controller/router.js'],     
                'bsg-master/view/all_view.js': ['bsg-master/view/auth.js',
                                            'bsg-master/view/menu.js',
                                            'bsg-master/view/order.js',  
                                            'bsg-master/ctrl/router.js'],                 
            }
        },
    },

    // Соединяем
    concat: {
        options: {
            separator: ';',
        },
        lib: {
            files: {
                'lib/bsg_order_lib.js': ['lib/jquery-2.1.3.js',
                                            'lib/md5.js',
                                            'lib/react_0.12.2.js',
                                            'lib/react-router.js',
                                            'lib/underscore_1.6.0.js',
                                            'lib/bootstrap.min.js'],
                'lib/bsg_main_lib.js': ['lib/jquery-2.1.3.js',
                                            'lib/md5.js',
                                            'lib/react_0.12.2.js',
                                            'lib/react-router.js',
                                            'lib/underscore_1.6.0.js',
                                            'lib/bootstrap.min.js'],
                'lib/bsg_master_lib.js': ['lib/jquery-2.1.3.js',
                                            'lib/md5.js',
                                            'lib/react_0.12.2.js',
                                            'lib/react-router.js',
                                            'lib/underscore_1.6.0.js',
                                            'lib/bootstrap.min.js'],
                'script/all_app_script.js': ['script/animate.js',
                                            'script/format.js'],
                'bsg-order/ctrl/all_ctrl.js': ['bsg-order/ctrl/new_order_ctrl.js', 
                                                    'bsg-order/ctrl/order_ctrl.js'],
                'bsg-main/controller/all_ctrl.js': ['bsg-main/controller/main_company_controller.js', 
                                                    'bsg-main/controller/user_controller.js',
                                                    'bsg-main/controller/new_order_ctrl.js',
                                                    'bsg-main/controller/all_order_ctrl.js'],
                'bsg-master/ctrl/all_ctrl.js': ['bsg-master/ctrl/order_ctrl.js'],
                'bsg-order/script/all_script.js': ['bsg-order/script/animate.js', 
                                                    'bsg-order/script/format.js',
                                                    'bsg-order/script/model.js',
                                                    'bsg-order/script/root.js',
                                                    'bsg-order/script/select.js'],
                'bsg-master/script/all_script.js': ['bsg-master/script/animate.js', 
                                                    'bsg-master/script/format.js',
                                                    'bsg-master/script/model.js',
                                                    'bsg-master/script/root.js',
                                                    'bsg-master/script/select.js'],
                'bsg-main/script/all_script.js': ['bsg-main/script/animate.js', 
                                                    'bsg-main/script/format.js',
                                                    'bsg-main/script/model.js',
                                                    'bsg-main/script/root.js',
                                                    'bsg-main/script/select.js'],
                'bsg-order/bsg-order.js': ['lib/bsg_order_lib.js', 
                                            'element/all_element.js',
                                            'script/all_app_script.js', 
                                            'bsg-order/script/all_script.js',
                                            'bsg-order/view/all_view.js',
                                            'bsg-order/ctrl/all_ctrl.js'], 
                'bsg-main/bsg-main.js': ['lib/bsg_main_lib.js', 
                                            'element/all_element.js',
                                            'script/all_app_script.js', 
                                            'bsg-main/script/all_script.js',
                                            'bsg-main/view/all_view.js',
                                            'bsg-main/controller/all_ctrl.js'], 
                'bsg-master/bsg-master.js': ['lib/bsg_master_lib.js', 
                                            'element/all_element.js',
                                            'script/all_app_script.js', 
                                            'bsg-master/script/all_script.js',
                                            'bsg-master/view/all_view.js',
                                            'bsg-master/ctrl/all_ctrl.js'], 
            },
        },
    },
    cssmin: {
        options: {
            shorthandCompacting: false,
            roundingPrecision: -1
        },
        target: {
            files: {
                'bsg-order/styles/all_styles.css': ['bsg-order/styles/bootstrap.css',
                                            'bsg-order/styles/prettycheckable.css',
                                            'bsg-order/styles/style.css'],  
                'bsg-main/styles/all_styles.css': ['bsg-main/styles/bootstrap.css',
                                            'bsg-main/styles/prettycheckable.css',
                                            'bsg-main/styles/style.css'], 
                'bsg-master/styles/all_styles.css': ['bsg-master/styles/bootstrap.css',
                                            'bsg-master/styles/prettycheckable.css',
                                            'bsg-master/styles/style.css'], 
            }
        }
    },
    // Жмём
    uglify: {
        options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
        },
        my_target: {
            files: {
                'bsg-order/bsg-order.min.js': ['bsg-order/bsg-order.js'],
                'bsg-main/bsg-main.min.js': ['bsg-main/bsg-main.js'],
                'bsg-master/bsg-master.min.js': ['bsg-master/bsg-master.js'],
            }
        }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-react');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task(s).
  grunt.registerTask('default', ['react','concat', 'cssmin', 'uglify']);

};

/*

files: {
          'bsg/output1.min.js': ['js/bsg-online.js'],
          'build/output2.min.js': ['js/132.js']
        }

*/