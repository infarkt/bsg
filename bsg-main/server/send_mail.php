<?php
    $data = json_decode(file_get_contents('php://input'), true);
    $mail = $data["mail"];
    $content = $data["content"];

    // Отправка письма
    $bug_note_utf8 = urldecode($content);
    $bug_note = iconv("UTF-8", "WINDOWS-1251", $bug_note_utf8);

    $dt=date("d F Y, H:i:s");
    $from = "info@bsg-online.ru";
    $subj = "Системное сообщение: $dt";
    $what = "$bug_note";
    $header="From: $from;";
    
    mail($mail, $subj, $what, $header);
?>    