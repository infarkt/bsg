<?php
header('content-encoding: gzip');
require "base.php";

$data = json_decode(file_get_contents('php://input'), true);

$by = $data["by"];
$limit = $data["limit"];
$offset = $data["offset"];
$name = $data["name"];
$address = $data["address"];
$step = $data["step"];

$dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

$query_count = "SELECT * FROM company";
$result_count = pg_query($query_count); 
$result_query["count"] = pg_affected_rows($result_count);

$query_new_count = "SELECT * FROM company WHERE (doc->'status'->>'step') = '0'";
$result_new_count = pg_query($query_new_count); 
if(pg_affected_rows($result_new_count) != 0) {
	$result_query["new_count"] = pg_affected_rows($result_new_count);
} else {
	$result_query["new_count"] = 0;
};

$query = "SELECT * FROM company WHERE (doc->'primary'->>'name') ILIKE '%$name%'  
								AND (doc->>'object') ILIKE '%$address%'
								AND (doc->'status'->>'step') IN $step
								ORDER BY (doc->'primary'->>'$by') ASC LIMIT $limit OFFSET $offset";
$result = pg_query($query); 
echo pg_last_error();
if(pg_affected_rows($result) != 0) {
	$result_query["data"] = pg_fetch_all($result);
} else {
	$result_query["data"] = [];
};
echo pg_last_error();
pg_close($dbconn);

$result_json = json_encode($result_query);
$result_zip = gzencode($result_json, $zip_compress);
echo $result_zip;
?>