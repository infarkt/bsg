<?php
header('content-encoding: gzip');

$data = json_decode(file_get_contents('php://input'), true);
$host = $data["server"]["host"];
$dbname = $data["server"]["dbname"];
$user = $data["server"]["user"];
$password = $data["server"]["password"];

$name = $data["val"]["name"];
$by = $data["val"]["by"];
$limit = $data["val"]["limit"];
$offset = $data["val"]["offset"];

$dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

$query_count = "SELECT * FROM $name";
$result_count = pg_query($query_count); 
$result_query["count"] = pg_affected_rows($result_count);

$query = "SELECT * FROM $name ORDER BY (doc->>'$by') ASC LIMIT $limit OFFSET $offset";
$result = pg_query($query); 
echo pg_last_error();
if(pg_affected_rows($result) != 0) {
	$result_query["data"] = pg_fetch_all($result);
} else {
	$result_query = 0;
};

pg_close($dbconn);

$result_json = json_encode($result_query);
$result_zip = gzencode($result_json, $zip_compress);
echo $result_zip;
?>