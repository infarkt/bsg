<?php
header('content-encoding: gzip');
require "base.php";

$data = json_decode(file_get_contents('php://input'), true);

$hash = $data["hash"];

$dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

$query = "SELECT * FROM people WHERE (doc->>'hash') = '$hash' AND (doc->>'role') = 'Администратор'";
$result = pg_query($query); 

if(pg_affected_rows($result) == 1) {
	$result_query = pg_fetch_object($result);
} else {
	$result_query = 0;
};

pg_close($dbconn);

$result_json = json_encode($result_query);
$result_zip = gzencode($result_json, $zip_compress);
echo $result_zip;
?>