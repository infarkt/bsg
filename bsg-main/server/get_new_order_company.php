<?php
header('content-encoding: gzip');
require "base.php";

$data = json_decode(file_get_contents('php://input'), true);

$search = $data["search"];

$dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

$query = "SELECT * FROM company WHERE (doc->'primary'->>'name') ILIKE '%$search%'
								ORDER BY (doc->'primary'->>'name') ASC LIMIT 10";
$result = pg_query($query); 
echo pg_last_error();
if(pg_affected_rows($result) != 0) {
	$result_query = pg_fetch_all($result);
} else {
	$result_query = [];
};
echo pg_last_error();
pg_close($dbconn);

$result_json = json_encode($result_query);
$result_zip = gzencode($result_json, $zip_compress);
echo $result_zip;
?>