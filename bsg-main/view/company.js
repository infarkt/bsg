Company = React.createClass({
    componentDidMount: function() {

    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-3">
                        <MainCompanyMenu/>
                    </div>
                    <div className="col-md-9">
                        <MainCompanyForm/>
                        <MainCompanyList/>
                        <MainCompanyInfo/>
                        <MainCompanyObject/>
                        <MainCompanyAddress/>
                        <MainCompanyUsers/>
                    </div>
                </div>
            </div>    
        )
    }
});


MainCompanyMenu = React.createClass({
    getInitialState: function() {
        return {filter: "job",
                new_count: main_company_ctrl.data_count.news};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_company_ctrl.update(val);
    },
    setCount: function(val) {
        this.setState({new_count: val});
    },
    componentDidMount: function() {
        main_company_ctrl.setNewCount = this.setCount;
        main_company_ctrl.setFilter = this.setFilter;
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "new")} 
                            className={this.state.filter == "new" ? "new filter_active" : "new"}>
                            <span>Новые</span><span className="count">{this.state.new_count}</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "job")}
                            className={this.state.filter == "job" ? "job filter_active" : "job"}>
                            <span>В работе</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "archive")}
                            className={this.state.filter == "archive" ? "archive filter_active" : "archive"}>
                            <span>Архив</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});

MainCompanyForm = React.createClass({
    getInitialState: function() {
        return {doc: new model.company(),
                    veri_view: false,
                    veri_view_content: ""};
    },
    inputBind: function(event) {
        this.state.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    inputBindUser: function(event) {
        this.state.doc.users[0][event.target.name] = event.target.value;
        this.forceUpdate();
    },
    veriForm: function() {
        if(this.state.doc.primary.name == '') {
            this.setState({veri_view_content: "Нужно указать название компании!"});
            this.setState({veri_view: true});
        } else {
            this.state.doc.primary.key = root.key(this.state.doc.primary.name);
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        main_company_ctrl.add(this.state.doc); 
    },
    addForm: function() {
        this.setState({doc: new model.company(),
                        veri_view: false,
                        veri_view_content: ""});
        $('#MainCompanyModal').modal('show');
    },
    componentDidMount: function() {
        main_company_ctrl.addForm = this.addForm;
    },
    render: function() {
        return (
            <div id="MainCompanyModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w700">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">Новая компания</h4>
                        </div>
                        <div className="veri-modal" style={this.state.veri_view == true ? {display: ''} : {display: 'none'}}>
                            {this.state.veri_view_content}</div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <label>Название</label>
                                        <input type="text" 
                                                name="name"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.name}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Контактное лицо</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Телефон 1</label>
                                        <input type="text" 
                                                name="phone1"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.phone1}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Телефон 2</label>
                                        <input type="text" 
                                                name="phone2"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.phone2}/>
                                    </div>
                                </div>
                                <div className="col-md-6">   
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.mail}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Сайт</label>
                                        <input type="text" 
                                                name="url"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.primary.url}/>
                                    </div>
                                </div>  
                            </div>    
                        </div>
                        <div className="modal-footer">
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.veriForm} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});


MainCompanyList = React.createClass({
    getInitialState: function() {
        return {data: main_company_ctrl.store,
                request: jQuery.extend(true, {}, main_company_ctrl.default_request_data),
                display: jQuery.extend(true, {}, main_company_ctrl.display_step)};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        main_company_ctrl.update("change_limit");
    },
    stepPrev: function() {
        main_company_ctrl.update("step_prev");
    },
    stepNext: function() {
        main_company_ctrl.update("step_next");
    },
    add: function() {
        main_company_ctrl.addForm();
    },
    searchBind: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
        main_company_ctrl.update("search");
    },
    componentDidMount: function() {
        main_company_ctrl.setViewList = this.update;
        main_company_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, setViewBlock = this.setViewBlock, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Компании {this.state.request.offset} - {(this.state.request.offset*1) + (this.state.request.limit*1)} из {this.state.data.count}
                    <a className="btn blue right" onClick={this.add}>Добавить</a>
                </h3>
                <div className="row">
                    <div className="col-md-6">
                        <div className="input-f-wrapper">
                            <div className="wrap-company-input">
                                <input type="text" 
                                        name="name" 
                                        placeholder="Поиск по компании"
                                        onChange={this.searchBind} 
                                        value={this.state.request.name}/>
                                <a></a>
                            </div>
                        </div>   
                    </div>
                    <div className="col-md-6">
                        <div className="input-f-wrapper">
                            <div className="wrap-location-input">
                                <input type="text" 
                                        name="address" 
                                        placeholder="Поиск по договору или адресу"
                                        onChange={this.searchBind} 
                                        value={this.state.request.address}/>
                                <a></a>
                            </div>        
                        </div>
                    </div>
                </div>
                {this.state.data.data.map(function(elem) {
                    return (
                        <MainCompanyElem key={elem.id} data={elem}/>
                    )
                })}
                <div className="table-footer">
                    <div className="wrap-input-button input-f-wrapper w170 left tcenter">
                        <input className="input-button"
                                type="text" 
                                name="limit"
                                onChange={this.limitSet} 
                                value={this.state.request.limit}/>
                        <a className="btn default" onClick={this.limitChange}>Показать</a>
                    </div>
                    <div className="table-step-div">
                        <a className="btn default middle left"
                            style={{display: this.state.display.prev_button}}
                            onClick={this.stepPrev}>Назад</a>
                        <a className="btn default middle right"
                            style={{display: this.state.display.next_button}}
                            onClick={this.stepNext}>В перёд</a>
                    </div>
                </div>
            </div> 
        )
    }
});


MainCompanyElem = React.createClass({
    getInitialState: function() {
        return {data: this.props.data,
                saveView: "none",
                current_object: 0,
                listObjectView: false,
                listUserView: false};
    },
    bindStep: function(val) {
        this.state.data.doc.status.step = val;
        this.saveView();
    },
    saveView: function() {
        this.state.saveView = "";
        this.forceUpdate();
    },
    noteBind: function(event) {
        this.state.data.doc.primary.note = event.target.value;
        this.saveView();
    },
    elemEdit: function() {
        main_company_ctrl.edit(this.state.data);
        this.setState({saveView: "none"});
    },
    setElem: function(elem) {
        this.setState({data: elem});
    },
    infoEdit: function(form) {
        main_company_ctrl.info_edit(form, this.props.data, this.setElem);
    },
    addObject: function() {
        main_company_ctrl.addCompanyObject(this.state.data, this.setElem);
    },
    editObject: function(index) {
        main_company_ctrl.editCompanyObject(this.state.data, this.setElem, index);
    },
    setObject: function(index) {
        this.setState({current_object: index});
    },
    addAddress: function() {
        main_company_ctrl.addCompanyAddress(this.state.data, this.state.current_object, this.setElem);
    },
    editAddress: function(index) {
        main_company_ctrl.editCompanyAddress(this.state.data, this.state.current_object, this.setElem, index);
    },
    addUsers: function() {
        main_company_ctrl.addCompanyUsers(this.state.data, this.setElem);
    },
    editUsers: function(index) {
        main_company_ctrl.editCompanyUsers(this.state.data, this.setElem, index);
    },
    listObjectView: function() {
        this.setState({listUserView: false,
                        listObjectView: !this.state.listObjectView});
    },
    listUserView: function() {
        this.setState({listUserView: !this.state.listUserView,
                        listObjectView: false});
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var doc = this.state.data.doc, 
        self = this, 
        editObject = this.editObject, 
        setObject = this.setObject,
        current_object = this.state.current_object,
        editAddress = this.editAddress,
        editUsers = this.editUsers;
        return (
            <div>
                <div className="timeline-post">
                    <ul>
                        <li onClick={this.bindStep.bind(self, 0)}
                            className={doc.status.step == 0 ? "orange_active" : ""}><a>Новая</a></li>
                        <li onClick={this.bindStep.bind(self, 1)}
                            className={doc.status.step == 1 ? "green_active" : ""}><a>В работе</a></li>
                        <li onClick={this.bindStep.bind(self, 2)}
                            className={doc.status.step == 2 ? "blue_active" : ""}><a>Архив</a></li>
                    </ul>
                    <div className="info-block">
                        <div className="w250">
                            <div className="text_view green_h_t">{this.state.data.doc.primary.name}</div>
                            <div className="text_view">{this.state.data.doc.primary.mail}</div>
                            <div className="text_view">{this.state.data.doc.primary.url}</div>
                        </div>
                        <div className="w250">
                            <div className="text_view orange_h_t">{this.state.data.doc.primary.fio}</div>
                            <div className="text_view">{this.state.data.doc.primary.phone1}</div>
                            <div className="text_view">{this.state.data.doc.primary.phone2}</div>
                        </div>
                        <div className="textarea-i-wrapper">
                            <textarea name="nrd_note" 
                                onChange={this.noteBind} 
                                value={this.state.data.doc.primary.note}/>
                        </div>
                    </div>
                    <div className="options clearfix">
                        <ul>
                            <li><a onClick={this.infoEdit.bind(self, "info")} className="info"></a></li>
                            <li><a onClick={this.listObjectView.bind(self, "object")} className="object"></a></li>  
                            <li><a onClick={this.listUserView.bind(self, "users")} className="user"></a></li> 
                            <li><a onClick={this.infoEdit.bind(self, "order")} className="order"></a></li>  
                        </ul>
                        <a style={{display: this.state.saveView}} 
                            onClick={this.elemEdit}>Сохранить изменения!</a>
                    </div>
                    <div className="list" style={this.state.listObjectView == true ? {display: ''} : {display: 'none'}}>
                        <h4 className="heading-title"><span className="white">Договора {this.state.data.doc.object.length}</span>
                            <a className="btn blue small right" onClick={this.addObject}>Добавить договор</a>
                        </h4>
                        <table className="table simple">
                            <thead>
                                <tr>
                                    <th>Договор</th>
                                    <th>Цена</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.data.doc.object.map(function(elem, index) {
                                    return (
                                        <tr onClick={setObject.bind(self, index)} 
                                            onDoubleClick={editObject.bind(self, index)} 
                                            key={elem.object_name}
                                            className={index == current_object ? "red_t_b" : ""}>
                                            <td>{elem.object_name}</td>
                                            <td>{elem.contract_price}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <h4 className="heading-title white"><span className="white">Адреса по договору {this.state.data.doc.object[current_object] != undefined ? 
                                                                                                    this.state.data.doc.object[current_object].object_name : ''}</span>
                            <a style={this.state.data.doc.object[current_object] != undefined ? {display: ''} : {display: 'none'}} 
                                className="btn blue small right" onClick={this.addAddress}>Добавить адрес</a>
                        </h4>
                        <table className="table simple">
                            <thead>
                                <tr>
                                    <th>Адрес</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.data.doc.object[current_object] != undefined ? 
                                    this.state.data.doc.object[current_object].address.map(function(elem, index) {
                                    return (
                                        <tr onClick={editAddress.bind(self, index)} 
                                            key={elem.address}>
                                            <td>{elem.address}</td>
                                        </tr>
                                    )
                                }) : ''}
                            </tbody>
                        </table>
                    </div>
                    <div className="list" style={this.state.listUserView == true ? {display: ''} : {display: 'none'}}>
                        <h4 className="heading-title"><span className="white">Сотрудники {this.state.data.doc.users.length}</span>
                            <a className="btn blue small right" onClick={this.addUsers}>Добавить сотрудника</a>
                        </h4>
                        <table className="table simple">
                            <thead>
                                <tr>
                                    <th>ФИО</th>
                                    <th>Пользователь</th>
                                    <th>Почта</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.data.doc.users.length != 0 ? 
                                    this.state.data.doc.users.map(function(elem, index) {
                                        return (
                                            <tr onClick={editUsers.bind(self, index)} 
                                                key={elem.key}>
                                                <td>{elem.fio}</td>
                                                <td>{elem.login}</td>
                                                <td>{elem.mail}</td>
                                            </tr>
                                        )
                                    }) : ""}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
});


MainCompanyInfo = React.createClass({
    getInitialState: function() {
        return {data: {id: 0,
                        doc: new model.company()}
                };
    },
    setElem: function(elem, callback) {
        this.setState({data: elem});
        this.callbackSetElem = callback;
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        this.state.data.doc.primary.key = root.key(this.state.data.doc.primary.name);
        this.callbackSetElem(this.state.data);
        main_company_ctrl.edit(this.state.data);
        $('#MainCompanyInfoModal').modal('hide');
    },
    del: function() {
        main_company_ctrl.del(this.state.data.id);
    },
    componentDidMount: function() {
        main_company_ctrl.setMainCompanyInfo = this.setElem;
    },
    render: function() {
        return (
            <div id="MainCompanyInfoModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w660">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">Редактирование компании</h4>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <label>Название</label>
                                        <input type="text" 
                                                name="name"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.name}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Контактное лицо</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Телефон 1</label>
                                        <input type="text" 
                                                name="phone1"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.phone1}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Телефон 2</label>
                                        <input type="text" 
                                                name="phone2"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.phone2}/>
                                    </div>
                                </div>
                                <div className="col-md-6">   
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.mail}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Сайт</label>
                                        <input type="text" 
                                                name="url"
                                                onChange={this.inputBind} 
                                                value={this.state.data.doc.primary.url}/>
                                    </div>
                                </div>     
                            </div>      
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" onDoubleClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});


MainCompanyObject = React.createClass({
    getInitialState: function() {
        return {data: new model.object(),
                head: "Добавить договор",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                elem: "",
                index: "",
                type_check: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(type_check, val, head) {
        this.state.data.works_type = ['','',''];
        this.state.data.works_type[type_check] = val;
        this.state.type_check = type_check;
        var name = this.state.data.object_name.replace(/(ТО)/g,"");
        var name = name.replace(/ВиК/g,"");
        var name = name.replace(/Кс/g,"");
        var name = name.replace(/КСОБ/g,"");
        this.state.data.object_name = name + head;
        this.forceUpdate();
    },
    add: function(elem, callback) {
        this.setState({data: new model.object(),
                        head: "Добавить договор",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        elem: elem,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyObjectModal').modal('show');
    },
    edit: function(elem, callback, index) {
        this.setState({data: elem.doc.object[index],
                        head: "Редактировать договор",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        elem: elem,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyObjectModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.type_check == '') {
            this.setState({veri_view_content: "Нужно указать тип работ по договору!"});
            this.setState({veri_view: true});
        } else {
            /*var duble_object = false;
            for(var i in this.state.elem.doc.object) {
                if(this.state.elem.doc.object[i].works_type[this.state.type_check] == "checked" && this.state.index != i) {
                    duble_object = true;
                }  
            }
            if(duble_object == true) {
                this.setState({veri_view_content: "Такой договор уже существует!"});
                this.setState({veri_view: true}); 
            } else {
                this.setState({veri_view: false});
                this.save();
            }*/
            this.save();
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.object.push(this.state.data);
                    break;
            case "edit":   
                this.state.elem.doc.object[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyObjectModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.object.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyObjectModal').modal('hide');
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyObject = this.add;
        main_company_ctrl.editCompanyObject = this.edit;
    },
    render: function() {
        return (
            <div id="MainCompanyObjectModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w660">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.head}</h4>
                        </div>
                        <div className="veri-modal" style={this.state.veri_view == true ? {display: ''} : {display: 'none'}}>
                            {this.state.veri_view_content}</div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <label>Договор</label>
                                        <input type="text" 
                                                name="object_name"
                                                onChange={this.inputBind} 
                                                value={this.state.data.object_name}/>
                                    </div>
                                    <InputTextDate
                                        head="Дата заключения договора"
                                        name="contract_date"
                                        val={this.state.data.contract_date}
                                        bind={this.inputBind}/>
                                    <div className="input-f-wrapper">
                                        <label>Цена</label>
                                        <input type="text" 
                                                name="contract_price"
                                                onChange={this.inputBind} 
                                                value={this.state.data.contract_price}/>
                                    </div>
                                </div>
                                <div className="col-md-6">  
                                    <label>Типы работ</label> 
                                    <ul className="wrap-check">
                                        <InputCheck head="ТО"
                                                    val={this.state.data.works_type[0]}
                                                    num_check={0}
                                                    bind={this.checkGroupBind}/>
                                        <InputCheck head="ВиК"
                                                    val={this.state.data.works_type[1]}
                                                    num_check={1}
                                                    bind={this.checkGroupBind}/>
                                        <InputCheck head="КСОБ"
                                                    val={this.state.data.works_type[2]}
                                                    num_check={2}
                                                    bind={this.checkGroupBind}/>
                                    </ul>
                                </div>     
                            </div>      
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={this.state.view_del == true ? {display: ''} : {display: 'none'}} 
                                onDoubleClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.veriForm} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});

MainCompanyAddress = React.createClass({
    getInitialState: function() {
        return {data: new model.address(),
                head: "Добавить адрес",
                view_del: false,
                save_type: "add",
                elem: "",
                current_object: "",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        this.state.data[name] = val;
        this.forceUpdate();
    },
    add: function(elem, current_object, callback) {
        this.setState({data: new model.address(),
                        head: "Добавить адрес",
                        view_del: false,
                        save_type: "add",
                        elem: elem,
                        current_object: current_object,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyAddressModal').modal('show');
    },
    edit: function(elem, current_object, callback, index) {
        this.setState({data: elem.doc.object[current_object].address[index],
                        head: "Редактировать адрес",
                        view_del: true,
                        save_type: "edit",
                        elem: elem,
                        current_object: current_object,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyAddressModal').modal('show');
    },
    save: function() {        
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.object[this.state.current_object].address.push(this.state.data);
                    break;
            case "edit":   
                this.state.elem.doc.object[this.state.current_object].address[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyAddressModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.object[this.state.current_object].address.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyAddressModal').modal('hide');
    },
    pre_save: function() {
        var self = this;
        root.geocode(this.state.data.address, function(data) {
            if(data.response.GeoObjectCollection.featureMember[0] != undefined) {
                self.state.data.coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
            }
            self.forceUpdate();
            self.save();
        });
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyAddress = this.add;
        main_company_ctrl.editCompanyAddress = this.edit;
    },
    render: function() {
        return (
            <div id="MainCompanyAddressModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.head}</h4>
                        </div>
                        <div className="modal-body">
                            <InputKladr name="address"
                                        val={this.state.data.address}
                                        bind={this.inputBind}
                                        set={this.selectBind}/>
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={this.state.view_del == true ? {display: ''} : {display: 'none'}} 
                                onDoubleClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.pre_save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});


MainCompanyUsers = React.createClass({
    getInitialState: function() {
        return {data: new model.users(),
                head: "Добавить сотрудника",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                elem: "",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(num_check, val) {
        this.state.data.works_type[num_check] = val;
        this.forceUpdate();
    },
    add: function(elem, callback) {
        this.setState({data: new model.users(),
                        head: "Добавить сотрудника",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        elem: elem,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyUsersModal').modal('show');
    },
    edit: function(elem, callback, index) {
        this.setState({data: elem.doc.users[index],
                        head: "Редактировать сотрудника",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        elem: elem,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyUsersModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.login == '' || this.state.data.mail == '') {
            this.setState({veri_view_content: "Нужно указать пользователя и почту!"});
            this.setState({veri_view: true});
        } else {
            this.state.data.key = root.key(this.state.data.login + this.state.data.mail);
            var double_key = false;
            for(var i in this.state.elem.doc.users) {
                if(this.state.elem.doc.users[i].key == this.state.data.key) {
                    double_key = true;
                };
            };
            if(double_key == true) {
                this.setState({veri_view_content: "Пользователь с идентичным паролем и почтой уже существует!"});
                this.setState({veri_view: true});
            } else {
                this.setState({veri_view: false});
                this.save();
            }
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.users.push(this.state.data);
                root.send_mail(this.state.data.mail, "Вы добавленны в систему BSG-Online. Ваш логин: " + 
                                                        this.state.data.login + " Ваш пароль: " + 
                                                        this.state.data.mail + " Адрес системы http://bsg-online.ru");
                    break;
            case "edit":   
                this.state.elem.doc.users[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyUsersModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.users.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyUsersModal').modal('hide');
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyUsers = this.add;
        main_company_ctrl.editCompanyUsers = this.edit;
    },
    render: function() {
        return (
            <div id="MainCompanyUsersModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w660">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.head}</h4>
                        </div>
                        <div className="veri-modal" style={this.state.veri_view == true ? {display: ''} : {display: 'none'}}>
                            {this.state.veri_view_content}</div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputEng head="Пользователь"
                                        name="login"
                                        val={this.state.data.login}
                                        bind={this.inputBind}/>
                                    <div className="input-f-wrapper">
                                        <label>ФИО</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.data.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Должность</label>
                                        <input type="text" 
                                                name="post"
                                                onChange={this.inputBind} 
                                                value={this.state.data.post}/>
                                    </div>
                                </div>
                                <div className="col-md-6">  
                                    <div className="input-f-wrapper">
                                        <label>Телефон</label>
                                        <input type="text" 
                                                name="phone"
                                                onChange={this.inputBind} 
                                                value={this.state.data.phone}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.data.mail}/>
                                    </div>
                                </div>     
                            </div>      
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={this.state.view_del == true ? {display: ''} : {display: 'none'}} 
                                onDoubleClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.veriForm} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});
