MainUsers = React.createClass({
    componentDidMount: function() {

    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-3">
                        <MainUsersMenu/>
                    </div>
                    <div className="col-md-9">
                        <MainUsersForm/>
                        <MainUsersList/>
                    </div>
                </div>
            </div>    
        )
    }
});



MainUsersMenu = React.createClass({
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_user_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "admin")} 
                            className={this.state.filter == "admin" ? "admin filter_active" : "admin"}>
                            <span>Администраторы</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "master")}
                            className={this.state.filter == "master" ? "master filter_active" : "master"}>
                            <span>Инженеры</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});


MainUsersForm = React.createClass({
    getInitialState: function() {
        return {doc: new model.main_user(),
                header: "Новый пользователь",
                type: "add",
                id: "",
                veri_view: false,
                veri_view_content: "",
                delButton: "none",
                role: select.role};
    },
    inputBind: function(event) {
        this.state.doc[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        console.log(val);
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    veriForm: function() {
        if(this.state.doc.login == '' || this.state.doc.pass == '' || this.state.doc.mail == '') {
            this.setState({veri_view_content: "Нужно указать логин, почту и пароль!"});
            this.setState({veri_view: true});
        } else {
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                main_user_ctrl.add(this.state.doc); 
                    break;
            case "edit":
                main_user_ctrl.edit(this.state.doc, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({doc: new model.main_user(), 
                        header: "Новый пользователь", 
                        type: "add",
                        id: "",
                        veri_view: false,
                        veri_view_content: "",
                        delButton: "none"});
        $('#MainUsersModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({doc: elem.doc, 
                        id: elem.id, 
                        header: "Редактирование пользователя", 
                        veri_view: false,
                        veri_view_content: "",
                        type: "edit",
                        delButton: ""});
        $('#MainUsersModal').modal('show');
    },
    del: function() {
        main_user_ctrl.del(this.state.id);
    },
    componentDidMount: function() {
        main_user_ctrl.addForm = this.addForm;
        main_user_ctrl.editForm = this.editForm;
    },
    render: function() {
        return (
            <div id="MainUsersModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w700">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.header}</h4>
                        </div>
                        <div className="veri-modal" style={this.state.veri_view == true ? {display: ''} : {display: 'none'}}>
                            {this.state.veri_view_content}</div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputEng name="login"
                                                head="Логин"
                                                bind={this.inputBind}
                                                val={this.state.doc.login}/>
                                    <div className="input-f-wrapper">
                                        <label>ФИО</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Роль</label>
                                            <InputSelect name="role"
                                                        val={this.state.doc.role}
                                                        bind={this.selectBind}
                                                        data={this.state.role}/>
                                    </div>
                                    
                                </div>
                                <div className="col-md-6">   
                                    <InputPhone name="phone"
                                                head="Телефон"
                                                bind={this.inputBind}
                                                val={this.state.doc.phone}/>              
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.mail}/>
                                    </div>
                                    <InputEng name="pass"
                                                head="Пароль"
                                                bind={this.inputBind}
                                                val={this.state.doc.pass}/>
                                </div>  
                            </div>    
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={{display: this.state.delButton}} 
                                onClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.veriForm} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

MainUsersList = React.createClass({
    getInitialState: function() {
        return {data: main_user_ctrl.store,
                request: jQuery.extend(true, {}, main_user_ctrl.default_request_data),
                display: jQuery.extend(true, {}, main_user_ctrl.display_step)};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        main_user_ctrl.update("change_limit");
    },
    stepPrev: function() {
        main_user_ctrl.update("step_prev");
    },
    stepNext: function() {
        main_user_ctrl.update("step_next");
    },
    add: function() {
        main_user_ctrl.addForm();
    },
    edit: function(elem) {
        main_user_ctrl.editForm(elem);
    },
    componentDidMount: function() {
        main_user_ctrl.setViewList = this.update;
        main_user_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Пользователи {this.state.request.offset} - {(this.state.request.offset*1) + (this.state.request.limit*1)} из {this.state.data.count}
                    <a className="btn blue right" onClick={this.add}>Добавить</a>
                </h3>
                <table className="table simple">
                    <thead>
                        <tr>
                            <th>Пользователь</th>
                            <th>Логин</th>
                            <th>Роль</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.data.map(function(elem) {
                            return (
                                    <tr key={elem.id} onClick={edit.bind(self, elem)}>
                                        <td>{elem.doc.fio}</td>
                                        <td>{elem.doc.login}</td>
                                        <td>{elem.doc.role}</td>
                                    </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="table-footer">
                    <div className="wrap-input-button input-f-wrapper w170 left tcenter">
                        <input className="input-button"
                                type="text" 
                                name="limit"
                                onChange={this.limitSet} 
                                value={this.state.request.limit}/>
                        <a className="btn default" onClick={this.limitChange}>Показать</a>
                    </div>
                    <div className="table-step-div">
                        <a className="btn default middle left"
                            style={{display: this.state.display.prev_button}}
                            onClick={this.stepPrev}>Назад</a>
                        <a className="btn default middle right"
                            style={{display: this.state.display.next_button}}
                            onClick={this.stepNext}>В перёд</a>
                    </div>
                </div>
            </div> 
        )
    }
});