MainGuide = React.createClass({
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                        <MainGuideList/>
                </div>
            </div>    
        )
    }
});

MainGuideList = React.createClass({
    render: function() {
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Инструкции</h3>
                <div className="timeline-item">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="comment panel-style extra">
                                <p><a>Добавление внутреннего пользователя</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/XlYJXEGfchY" frameBorder="0" allowFullScreen></iframe>
                            </div>
                            <div className="comment panel-style extra">
                                <p><a>Редактирование компании</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px"  src="https://www.youtube.com/embed/37_SZepYmRM" frameBorder="0" allowFullScreen></iframe>
                            </div>
                            <div className="comment panel-style extra">
                                <p><a>Добавление адресов в компанию</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px"  src="https://www.youtube.com/embed/SNwCM1EEhDk" frameBorder="0" allowFullScreen></iframe>
                            </div>
                            <div className="comment panel-style extra">
                                <p><a>Добавление внешнего пользователя в компанию</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px"  src="https://www.youtube.com/embed/521Pzu_A190" frameBorder="0" allowFullScreen></iframe>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="comment panel-style extra">
                                <p><a>Добавление компании</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/sqitFi1fwIM" frameBorder="0" allowFullScreen></iframe>
                            </div>
                            <div className="comment panel-style extra">
                                <p><a>Добавление договоров в компанию</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/F7cQw81zhac" frameBorder="0" allowFullScreen></iframe>
                            </div>
                            <div className="comment panel-style extra">
                                <p><a>Удаление компании и связной информации</a></p>
                                <div className="pb10"></div>
                                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/6MQh-tIVTJw" frameBorder="0" allowFullScreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        )
    }
});


