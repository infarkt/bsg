NewOrder = React.createClass({
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            <div className="wrapper-content">
                <NewOrderMaster/>
                <MainCompanyUsers/>
            </div>    
        )
    }
});

NewOrderMaster = React.createClass({
    getInitialState: function() {
        return {master_step: 0,
                search: "",
                search_user: "",
                search_address: "",
                company: [],
                selected_company: "",
                company_full_address: [],
                company_all_address: [],
                selected_address: "",
                selected_user: "",
                selected_user_index: "",
                reason: "",
                right_contract: [],
                selected_contract: "",
                order: new model.order(),
                save: null};
    },
    resetMaster: function() {
        this.replaceState(this.getInitialState());
    },
    stepMaster: function(step) {
        this.setState({master_step: step});
    },
    searchBind: function(event) {
        this.setState({search: event.target.value});
        new_order_ctrl.company_request(event.target.value);
    },
    setCompanyList: function(data) {
        this.setState({company: data});
    },
    selectCompany: function(elem) {
        this.state.selected_company = elem;
        this.state.company_full_address = _.uniq(_.flatten(_.pluck(this.state.selected_company.doc.object, 'address')));
        this.state.company_all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.selected_company.doc.object, 'address')), 'address'));
        this.forceUpdate();
        this.stepMaster(1);  
    },
    searchAddress: function(event) {
        this.setState({search_address: event.target.value});
    },
    selectAllAddress: function(address) {
        for(var i in this.state.company_full_address) {
            if(this.state.company_full_address[i].address == address) {
                this.state.selected_address = this.state.company_full_address[i];
            }
        }
        if(this.state.selected_company.doc.users[this.state.selected_user_index].my_address.indexOf(this.state.selected_address.address) == -1) {
            this.state.selected_company.doc.users[this.state.selected_user_index].my_address.push(this.state.selected_address.address);
        };
        main_company_ctrl.edit(this.state.selected_company);
        this.stepMaster(3);
    },
    selectMyAddress: function(elem) {
        for(var i in this.state.company_full_address) {
            if(this.state.company_full_address[i].address == elem) {
                this.state.selected_address = this.state.company_full_address[i];
            }
        }
        this.stepMaster(3);
    },
    searchUser: function(event) {
        this.setState({search_user: event.target.value});
    },
    selectUser: function(elem, index) {
        this.state.selected_user = elem;
        this.state.selected_user_index = index;
        this.stepMaster(2);
    },
    addUser: function() {
        main_company_ctrl.addCompanyUsers(this.state.selected_company, this.updateCompany);
    },
    bindReason: function(event) {
        this.setState({reason: event.target.value});
    },
    updateCompany: function(elem) {
        this.setState({selected_company: elem});
    },
    getContract: function() {
        this.state.right_contract = new_order_ctrl.get_contract_in_address(this.state.selected_company.doc, this.state.selected_address.address);
        this.forceUpdate();
        if(this.state.right_contract.length == 1) {
            this.state.selected_contract = this.state.right_contract[0];
            this.getCountOrder();
        } else {
            this.stepMaster(4);
        }
    },
    selectContract: function(elem) {
        this.state.selected_contract = elem;
        this.getCountOrder();
    },
    getCountOrder: function() {
        new_order_ctrl.get_order_user_number(this.state.selected_user.key);
    },
    setResult: function(count) {
        this.state.order.system.key = this.state.selected_company.id + "/" + Number(this.state.selected_user_index + 1) + "/" + Number(count + 1);
        this.state.order.system.company_key = this.state.selected_company.doc.primary.key;
        this.state.order.system.persone_key = this.state.selected_company.doc.users[this.state.selected_user_index].key;
        this.state.order.system.add_date = format.date_ms();
        this.state.order.primary.status = 0;
        this.state.order.primary.company = this.state.selected_company.doc.primary.name;
        this.state.order.primary.persone = this.state.selected_company.doc.users[this.state.selected_user_index].fio;
        this.state.order.primary.phone = this.state.selected_company.doc.users[this.state.selected_user_index].phone;
        this.state.order.primary.mail = this.state.selected_company.doc.users[this.state.selected_user_index].mail;
        this.state.order.primary.contract = this.state.selected_contract.object_name;
        this.state.order.primary.address = this.state.selected_address.address;
        this.state.order.primary.coords = this.state.selected_address.coords;
        this.state.order.primary.type = new_order_ctrl.get_contract_type(this.state.selected_contract.works_type);
        this.state.order.primary.reason = this.state.reason;

        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                root.send_mail("9219505184@mail.ru", "Новая заявка");
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                root.send_mail("9312323303@mail.ru", "Новая заявка");
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                root.send_mail("9213005737@mail.ru", "Новая заявка");
                    break;
        }
        this.stepMaster(5);
    },
    saveOrder: function() {
        var self = this;
        new_order_ctrl.add_order(this.state.order, function(data) {
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
    },
    errorSave: function() {
        this.setState({save: false});
    },
    componentDidMount: function() {
        new_order_ctrl.set_company_list = this.setCompanyList;
        new_order_ctrl.set_result = this.setResult;
    },
    render: function() {
        self = this, selectCompany = this.selectCompany, selectMyAddress = this.selectMyAddress, 
        selectAllAddress = this.selectAllAddress, selectUser = this.selectUser, selectContract = this.selectContract, order = this.state.order;
        return (
            <div className="panel-style space m-top-0 ml510">
                <h3 className="heading-title">Создание новой заявки</h3>
                <div className="type-four-tab-wrapper">
                    <ul className="nav nav-tabs easy-style type-four">
                        <li className={this.state.master_step == 0 ? "active" : ""}>
                            <a>1. Найдите компанию</a>
                        </li>
                        <li className={this.state.master_step == 1 ? "active" : ""}>
                            <a>2. Укажите контактное лицо</a>
                        </li>
                        <li className={this.state.master_step == 2 ? "active" : ""}>
                            <a>3. Укажите адрес</a>
                        </li>
                        <li className={this.state.master_step == 3 ? "active" : ""}>
                            <a>4. Опишите проблему</a>
                        </li>
                        <li className={this.state.master_step == 4 ? "active" : ""}>
                            <a>5. Укажите договор</a>
                        </li>
                        <li className={this.state.master_step == 5 ? "active" : ""}>
                            <a>Результат</a>
                        </li>
                    </ul>

                    <div className="tab-content easy-style type-four">
                        <div className={this.state.master_step == 0 ? "tab-pane active" : "tab-pane"}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <div className="wrap-company-input">
                                            <input type="text" 
                                                    name="name" 
                                                    placeholder="Поиск по названию компании"
                                                    onChange={this.searchBind} 
                                                    value={this.state.search}/>
                                            <a></a>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            <table className="table simple" style={this.state.company.length != 0 ? {display: ''} : {display: 'none'}}>
                                <thead>
                                    <tr>
                                        <th>Найденные компании</th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.company.map(function(elem, index) {
                                        return (
                                            <tr key={elem.doc.primary.key}>
                                                <td>{elem.doc.primary.name}
                                                    <a className="btn simple green_h_t right" 
                                                        onClick={selectCompany.bind(self, elem)}>Выбрать</a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                            <p style={this.state.company.length == 0 ? {display: ''} : {display: 'none'}}>
                                Совпадений не найденно!</p>
                        </div>
                        <div className={this.state.master_step == 1 ? "tab-pane active" : "tab-pane"}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <div className="wrap-user-input">
                                            <input type="text" 
                                                    name="name" 
                                                    placeholder="Поиск контактного лица"
                                                    onChange={this.searchUser} 
                                                    value={this.state.search_user}/>
                                            <a></a>
                                        </div>
                                    </div> 
                                </div>
                                <div className="col-md-6">
                                    <h3 className="heading-title">
                                        <a className="btn blue right" onClick={this.addUser}>Добавить контактное лицо</a>
                                    </h3>
                                </div>
                            </div>
                            <table className="table simple" style={this.state.selected_company != "" ? {display: ''} : {display: 'none'}}>
                                <thead>
                                    <tr>
                                        <th>Зарегистрированные пользователи</th>
                                        <th>Телефон</th>
                                        <th></th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.selected_company != "" ? 
                                        this.state.selected_company.doc.users.map(function(elem, index) {
                                            if(elem.fio.toLowerCase().indexOf(self.state.search_user.toLowerCase()) != -1) {
                                                return (
                                                    <tr key={elem.key}>
                                                        <td>{elem.fio}</td>
                                                        <td>{elem.phone}</td>
                                                        <td><a className="btn simple green_h_t right" 
                                                                onClick={selectUser.bind(self, elem, index)}>Выбрать</a>
                                                        </td>
                                                    </tr>
                                                )
                                            }   
                                    }) : ''}
                                </tbody>
                            </table>
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 0)}>Назад</a>
                            </div>    
                        </div>
                        <div className={this.state.master_step == 2 ? "tab-pane active" : "tab-pane"}>
                            <table className="table simple" style={this.state.selected_user != "" ? {display: ''} : {display: 'none'}}>
                                <thead>
                                    <tr>
                                        <th>Предыдущие адреса заказчика</th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.selected_user != "" ? 
                                        this.state.selected_user.my_address.map(function(elem, index) {
                                            return (
                                                <tr key={index}>
                                                    <td>{elem}
                                                        <a className="btn simple green_h_t right" 
                                                            onClick={selectMyAddress.bind(self, elem)}>Выбрать</a>
                                                    </td>
                                                </tr>
                                            )
                                    }) : ''}
                                </tbody>
                            </table>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="input-f-wrapper">
                                        <div className="wrap-location-input">
                                            <input type="text" 
                                                    name="name" 
                                                    placeholder="Поиск по всем адресам"
                                                    onChange={this.searchAddress} 
                                                    value={this.state.search_address}/>
                                            <a></a>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            <table className="table simple" style={this.state.company_all_address.length != 0 ? {display: ''} : {display: 'none'}}>
                                <thead>
                                    <tr>
                                        <th>Все зарегистрированные адреса компании</th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.company_all_address.length != 0 ? 
                                        this.state.company_all_address.map(function(elem, index) {
                                            if(elem.toLowerCase().indexOf(self.state.search_address.toLowerCase()) != -1) {
                                                return (
                                                    <tr key={index}>
                                                        <td>{elem}
                                                            <a className="btn simple green_h_t right" 
                                                                onClick={selectAllAddress.bind(self, elem)}>Выбрать</a>
                                                        </td>
                                                    </tr>
                                                )
                                            }    
                                    }) : ''}
                                </tbody>
                            </table>  
                            <p style={this.state.company_all_address.length == 0 ? {display: ''} : {display: 'none'}}>
                                Нет зарегистрированных адресов!</p>              
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 1)}>Назад</a>
                            </div>
                        </div>
                        <div className={this.state.master_step == 3 ? "tab-pane active" : "tab-pane"}>
                            <div className="textarea-i-wrapper">
                                <textarea onChange={this.bindReason} 
                                        value={this.state.reason}/>
                            </div>
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 2)}>Назад</a>
                                <a className="btn green right" onClick={this.getContract} 
                                    style={this.state.reason != "" ? {display: ''} : {display: 'none'}}>Дальше</a>
                            </div>
                        </div>
                        <div className={this.state.master_step == 4 ? "tab-pane active" : "tab-pane"}>
                            <table className="table simple">
                                <thead>
                                    <tr>
                                        <th>Существующие договора по текущему адресу</th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.right_contract.length != 0 ? 
                                        this.state.right_contract.map(function(elem, index) {
                                            return (
                                                <tr key={index}>
                                                    <td>{elem.object_name}
                                                        <a className="btn simple green_h_t right" 
                                                            onClick={selectContract.bind(self, elem)}>Выбрать</a>
                                                    </td>
                                                </tr>
                                            )
                                    }) : ''}
                                </tbody>
                            </table>
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 3)}>Назад</a>
                            </div>
                        </div>
                        <div className={this.state.master_step == 5 ? "tab-pane active" : "tab-pane"}>
                             <div>
                                <div className="form-see" style={this.state.save == null ? {display: ''} : {display: 'none'}}>
                                    <span className="grey-title">Заказчик</span>
                                    <ul>
                                        <li>{order.primary.company}</li>
                                        <li>{order.primary.persone}</li>
                                        <li>{order.primary.phone}</li>
                                    </ul>
                                    <div className="separator"></div>
                                    <span className="grey-title">Заявка</span>
                                    <ul>
                                        <li>№ {order.system.key}</li>
                                        <li>{order.primary.contract}</li>
                                        <li>{order.primary.address}</li>
                                        <li>{order.primary.type}</li>
                                        <li className="blue_l_t">{order.primary.reason}</li>
                                    </ul>
                                    <div className="separator"></div> 
                                    <div className="master-footer">
                                        <a className="btn orange left" onClick={this.stepMaster.bind(self, 4)}>Назад</a>
                                        <a className="btn green right" onClick={this.saveOrder}>Отправить</a>
                                    </div>
                                </div>
                                <div style={this.state.save == true ? {display: ''} : {display: 'none'}}>
                                    <p>Заявка успешно добавлена!</p>
                                    <div className="master-footer">
                                        <a className="btn green right" onClick={this.resetMaster}>В начало</a>
                                    </div>
                                </div>
                                <div style={this.state.save == false ? {display: ''} : {display: 'none'}}>
                                    <p>Внимание заявка НЕ ДОБАВЛЕНА!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>     
        )
    }
});



// Компонет вложенный но выполнен выделенно!
/*SeeOrderMaster = React.createClass({
    getInitialState: function() {
        return {master_step: 0,
                order: new model.order(),
                save: null};
    },
    setForma: function(name, val) {
        this.state.order.primary[name] =  val;
        this.forceUpdate();
    },
    backStep: function() {
        new_order_ctrl.backStep();
    },
    saveOrder: function() {
        var self = this;
        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                    break;
        }
        // Отправка почты!!!!
        console.log(this.state.order);
        new_order_ctrl.add_order(this.state.order, function(data) {
            console.log(data);
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
        order_ctrl.load();
    },
    errorSave: function() {
        this.setState({save: false});
    },
    resetMaster: function() {
        new_order_ctrl.resetMaster();
        this.setState({master_step: 0,
                        order: new model.order(),
                        save: null});
    },
    componentDidMount: function() {
        new_order_ctrl.setForma = this.setForma;
    },
    render: function() {
        var order = this.state.order;
        return (
                <div>
                    <div className="form-see" style={this.state.save == null ? {display: ''} : {display: 'none'}}>
                        <span className="grey-title">Заказчик</span>
                        <ul>
                            <li>{order.primary.company}</li>
                            <li>{order.primary.persone}</li>
                            <li>{order.primary.phone}</li>
                        </ul>
                        <div className="separator"></div>
                        <span className="grey-title">Заявка</span>
                        <ul>
                            <li>№ {order.system.key}</li>
                            <li>{order.primary.contract}</li>
                            <li>{order.primary.address}</li>
                            <li>{order.primary.type}</li>
                            <li className="blue_l_t">{order.primary.reason}</li>
                        </ul>
                        <div className="separator"></div> 
                        <div className="master-footer">
                            <a className="btn orange left" onClick={this.backStep}>Назад</a>
                            <a className="btn green right" onClick={this.saveOrder}>Отправить</a>
                        </div>
                    </div>
                    <div style={this.state.save == true ? {display: ''} : {display: 'none'}}>
                        <p>Заявка успешно отправлена!</p>
                        <div className="master-footer">
                            <a className="btn green right" onClick={this.resetMaster}>В начало</a>
                        </div>
                    </div>
                    <div style={this.state.save == false ? {display: ''} : {display: 'none'}}>
                        <p>Внимание заявка НЕ ОТПРАВЛЕНА!</p>
                    </div>
                </div>
        )
    }
});*/