Auth = React.createClass({displayName: "Auth",
    getInitialState: function() {
        return {error_view: "none",
                login: "",
                pass: ""};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    set_view_error: function(val) {
        this.setState({error_view: val});
    },
    login: function() {
        var _set_view_error = this.set_view_error;
        root.send("login", this.state, function(result) {
            if(result != 0) {
                _set_view_error('none');
                root.set_user(JSON.parse(result.doc));
            } else {
                _set_view_error('block');
            }
        });
    },
    render: function() {
        return (
            React.createElement("div", {id: "login"}, 
                React.createElement("div", {className: "logo"}, 
                    React.createElement("a", {href: "index.html"}, 
                        React.createElement("img", {alt: "", src: "../styles/images/logo-crm.png"})
                    )
                ), 
                React.createElement("div", {className: "wrapper-login"}, 
                    React.createElement("input", {className: "user", 
                            type: "text", 
                            name: "login", 
                            onChange: this.inputBind, 
                            value: this.state.login}), 
                    React.createElement("input", {className: "password", 
                            type: "password", 
                            name: "pass", 
                            onChange: this.inputBind, 
                            value: this.state.pass})
                ), 
                React.createElement("div", {className: "message", style: {display: this.state.error_view}}, React.createElement("h4", null, "Ошибка!")), 
                React.createElement("a", {className: "login-button", onClick: this.login}, "Вход"), 
                React.createElement("span", null, 
                    React.createElement("a", {href: "#"})
                )
            )
        );
    } 
});
Company = React.createClass({displayName: "Company",
    componentDidMount: function() {

    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(MainCompanyMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        React.createElement(MainCompanyForm, null), 
                        React.createElement(MainCompanyList, null), 
                        React.createElement(MainCompanyInfo, null), 
                        React.createElement(MainCompanyObject, null), 
                        React.createElement(MainCompanyAddress, null), 
                        React.createElement(MainCompanyUsers, null)
                    )
                )
            )    
        )
    }
});


MainCompanyMenu = React.createClass({displayName: "MainCompanyMenu",
    getInitialState: function() {
        return {filter: "job",
                new_count: main_company_ctrl.data_count.news};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_company_ctrl.update(val);
    },
    setCount: function(val) {
        this.setState({new_count: val});
    },
    componentDidMount: function() {
        main_company_ctrl.setNewCount = this.setCount;
        main_company_ctrl.setFilter = this.setFilter;
    },
    render: function() {
        var self = this;
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "ФИЛЬТР"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "new"), 
                            className: this.state.filter == "new" ? "new filter_active" : "new"}, 
                            React.createElement("span", null, "Новые"), React.createElement("span", {className: "count"}, this.state.new_count), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "job"), 
                            className: this.state.filter == "job" ? "job filter_active" : "job"}, 
                            React.createElement("span", null, "В работе"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "archive"), 
                            className: this.state.filter == "archive" ? "archive filter_active" : "archive"}, 
                            React.createElement("span", null, "Архив"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "all"), 
                            className: this.state.filter == "all" ? "all filter_active" : "all"}, 
                            React.createElement("span", null, "Все"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});

MainCompanyForm = React.createClass({displayName: "MainCompanyForm",
    getInitialState: function() {
        return {doc: new model.company(),
                    veri_view: false,
                    veri_view_content: ""};
    },
    inputBind: function(event) {
        this.state.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    inputBindUser: function(event) {
        this.state.doc.users[0][event.target.name] = event.target.value;
        this.forceUpdate();
    },
    veriForm: function() {
        if(this.state.doc.primary.name == '') {
            this.setState({veri_view_content: "Нужно указать название компании!"});
            this.setState({veri_view: true});
        } else {
            this.state.doc.primary.key = root.key(this.state.doc.primary.name);
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        main_company_ctrl.add(this.state.doc); 
    },
    addForm: function() {
        this.setState({doc: new model.company(),
                        veri_view: false,
                        veri_view_content: ""});
        $('#MainCompanyModal').modal('show');
    },
    componentDidMount: function() {
        main_company_ctrl.addForm = this.addForm;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainCompanyModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w700"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, "Новая компания")
                        ), 
                        React.createElement("div", {className: "veri-modal", style: this.state.veri_view == true ? {display: ''} : {display: 'none'}}, 
                            this.state.veri_view_content), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Название"), 
                                        React.createElement("input", {type: "text", 
                                                name: "name", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.name})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Контактное лицо"), 
                                        React.createElement("input", {type: "text", 
                                                name: "fio", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.fio})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон 1"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone1", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.phone1})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон 2"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone2", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.phone2})
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Почта"), 
                                        React.createElement("input", {type: "text", 
                                                name: "mail", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.mail})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Сайт"), 
                                        React.createElement("input", {type: "text", 
                                                name: "url", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.primary.url})
                                    )
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.veriForm, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )
    }
});


MainCompanyList = React.createClass({displayName: "MainCompanyList",
    getInitialState: function() {
        return {data: main_company_ctrl.store,
                request: jQuery.extend(true, {}, main_company_ctrl.default_request_data),
                display: jQuery.extend(true, {}, main_company_ctrl.display_step)};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        main_company_ctrl.update("change_limit");
    },
    stepPrev: function() {
        main_company_ctrl.update("step_prev");
    },
    stepNext: function() {
        main_company_ctrl.update("step_next");
    },
    add: function() {
        main_company_ctrl.addForm();
    },
    searchBind: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
        main_company_ctrl.update("search");
    },
    componentDidMount: function() {
        main_company_ctrl.setViewList = this.update;
        main_company_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, setViewBlock = this.setViewBlock, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Компании ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", onClick: this.add}, "Добавить")
                ), 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-6"}, 
                        React.createElement("div", {className: "input-f-wrapper"}, 
                            React.createElement("div", {className: "wrap-company-input"}, 
                                React.createElement("input", {type: "text", 
                                        name: "name", 
                                        placeholder: "Поиск по компании", 
                                        onChange: this.searchBind, 
                                        value: this.state.request.name}), 
                                React.createElement("a", null)
                            )
                        )
                    ), 
                    React.createElement("div", {className: "col-md-6"}, 
                        React.createElement("div", {className: "input-f-wrapper"}, 
                            React.createElement("div", {className: "wrap-location-input"}, 
                                React.createElement("input", {type: "text", 
                                        name: "address", 
                                        placeholder: "Поиск по договору или адресу", 
                                        onChange: this.searchBind, 
                                        value: this.state.request.address}), 
                                React.createElement("a", null)
                            )
                        )
                    )
                ), 
                this.state.data.data.map(function(elem) {
                    return (
                        React.createElement(MainCompanyElem, {key: elem.id, data: elem})
                    )
                }), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "В перёд")
                    )
                )
            ) 
        )
    }
});


MainCompanyElem = React.createClass({displayName: "MainCompanyElem",
    getInitialState: function() {
        return {data: this.props.data,
                saveView: "none",
                current_object: 0,
                listObjectView: false,
                listUserView: false};
    },
    bindStep: function(val) {
        this.state.data.doc.status.step = val;
        this.saveView();
    },
    saveView: function() {
        this.state.saveView = "";
        this.forceUpdate();
    },
    noteBind: function(event) {
        this.state.data.doc.primary.note = event.target.value;
        this.saveView();
    },
    elemEdit: function() {
        main_company_ctrl.edit(this.state.data);
        this.setState({saveView: "none"});
    },
    setElem: function(elem) {
        this.setState({data: elem});
    },
    infoEdit: function(form) {
        main_company_ctrl.info_edit(form, this.props.data, this.setElem);
    },
    addObject: function() {
        main_company_ctrl.addCompanyObject(this.state.data, this.setElem);
    },
    editObject: function(index) {
        main_company_ctrl.editCompanyObject(this.state.data, this.setElem, index);
    },
    setObject: function(index) {
        this.setState({current_object: index});
    },
    addAddress: function() {
        main_company_ctrl.addCompanyAddress(this.state.data, this.state.current_object, this.setElem);
    },
    editAddress: function(index) {
        main_company_ctrl.editCompanyAddress(this.state.data, this.state.current_object, this.setElem, index);
    },
    addUsers: function() {
        main_company_ctrl.addCompanyUsers(this.state.data, this.setElem);
    },
    editUsers: function(index) {
        main_company_ctrl.editCompanyUsers(this.state.data, this.setElem, index);
    },
    listObjectView: function() {
        this.setState({listUserView: false,
                        listObjectView: !this.state.listObjectView});
    },
    listUserView: function() {
        this.setState({listUserView: !this.state.listUserView,
                        listObjectView: false});
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var doc = this.state.data.doc, 
        self = this, 
        editObject = this.editObject, 
        setObject = this.setObject,
        current_object = this.state.current_object,
        editAddress = this.editAddress,
        editUsers = this.editUsers;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "timeline-post"}, 
                    React.createElement("ul", null, 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 0), 
                            className: doc.status.step == 0 ? "orange_active" : ""}, React.createElement("a", null, "Новая")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 1), 
                            className: doc.status.step == 1 ? "green_active" : ""}, React.createElement("a", null, "В работе")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 2), 
                            className: doc.status.step == 2 ? "blue_active" : ""}, React.createElement("a", null, "Архив"))
                    ), 
                    React.createElement("div", {className: "info-block"}, 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view green_h_t"}, this.state.data.doc.primary.name), 
                            React.createElement("div", {className: "text_view"}, this.state.data.doc.primary.mail), 
                            React.createElement("div", {className: "text_view"}, this.state.data.doc.primary.url)
                        ), 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view orange_h_t"}, this.state.data.doc.primary.fio), 
                            React.createElement("div", {className: "text_view"}, this.state.data.doc.primary.phone1), 
                            React.createElement("div", {className: "text_view"}, this.state.data.doc.primary.phone2)
                        ), 
                        React.createElement("div", {className: "textarea-i-wrapper"}, 
                            React.createElement("textarea", {name: "nrd_note", 
                                onChange: this.noteBind, 
                                value: this.state.data.doc.primary.note})
                        )
                    ), 
                    React.createElement("div", {className: "options clearfix"}, 
                        React.createElement("ul", null, 
                            React.createElement("li", null, React.createElement("a", {onClick: this.infoEdit.bind(self, "info"), className: "info"})), 
                            React.createElement("li", null, React.createElement("a", {onClick: this.listObjectView.bind(self, "object"), className: "object"})), 
                            React.createElement("li", null, React.createElement("a", {onClick: this.listUserView.bind(self, "users"), className: "user"})), 
                            React.createElement("li", null, React.createElement("a", {onClick: this.infoEdit.bind(self, "order"), className: "order"}))
                        ), 
                        React.createElement("a", {style: {display: this.state.saveView}, 
                            onClick: this.elemEdit}, "Сохранить изменения!")
                    ), 
                    React.createElement("div", {className: "list", style: this.state.listObjectView == true ? {display: ''} : {display: 'none'}}, 
                        React.createElement("h4", {className: "heading-title"}, React.createElement("span", {className: "white"}, "Договора ", this.state.data.doc.object.length), 
                            React.createElement("a", {className: "btn blue small right", onClick: this.addObject}, "Добавить договор")
                        ), 
                        React.createElement("table", {className: "table simple"}, 
                            React.createElement("thead", null, 
                                React.createElement("tr", null, 
                                    React.createElement("th", null, "Договор"), 
                                    React.createElement("th", null, "Цена")
                                )
                            ), 
                            React.createElement("tbody", null, 
                                this.state.data.doc.object.map(function(elem, index) {
                                    return (
                                        React.createElement("tr", {onClick: setObject.bind(self, index), 
                                            onDoubleClick: editObject.bind(self, index), 
                                            key: elem.object_name, 
                                            className: index == current_object ? "red_t_b" : ""}, 
                                            React.createElement("td", null, elem.object_name), 
                                            React.createElement("td", null, elem.contract_price)
                                        )
                                    )
                                })
                            )
                        ), 
                        React.createElement("h4", {className: "heading-title white"}, React.createElement("span", {className: "white"}, "Адреса по договору ", this.state.data.doc.object[current_object] != undefined ? 
                                                                                                    this.state.data.doc.object[current_object].object_name : ''), 
                            React.createElement("a", {style: this.state.data.doc.object[current_object] != undefined ? {display: ''} : {display: 'none'}, 
                                className: "btn blue small right", onClick: this.addAddress}, "Добавить адрес")
                        ), 
                        React.createElement("table", {className: "table simple"}, 
                            React.createElement("thead", null, 
                                React.createElement("tr", null, 
                                    React.createElement("th", null, "Адрес")
                                )
                            ), 
                            React.createElement("tbody", null, 
                                this.state.data.doc.object[current_object] != undefined ? 
                                    this.state.data.doc.object[current_object].address.map(function(elem, index) {
                                    return (
                                        React.createElement("tr", {onClick: editAddress.bind(self, index), 
                                            key: elem.address}, 
                                            React.createElement("td", null, elem.address)
                                        )
                                    )
                                }) : ''
                            )
                        )
                    ), 
                    React.createElement("div", {className: "list", style: this.state.listUserView == true ? {display: ''} : {display: 'none'}}, 
                        React.createElement("h4", {className: "heading-title"}, React.createElement("span", {className: "white"}, "Сотрудники ", this.state.data.doc.users.length), 
                            React.createElement("a", {className: "btn blue small right", onClick: this.addUsers}, "Добавить сотрудника")
                        ), 
                        React.createElement("table", {className: "table simple"}, 
                            React.createElement("thead", null, 
                                React.createElement("tr", null, 
                                    React.createElement("th", null, "ФИО"), 
                                    React.createElement("th", null, "Пользователь"), 
                                    React.createElement("th", null, "Почта")
                                )
                            ), 
                            React.createElement("tbody", null, 
                                this.state.data.doc.users.length != 0 ? 
                                    this.state.data.doc.users.map(function(elem, index) {
                                        return (
                                            React.createElement("tr", {onClick: editUsers.bind(self, index), 
                                                key: elem.key}, 
                                                React.createElement("td", null, elem.fio), 
                                                React.createElement("td", null, elem.login), 
                                                React.createElement("td", null, elem.mail)
                                            )
                                        )
                                    }) : ""
                            )
                        )
                    )
                )
            )
        )
    }
});


MainCompanyInfo = React.createClass({displayName: "MainCompanyInfo",
    getInitialState: function() {
        return {data: {id: 0,
                        doc: new model.company()}
                };
    },
    setElem: function(elem, callback) {
        this.setState({data: elem});
        this.callbackSetElem = callback;
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        this.state.data.doc.primary.key = root.key(this.state.data.doc.primary.name);
        this.callbackSetElem(this.state.data);
        main_company_ctrl.edit(this.state.data);
        $('#MainCompanyInfoModal').modal('hide');
    },
    del: function() {
        main_company_ctrl.del(this.state.data.id);
    },
    componentDidMount: function() {
        main_company_ctrl.setMainCompanyInfo = this.setElem;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainCompanyInfoModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w660"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, "Редактирование компании")
                        ), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Название"), 
                                        React.createElement("input", {type: "text", 
                                                name: "name", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.name})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Контактное лицо"), 
                                        React.createElement("input", {type: "text", 
                                                name: "fio", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.fio})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон 1"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone1", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.phone1})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон 2"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone2", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.phone2})
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Почта"), 
                                        React.createElement("input", {type: "text", 
                                                name: "mail", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.mail})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Сайт"), 
                                        React.createElement("input", {type: "text", 
                                                name: "url", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.doc.primary.url})
                                    )
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", onDoubleClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.save, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});


MainCompanyObject = React.createClass({displayName: "MainCompanyObject",
    getInitialState: function() {
        return {data: new model.object(),
                head: "Добавить договор",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                elem: "",
                index: "",
                type_check: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(type_check, val, head) {
        this.state.data.works_type = ['','',''];
        this.state.data.works_type[type_check] = val;
        this.state.type_check = type_check;
        var name = this.state.data.object_name.replace(/(ТО)/g,"");
        var name = name.replace(/ВиК/g,"");
        var name = name.replace(/Кс/g,"");
        var name = name.replace(/КСОБ/g,"");
        this.state.data.object_name = name + head;
        this.forceUpdate();
    },
    add: function(elem, callback) {
        this.setState({data: new model.object(),
                        head: "Добавить договор",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        elem: elem,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyObjectModal').modal('show');
    },
    edit: function(elem, callback, index) {
        this.setState({data: elem.doc.object[index],
                        head: "Редактировать договор",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        elem: elem,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyObjectModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.type_check == '') {
            this.setState({veri_view_content: "Нужно указать тип работ по договору!"});
            this.setState({veri_view: true});
        } else {
            /*var duble_object = false;
            for(var i in this.state.elem.doc.object) {
                if(this.state.elem.doc.object[i].works_type[this.state.type_check] == "checked" && this.state.index != i) {
                    duble_object = true;
                }  
            }
            if(duble_object == true) {
                this.setState({veri_view_content: "Такой договор уже существует!"});
                this.setState({veri_view: true}); 
            } else {
                this.setState({veri_view: false});
                this.save();
            }*/
            this.save();
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.object.push(this.state.data);
                    break;
            case "edit":   
                this.state.elem.doc.object[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyObjectModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.object.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyObjectModal').modal('hide');
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyObject = this.add;
        main_company_ctrl.editCompanyObject = this.edit;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainCompanyObjectModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w660"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.head)
                        ), 
                        React.createElement("div", {className: "veri-modal", style: this.state.veri_view == true ? {display: ''} : {display: 'none'}}, 
                            this.state.veri_view_content), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Договор"), 
                                        React.createElement("input", {type: "text", 
                                                name: "object_name", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.object_name})
                                    ), 
                                    React.createElement(InputTextDate, {
                                        head: "Дата заключения договора", 
                                        name: "contract_date", 
                                        val: this.state.data.contract_date, 
                                        bind: this.inputBind}), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Цена"), 
                                        React.createElement("input", {type: "text", 
                                                name: "contract_price", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.contract_price})
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("label", null, "Типы работ"), 
                                    React.createElement("ul", {className: "wrap-check"}, 
                                        React.createElement(InputCheck, {head: "ТО", 
                                                    val: this.state.data.works_type[0], 
                                                    num_check: 0, 
                                                    bind: this.checkGroupBind}), 
                                        React.createElement(InputCheck, {head: "ВиК", 
                                                    val: this.state.data.works_type[1], 
                                                    num_check: 1, 
                                                    bind: this.checkGroupBind}), 
                                        React.createElement(InputCheck, {head: "КСОБ", 
                                                    val: this.state.data.works_type[2], 
                                                    num_check: 2, 
                                                    bind: this.checkGroupBind})
                                    )
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: this.state.view_del == true ? {display: ''} : {display: 'none'}, 
                                onDoubleClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.veriForm, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});

MainCompanyAddress = React.createClass({displayName: "MainCompanyAddress",
    getInitialState: function() {
        return {data: new model.address(),
                head: "Добавить адрес",
                view_del: false,
                save_type: "add",
                elem: "",
                current_object: "",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        this.state.data[name] = val;
        this.forceUpdate();
    },
    add: function(elem, current_object, callback) {
        this.setState({data: new model.address(),
                        head: "Добавить адрес",
                        view_del: false,
                        save_type: "add",
                        elem: elem,
                        current_object: current_object,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyAddressModal').modal('show');
    },
    edit: function(elem, current_object, callback, index) {
        this.setState({data: elem.doc.object[current_object].address[index],
                        head: "Редактировать адрес",
                        view_del: true,
                        save_type: "edit",
                        elem: elem,
                        current_object: current_object,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyAddressModal').modal('show');
    },
    save: function() {        
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.object[this.state.current_object].address.push(this.state.data);
                    break;
            case "edit":   
                this.state.elem.doc.object[this.state.current_object].address[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyAddressModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.object[this.state.current_object].address.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyAddressModal').modal('hide');
    },
    pre_save: function() {
        var self = this;
        root.geocode(this.state.data.address, function(data) {
            if(data.response.GeoObjectCollection.featureMember[0] != undefined) {
                self.state.data.coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
            }
            self.forceUpdate();
            self.save();
        });
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyAddress = this.add;
        main_company_ctrl.editCompanyAddress = this.edit;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainCompanyAddressModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.head)
                        ), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement(InputKladr, {name: "address", 
                                        val: this.state.data.address, 
                                        bind: this.inputBind, 
                                        set: this.selectBind})
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: this.state.view_del == true ? {display: ''} : {display: 'none'}, 
                                onDoubleClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.pre_save, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});


MainCompanyUsers = React.createClass({displayName: "MainCompanyUsers",
    getInitialState: function() {
        return {data: new model.users(),
                head: "Добавить сотрудника",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                elem: "",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(num_check, val) {
        this.state.data.works_type[num_check] = val;
        this.forceUpdate();
    },
    add: function(elem, callback) {
        this.setState({data: new model.users(),
                        head: "Добавить сотрудника",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        elem: elem,
                        index: ""});
        this.callbackSetElem = callback;
        $('#MainCompanyUsersModal').modal('show');
    },
    edit: function(elem, callback, index) {
        this.setState({data: elem.doc.users[index],
                        head: "Редактировать сотрудника",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        elem: elem,
                        index: index});
        this.callbackSetElem = callback;
        $('#MainCompanyUsersModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.login == '' || this.state.data.mail == '') {
            this.setState({veri_view_content: "Нужно указать пользователя и почту!"});
            this.setState({veri_view: true});
        } else {
            this.state.data.key = root.key(this.state.data.login + this.state.data.mail);
            var double_key = false;
            for(var i in this.state.elem.doc.users) {
                if(this.state.elem.doc.users[i].key == this.state.data.key) {
                    double_key = true;
                };
            };
            if(double_key == true) {
                this.setState({veri_view_content: "Пользователь с идентичным паролем и почтой уже существует!"});
                this.setState({veri_view: true});
            } else {
                this.setState({veri_view: false});
                this.save();
            }
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                this.state.elem.doc.users.push(this.state.data);
                root.send_mail(this.state.data.mail, "Вы добавленны в систему BSG-Online. Ваш логин: " + 
                                                        this.state.data.login + " Ваш пароль: " + 
                                                        this.state.data.mail + " Адрес системы http://bsg-online.ru");
                    break;
            case "edit":   
                this.state.elem.doc.users[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyUsersModal').modal('hide');
    },
    del: function() {
        this.state.elem.doc.users.splice(this.state.index, 1);
        this.callbackSetElem(this.state.elem);
        main_company_ctrl.edit(this.state.elem);
        $('#MainCompanyUsersModal').modal('hide');
    },
    componentDidMount: function() {
        main_company_ctrl.addCompanyUsers = this.add;
        main_company_ctrl.editCompanyUsers = this.edit;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainCompanyUsersModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w660"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.head)
                        ), 
                        React.createElement("div", {className: "veri-modal", style: this.state.veri_view == true ? {display: ''} : {display: 'none'}}, 
                            this.state.veri_view_content), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement(InputEng, {head: "Пользователь", 
                                        name: "login", 
                                        val: this.state.data.login, 
                                        bind: this.inputBind}), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "ФИО"), 
                                        React.createElement("input", {type: "text", 
                                                name: "fio", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.fio})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Должность"), 
                                        React.createElement("input", {type: "text", 
                                                name: "post", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.post})
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.phone})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Почта"), 
                                        React.createElement("input", {type: "text", 
                                                name: "mail", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.mail})
                                    )
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: this.state.view_del == true ? {display: ''} : {display: 'none'}, 
                                onDoubleClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.veriForm, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});

CompanyMenu = React.createClass({displayName: "CompanyMenu",
    getInitialState: function() {
        return  {user: root.user.login};
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("nav", {className: "navbar navbar-default", role: "navigation"}, 
                    React.createElement("div", {className: "navbar-header"}, 
                        React.createElement("a", {className: "navbar-brand"}, 
                            React.createElement("img", {alt: "", src: "../styles/images/logo.png"}), 
                            React.createElement("span", {id: "sidebar-exp"})
                        )
                    ), 
                    React.createElement("div", {className: "collapse navbar-collapse navbar-ex1-collapse"}, 
                        React.createElement("ul", {className: "nav navbar-nav navbar-right header-drop-right"}, 
                            React.createElement("li", {className: "dropdown user-dropdown-one"}, 
                                React.createElement("a", {className: "dropdown-toggle", "data-toggle": "dropdown", href: "#"}, 
                                    this.state.user, 
                                    React.createElement("img", {className: "img-user", alt: "", src: "../styles/images/user-img.png"}), 
                                    React.createElement("b", {className: "caret"})
                                ), 
                                React.createElement("ul", {className: "dropdown-menu user-dropdown-two"}, 
                                    React.createElement("li", null, 
                                        React.createElement("a", {href: "#/guide"}, "Инструкции")
                                    ), 
                                    React.createElement("li", {onClick: this.logOut, className: "log-out"}, 
                                        React.createElement("a", null, "Выход")
                                    )
                                )
                            )
                        )
                    )
                ), 
                React.createElement("div", {id: "wrapper", className: ""}, 
                    React.createElement("div", {id: "body-overlay"}), 
                    React.createElement("div", {id: "sidebar"}, 
                        React.createElement("ul", {className: "drop-area"}, 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/new_order"}, "Новая заявка")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/all_order"}, "Все заявки")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/company"}, "Компании")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/users"}, "Пользователи")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/list"}, "Справочники")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/report"}, "Отчёты")
                            )
                        )
                    ), 
                    React.createElement("div", {id: "wrap"}, 
                        React.createElement("div", {className: "container"}, 
                        
                        React.createElement(RouteHandler, null)
                        )
                    )
                ), 
                React.createElement("div", {id: "footer"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-6 left"}, 
                            React.createElement("ul", null, 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Раз"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: ""}, "Два"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Три")
                                )
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6 right"}, 
                            React.createElement("span", {className: "pull-right"}, 
                                "Разработка", 
                                React.createElement("a", {href: "ordo-crm.ru"}, 
                                    React.createElement("strong", null, " Ordo-CRM ")
                                ), 
                                "2015"
                            )
                        )
                    )
                )
            )
        );
    }
});   

MainGuide = React.createClass({displayName: "MainGuide",
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                        React.createElement(MainGuideList, null)
                )
            )    
        )
    }
});

MainGuideList = React.createClass({displayName: "MainGuideList",
    render: function() {
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Инструкции"), 
                React.createElement("div", {className: "timeline-item"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-6"}, 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Добавление внутреннего пользователя")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/XlYJXEGfchY", frameBorder: "0", allowFullScreen: true})
                            ), 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Редактирование компании")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/37_SZepYmRM", frameBorder: "0", allowFullScreen: true})
                            ), 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Добавление адресов в компанию")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/SNwCM1EEhDk", frameBorder: "0", allowFullScreen: true})
                            ), 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Добавление внешнего пользователя в компанию")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/521Pzu_A190", frameBorder: "0", allowFullScreen: true})
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6"}, 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Добавление компании")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/sqitFi1fwIM", frameBorder: "0", allowFullScreen: true})
                            ), 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Добавление договоров в компанию")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/F7cQw81zhac", frameBorder: "0", allowFullScreen: true})
                            ), 
                            React.createElement("div", {className: "comment panel-style extra"}, 
                                React.createElement("p", null, React.createElement("a", null, "Удаление компании и связной информации")), 
                                React.createElement("div", {className: "pb10"}), 
                                React.createElement("iframe", {width: "100%", height: "300px", src: "https://www.youtube.com/embed/6MQh-tIVTJw", frameBorder: "0", allowFullScreen: true})
                            )
                        )
                    )
                )
            )    
        )
    }
});



Lists = React.createClass({displayName: "Lists",
    componentDidMount: function() {

    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(ListsMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        
                        React.createElement(RouteHandler, null)
                    )
                )
            )    
        )
    }
});


ListsMenu = React.createClass({displayName: "ListsMenu",
    render: function() {
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "СПРАВОЧНИКИ"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {className: "sourse", href: "#/lists/sourse"}, 
                            React.createElement("span", null, "Источники"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {className: "tasks", href: "#/lists/sourse2"}, 
                            React.createElement("span", null, "Источники 2"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});
Menu = React.createClass({displayName: "Menu",
    getInitialState: function() {
    return  {
                class_menu: {Заявки:'active item', 
                            Отчёты:'item', 
                            Справочники:'item'},
                user: root.user.login
            };
    },
    setClass: function(event) {
        this.state.class_menu = {Заявки:'item', 
                            Отчёты:'item', 
                            Справочники:'item'}
        this.state.class_menu[event.target.text] = "active item";
        this.forceUpdate();
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("nav", {className: "navbar navbar-default", role: "navigation"}, 
                    React.createElement("div", {className: "navbar-header"}, 
                        React.createElement("a", {className: "navbar-brand"}, 
                            React.createElement("img", {alt: "", src: "../styles/images/logo.png"}), 
                            React.createElement("span", {id: "sidebar-exp"})
                        )
                    ), 
                    React.createElement("div", {className: "collapse navbar-collapse navbar-ex1-collapse"}, 
                        React.createElement("ul", {className: "nav navbar-nav navbar-right header-drop-right"}, 
                            React.createElement("li", {className: "dropdown user-dropdown-one"}, 
                                React.createElement("a", {className: "dropdown-toggle", "data-toggle": "dropdown", href: "#"}, 
                                    this.state.user, 
                                    React.createElement("img", {className: "img-user", alt: "", src: "../styles/images/user-img.png"}), 
                                    React.createElement("b", {className: "caret"})
                                ), 
                                React.createElement("ul", {className: "dropdown-menu user-dropdown-two"}, 
                                    React.createElement("li", null, 
                                        React.createElement("a", {href: "/guide"}, "Инструкции")
                                    ), 
                                    React.createElement("li", {onClick: this.logOut, className: "log-out"}, 
                                        React.createElement("a", null, "Выход")
                                    )
                                )
                            )
                        )
                    )
                ), 
                React.createElement("div", {id: "wrapper", className: ""}, 
                    React.createElement("div", {id: "body-overlay"}), 
                    React.createElement("div", {id: "sidebar"}, 
                        React.createElement("ul", {className: "drop-area"}, 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/order"}, "Заявки")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/lists"}, "Справочники")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/report"}, "Отчёты")
                            )
                        )
                    ), 
                    React.createElement("div", {id: "wrap"}, 
                        React.createElement("div", {className: "container"}, 
                        
                        React.createElement(RouteHandler, null)
                        )
                    )
                ), 
                React.createElement("div", {id: "footer"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-6 left"}, 
                            React.createElement("ul", null, 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Раз"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: ""}, "Два"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Три")
                                )
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6 right"}, 
                            React.createElement("span", {className: "pull-right"}, 
                                "Разработка", 
                                React.createElement("a", {href: "ordo-crm.ru"}, 
                                    React.createElement("strong", null, " Ordo-CRM ")
                                ), 
                                "2015"
                            )
                        )
                    )
                )
            )
        );
    }
});   


/*
    <ul className="header-notifications pull-right">
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>
*/

/*
Суб меню
<li class="">
UI Elements
<ul style="display: none;" class="">
<li>
<span class="icn icon-190"></span>
<a href="ui-sliders.html">UI Sliders</a>
</li>

*/ 

/*
поиск
<div class="search-block clearfix">
<span></span>
<input type="text" placeholder="Are you looking for something?">
</div>

*/


/*<ul className="header-notifications pull-right">
                            <li>
                                <a className="clock" href="#"></a>
                            </li>
                            <li>
                                <a className="task" href="#"></a>
                            </li>
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>*/

/*
<div id="main" className="z-depth-1">
              <div id="header">
              </div>
              <div className="ui inverted db1 menu">
                <a onClick={this.setClass} className={this.state.class_menu["Заявки"]} href="#/order">Заявки</a>
                <a onClick={this.setClass} className={this.state.class_menu["Отчёты"]}>Отчёты</a>
                <a onClick={this.setClass} className={this.state.class_menu["Справочники"]} href="#/lists">Справочники</a>
                <div className="right menu">
                    <div className="item blue">{this.state.user}</div>
                    <div onClick={this.logOut} 
                        className="item piont"><i className="power icon orange"></i></div>
                </div>
              </div>
              {}
              <RouteHandler/>
            </div>
*/
Sourse = React.createClass({displayName: "Sourse",
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(FormSourse, null), 
                React.createElement(SourseList, null)
            )    
        )
    }
});

FormSourse = React.createClass({displayName: "FormSourse",
    getInitialState: function() {
        return {sourse: "",
                header: "Новый источник",
                type: "add",
                id: "",
                delButton: "none"};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                ctrl_sourse.add(this.state.sourse); 
                    break;
            case "edit":
                ctrl_sourse.edit(this.state.sourse, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({sourse: "", 
                        header: "Новый источник", 
                        type: "add",
                        delButton: "none"});
        $('#SourseModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({sourse: elem.doc.name, 
                        id: elem.id, 
                        header: "Редактирование источника", 
                        type: "edit",
                        delButton: ""});
        $('#SourseModal').modal('show');
    },
    del: function() {
        ctrl_sourse.del(this.state.id);
    },
    componentDidMount: function() {
        ctrl_sourse.addForm = this.addForm;
        ctrl_sourse.editForm = this.editForm;
    },
    render: function() {
        return (
            React.createElement("div", {id: "SourseModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w400"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.header)
                        ), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "input-f-wrapper"}, 
                                React.createElement("label", null, "Название источника"), 
                                React.createElement("input", {type: "text", 
                                        name: "sourse", 
                                        onChange: this.inputBind, 
                                        value: this.state.sourse})
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: {display: this.state.delButton}, 
                                onClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.save, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )
    }
});

SourseList = React.createClass({displayName: "SourseList",
	getInitialState: function() {
        return {data: ctrl_sourse.store,
                request: {
                    name: "sourse", 
                    limit: root.options.limit,
                    offset: 0,
                    by: "name",
                },
                display: {
                    prev_button: "none",
                    next_botton: ""
                }};
    },
    ctrl: function() {
    	ctrl_sourse.setViewList = this.update;
        ctrl_sourse.request_data = this.state.request;
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        ctrl_sourse.update("change_limit");
    },
    stepPrev: function() {
        ctrl_sourse.update("step_prev");
    },
    stepNext: function() {
        ctrl_sourse.update("step_next");
    },
    add: function() {
        ctrl_sourse.addForm();
    },
    edit: function(elem) {
        ctrl_sourse.editForm(elem);
    },
    render: function() {
    	this.ctrl(); 
        var edit = this.edit, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Источники ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", onClick: this.add}, "Добавить")
                ), 
                React.createElement("table", {className: "table simple"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "Название источника")
                        )
                    ), 
                    React.createElement("tbody", null, 
                        this.state.data.data.map(function(elem) {
                            return (
                                React.createElement("tr", {key: elem.id, onClick: edit.bind(self, elem)}, 
                                    React.createElement("td", null, elem.doc.name)
                                )
                            )
                        })
                    )
                ), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "В перёд")
                    )
                )
            ) 
        )
    }
});
MainUsers = React.createClass({displayName: "MainUsers",
    componentDidMount: function() {

    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(MainUsersMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        React.createElement(MainUsersForm, null), 
                        React.createElement(MainUsersList, null)
                    )
                )
            )    
        )
    }
});



MainUsersMenu = React.createClass({displayName: "MainUsersMenu",
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_user_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "ФИЛЬТР"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "admin"), 
                            className: this.state.filter == "admin" ? "admin filter_active" : "admin"}, 
                            React.createElement("span", null, "Администраторы"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "master"), 
                            className: this.state.filter == "master" ? "master filter_active" : "master"}, 
                            React.createElement("span", null, "Инженеры"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "all"), 
                            className: this.state.filter == "all" ? "all filter_active" : "all"}, 
                            React.createElement("span", null, "Все"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});


MainUsersForm = React.createClass({displayName: "MainUsersForm",
    getInitialState: function() {
        return {doc: new model.main_user(),
                header: "Новый пользователь",
                type: "add",
                id: "",
                veri_view: false,
                veri_view_content: "",
                delButton: "none",
                role: select.role};
    },
    inputBind: function(event) {
        this.state.doc[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        console.log(val);
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    veriForm: function() {
        if(this.state.doc.login == '' || this.state.doc.pass == '' || this.state.doc.mail == '') {
            this.setState({veri_view_content: "Нужно указать логин, почту и пароль!"});
            this.setState({veri_view: true});
        } else {
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                main_user_ctrl.add(this.state.doc); 
                    break;
            case "edit":
                main_user_ctrl.edit(this.state.doc, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({doc: new model.main_user(), 
                        header: "Новый пользователь", 
                        type: "add",
                        id: "",
                        veri_view: false,
                        veri_view_content: "",
                        delButton: "none"});
        $('#MainUsersModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({doc: elem.doc, 
                        id: elem.id, 
                        header: "Редактирование пользователя", 
                        veri_view: false,
                        veri_view_content: "",
                        type: "edit",
                        delButton: ""});
        $('#MainUsersModal').modal('show');
    },
    del: function() {
        main_user_ctrl.del(this.state.id);
    },
    componentDidMount: function() {
        main_user_ctrl.addForm = this.addForm;
        main_user_ctrl.editForm = this.editForm;
    },
    render: function() {
        return (
            React.createElement("div", {id: "MainUsersModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w700"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.header)
                        ), 
                        React.createElement("div", {className: "veri-modal", style: this.state.veri_view == true ? {display: ''} : {display: 'none'}}, 
                            this.state.veri_view_content), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement(InputEng, {name: "login", 
                                                head: "Логин", 
                                                bind: this.inputBind, 
                                                val: this.state.doc.login}), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "ФИО"), 
                                        React.createElement("input", {type: "text", 
                                                name: "fio", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.fio})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Роль"), 
                                            React.createElement(InputSelect, {name: "role", 
                                                        val: this.state.doc.role, 
                                                        bind: this.selectBind, 
                                                        data: this.state.role})
                                    )
                                    
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement(InputPhone, {name: "phone", 
                                                head: "Телефон", 
                                                bind: this.inputBind, 
                                                val: this.state.doc.phone}), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Почта"), 
                                        React.createElement("input", {type: "text", 
                                                name: "mail", 
                                                onChange: this.inputBind, 
                                                value: this.state.doc.mail})
                                    ), 
                                    React.createElement(InputEng, {name: "pass", 
                                                head: "Пароль", 
                                                bind: this.inputBind, 
                                                val: this.state.doc.pass})
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: {display: this.state.delButton}, 
                                onClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.veriForm, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )
    }
});

MainUsersList = React.createClass({displayName: "MainUsersList",
    getInitialState: function() {
        return {data: main_user_ctrl.store,
                request: jQuery.extend(true, {}, main_user_ctrl.default_request_data),
                display: jQuery.extend(true, {}, main_user_ctrl.display_step)};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        main_user_ctrl.update("change_limit");
    },
    stepPrev: function() {
        main_user_ctrl.update("step_prev");
    },
    stepNext: function() {
        main_user_ctrl.update("step_next");
    },
    add: function() {
        main_user_ctrl.addForm();
    },
    edit: function(elem) {
        main_user_ctrl.editForm(elem);
    },
    componentDidMount: function() {
        main_user_ctrl.setViewList = this.update;
        main_user_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Пользователи ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", onClick: this.add}, "Добавить")
                ), 
                React.createElement("table", {className: "table simple"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "Пользователь"), 
                            React.createElement("th", null, "Логин"), 
                            React.createElement("th", null, "Роль")
                        )
                    ), 
                    React.createElement("tbody", null, 
                        this.state.data.data.map(function(elem) {
                            return (
                                    React.createElement("tr", {key: elem.id, onClick: edit.bind(self, elem)}, 
                                        React.createElement("td", null, elem.doc.fio), 
                                        React.createElement("td", null, elem.doc.login), 
                                        React.createElement("td", null, elem.doc.role)
                                    )
                            )
                        })
                    )
                ), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "В перёд")
                    )
                )
            ) 
        )
    }
});
NewOrder = React.createClass({displayName: "NewOrder",
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement(NewOrderMaster, null), 
                React.createElement(MainCompanyUsers, null)
            )    
        )
    }
});

NewOrderMaster = React.createClass({displayName: "NewOrderMaster",
    getInitialState: function() {
        return {master_step: 0,
                search: "",
                search_user: "",
                search_address: "",
                company: [],
                selected_company: "",
                company_full_address: [],
                company_all_address: [],
                selected_address: "",
                selected_user: "",
                selected_user_index: "",
                reason: "",
                right_contract: [],
                selected_contract: "",
                order: new model.order(),
                save: null};
    },
    resetMaster: function() {
        this.replaceState(this.getInitialState());
    },
    stepMaster: function(step) {
        this.setState({master_step: step});
    },
    searchBind: function(event) {
        this.setState({search: event.target.value});
        new_order_ctrl.company_request(event.target.value);
    },
    setCompanyList: function(data) {
        this.setState({company: data});
    },
    selectCompany: function(elem) {
        this.state.selected_company = elem;
        this.state.company_full_address = _.uniq(_.flatten(_.pluck(this.state.selected_company.doc.object, 'address')));
        this.state.company_all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.selected_company.doc.object, 'address')), 'address'));
        this.forceUpdate();
        this.stepMaster(1);  
    },
    searchAddress: function(event) {
        this.setState({search_address: event.target.value});
    },
    selectAllAddress: function(address) {
        for(var i in this.state.company_full_address) {
            if(this.state.company_full_address[i].address == address) {
                this.state.selected_address = this.state.company_full_address[i];
            }
        }
        if(this.state.selected_company.doc.users[this.state.selected_user_index].my_address.indexOf(this.state.selected_address.address) == -1) {
            this.state.selected_company.doc.users[this.state.selected_user_index].my_address.push(this.state.selected_address.address);
        };
        main_company_ctrl.edit(this.state.selected_company);
        this.stepMaster(3);
    },
    selectMyAddress: function(elem) {
        for(var i in this.state.company_full_address) {
            if(this.state.company_full_address[i].address == elem) {
                this.state.selected_address = this.state.company_full_address[i];
            }
        }
        this.stepMaster(3);
    },
    searchUser: function(event) {
        this.setState({search_user: event.target.value});
    },
    selectUser: function(elem, index) {
        this.state.selected_user = elem;
        this.state.selected_user_index = index;
        this.stepMaster(2);
    },
    addUser: function() {
        main_company_ctrl.addCompanyUsers(this.state.selected_company, this.updateCompany);
    },
    bindReason: function(event) {
        this.setState({reason: event.target.value});
    },
    updateCompany: function(elem) {
        this.setState({selected_company: elem});
    },
    getContract: function() {
        this.state.right_contract = new_order_ctrl.get_contract_in_address(this.state.selected_company.doc, this.state.selected_address.address);
        this.forceUpdate();
        if(this.state.right_contract.length == 1) {
            this.state.selected_contract = this.state.right_contract[0];
            this.getCountOrder();
        } else {
            this.stepMaster(4);
        }
    },
    selectContract: function(elem) {
        this.state.selected_contract = elem;
        this.getCountOrder();
    },
    getCountOrder: function() {
        new_order_ctrl.get_order_user_number(this.state.selected_user.key);
    },
    setResult: function(count) {
        this.state.order.system.key = this.state.selected_company.id + "/" + Number(this.state.selected_user_index + 1) + "/" + Number(count + 1);
        this.state.order.system.company_key = this.state.selected_company.doc.primary.key;
        this.state.order.system.persone_key = this.state.selected_company.doc.users[this.state.selected_user_index].key;
        this.state.order.system.add_date = format.date_ms();
        this.state.order.primary.status = 0;
        this.state.order.primary.company = this.state.selected_company.doc.primary.name;
        this.state.order.primary.persone = this.state.selected_company.doc.users[this.state.selected_user_index].fio;
        this.state.order.primary.phone = this.state.selected_company.doc.users[this.state.selected_user_index].phone;
        this.state.order.primary.mail = this.state.selected_company.doc.users[this.state.selected_user_index].mail;
        this.state.order.primary.contract = this.state.selected_contract.object_name;
        this.state.order.primary.address = this.state.selected_address.address;
        this.state.order.primary.coords = this.state.selected_address.coords;
        this.state.order.primary.type = new_order_ctrl.get_contract_type(this.state.selected_contract.works_type);
        this.state.order.primary.reason = this.state.reason;

        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                root.send_mail("9219505184@mail.ru", "Новая заявка");
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                root.send_mail("9312323303@mail.ru", "Новая заявка");
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                root.send_mail("9213005737@mail.ru", "Новая заявка");
                    break;
        }
        this.stepMaster(5);
    },
    saveOrder: function() {
        var self = this;
        new_order_ctrl.add_order(this.state.order, function(data) {
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
    },
    errorSave: function() {
        this.setState({save: false});
    },
    componentDidMount: function() {
        new_order_ctrl.set_company_list = this.setCompanyList;
        new_order_ctrl.set_result = this.setResult;
    },
    render: function() {
        self = this, selectCompany = this.selectCompany, selectMyAddress = this.selectMyAddress, 
        selectAllAddress = this.selectAllAddress, selectUser = this.selectUser, selectContract = this.selectContract, order = this.state.order;
        return (
            React.createElement("div", {className: "panel-style space m-top-0 ml510"}, 
                React.createElement("h3", {className: "heading-title"}, "Создание новой заявки"), 
                React.createElement("div", {className: "type-four-tab-wrapper"}, 
                    React.createElement("ul", {className: "nav nav-tabs easy-style type-four"}, 
                        React.createElement("li", {className: this.state.master_step == 0 ? "active" : ""}, 
                            React.createElement("a", null, "1. Найдите компанию")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 1 ? "active" : ""}, 
                            React.createElement("a", null, "2. Укажите контактное лицо")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 2 ? "active" : ""}, 
                            React.createElement("a", null, "3. Укажите адрес")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 3 ? "active" : ""}, 
                            React.createElement("a", null, "4. Опишите проблему")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 4 ? "active" : ""}, 
                            React.createElement("a", null, "5. Укажите договор")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 5 ? "active" : ""}, 
                            React.createElement("a", null, "Результат")
                        )
                    ), 

                    React.createElement("div", {className: "tab-content easy-style type-four"}, 
                        React.createElement("div", {className: this.state.master_step == 0 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("div", {className: "wrap-company-input"}, 
                                            React.createElement("input", {type: "text", 
                                                    name: "name", 
                                                    placeholder: "Поиск по названию компании", 
                                                    onChange: this.searchBind, 
                                                    value: this.state.search}), 
                                            React.createElement("a", null)
                                        )
                                    )
                                )
                            ), 
                            React.createElement("table", {className: "table simple", style: this.state.company.length != 0 ? {display: ''} : {display: 'none'}}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Найденные компании")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.company.map(function(elem, index) {
                                        return (
                                            React.createElement("tr", {key: elem.doc.primary.key}, 
                                                React.createElement("td", null, elem.doc.primary.name, 
                                                    React.createElement("a", {className: "btn simple green_h_t right", 
                                                        onClick: selectCompany.bind(self, elem)}, "Выбрать")
                                                )
                                            )
                                        )
                                    })
                                )
                            ), 
                            React.createElement("p", {style: this.state.company.length == 0 ? {display: ''} : {display: 'none'}}, 
                                "Совпадений не найденно!")
                        ), 
                        React.createElement("div", {className: this.state.master_step == 1 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("div", {className: "wrap-user-input"}, 
                                            React.createElement("input", {type: "text", 
                                                    name: "name", 
                                                    placeholder: "Поиск контактного лица", 
                                                    onChange: this.searchUser, 
                                                    value: this.state.search_user}), 
                                            React.createElement("a", null)
                                        )
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("h3", {className: "heading-title"}, 
                                        React.createElement("a", {className: "btn blue right", onClick: this.addUser}, "Добавить контактное лицо")
                                    )
                                )
                            ), 
                            React.createElement("table", {className: "table simple", style: this.state.selected_company != "" ? {display: ''} : {display: 'none'}}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Зарегистрированные пользователи"), 
                                        React.createElement("th", null, "Телефон"), 
                                        React.createElement("th", null)
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.selected_company != "" ? 
                                        this.state.selected_company.doc.users.map(function(elem, index) {
                                            if(elem.fio.toLowerCase().indexOf(self.state.search_user.toLowerCase()) != -1) {
                                                return (
                                                    React.createElement("tr", {key: elem.key}, 
                                                        React.createElement("td", null, elem.fio), 
                                                        React.createElement("td", null, elem.phone), 
                                                        React.createElement("td", null, React.createElement("a", {className: "btn simple green_h_t right", 
                                                                onClick: selectUser.bind(self, elem, index)}, "Выбрать")
                                                        )
                                                    )
                                                )
                                            }   
                                    }) : ''
                                )
                            ), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 0)}, "Назад")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 2 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("table", {className: "table simple", style: this.state.selected_user != "" ? {display: ''} : {display: 'none'}}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Предыдущие адреса заказчика")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.selected_user != "" ? 
                                        this.state.selected_user.my_address.map(function(elem, index) {
                                            return (
                                                React.createElement("tr", {key: index}, 
                                                    React.createElement("td", null, elem, 
                                                        React.createElement("a", {className: "btn simple green_h_t right", 
                                                            onClick: selectMyAddress.bind(self, elem)}, "Выбрать")
                                                    )
                                                )
                                            )
                                    }) : ''
                                )
                            ), 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("div", {className: "wrap-location-input"}, 
                                            React.createElement("input", {type: "text", 
                                                    name: "name", 
                                                    placeholder: "Поиск по всем адресам", 
                                                    onChange: this.searchAddress, 
                                                    value: this.state.search_address}), 
                                            React.createElement("a", null)
                                        )
                                    )
                                )
                            ), 
                            React.createElement("table", {className: "table simple", style: this.state.company_all_address.length != 0 ? {display: ''} : {display: 'none'}}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Все зарегистрированные адреса компании")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.company_all_address.length != 0 ? 
                                        this.state.company_all_address.map(function(elem, index) {
                                            if(elem.toLowerCase().indexOf(self.state.search_address.toLowerCase()) != -1) {
                                                return (
                                                    React.createElement("tr", {key: index}, 
                                                        React.createElement("td", null, elem, 
                                                            React.createElement("a", {className: "btn simple green_h_t right", 
                                                                onClick: selectAllAddress.bind(self, elem)}, "Выбрать")
                                                        )
                                                    )
                                                )
                                            }    
                                    }) : ''
                                )
                            ), 
                            React.createElement("p", {style: this.state.company_all_address.length == 0 ? {display: ''} : {display: 'none'}}, 
                                "Нет зарегистрированных адресов!"), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 1)}, "Назад")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 3 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("div", {className: "textarea-i-wrapper"}, 
                                React.createElement("textarea", {onChange: this.bindReason, 
                                        value: this.state.reason})
                            ), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 2)}, "Назад"), 
                                React.createElement("a", {className: "btn green right", onClick: this.getContract, 
                                    style: this.state.reason != "" ? {display: ''} : {display: 'none'}}, "Дальше")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 4 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("table", {className: "table simple"}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Существующие договора по текущему адресу")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.right_contract.length != 0 ? 
                                        this.state.right_contract.map(function(elem, index) {
                                            return (
                                                React.createElement("tr", {key: index}, 
                                                    React.createElement("td", null, elem.object_name, 
                                                        React.createElement("a", {className: "btn simple green_h_t right", 
                                                            onClick: selectContract.bind(self, elem)}, "Выбрать")
                                                    )
                                                )
                                            )
                                    }) : ''
                                )
                            ), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 3)}, "Назад")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 5 ? "tab-pane active" : "tab-pane"}, 
                             React.createElement("div", null, 
                                React.createElement("div", {className: "form-see", style: this.state.save == null ? {display: ''} : {display: 'none'}}, 
                                    React.createElement("span", {className: "grey-title"}, "Заказчик"), 
                                    React.createElement("ul", null, 
                                        React.createElement("li", null, order.primary.company), 
                                        React.createElement("li", null, order.primary.persone), 
                                        React.createElement("li", null, order.primary.phone)
                                    ), 
                                    React.createElement("div", {className: "separator"}), 
                                    React.createElement("span", {className: "grey-title"}, "Заявка"), 
                                    React.createElement("ul", null, 
                                        React.createElement("li", null, "№ ", order.system.key), 
                                        React.createElement("li", null, order.primary.contract), 
                                        React.createElement("li", null, order.primary.address), 
                                        React.createElement("li", null, order.primary.type), 
                                        React.createElement("li", {className: "blue_l_t"}, order.primary.reason)
                                    ), 
                                    React.createElement("div", {className: "separator"}), 
                                    React.createElement("div", {className: "master-footer"}, 
                                        React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 4)}, "Назад"), 
                                        React.createElement("a", {className: "btn green right", onClick: this.saveOrder}, "Отправить")
                                    )
                                ), 
                                React.createElement("div", {style: this.state.save == true ? {display: ''} : {display: 'none'}}, 
                                    React.createElement("p", null, "Заявка успешно добавлена!"), 
                                    React.createElement("div", {className: "master-footer"}, 
                                        React.createElement("a", {className: "btn green right", onClick: this.resetMaster}, "В начало")
                                    )
                                ), 
                                React.createElement("div", {style: this.state.save == false ? {display: ''} : {display: 'none'}}, 
                                    React.createElement("p", null, "Внимание заявка НЕ ДОБАВЛЕНА!")
                                )
                            )
                        )
                    )
                )
            )     
        )
    }
});



// Компонет вложенный но выполнен выделенно!
/*SeeOrderMaster = React.createClass({
    getInitialState: function() {
        return {master_step: 0,
                order: new model.order(),
                save: null};
    },
    setForma: function(name, val) {
        this.state.order.primary[name] =  val;
        this.forceUpdate();
    },
    backStep: function() {
        new_order_ctrl.backStep();
    },
    saveOrder: function() {
        var self = this;
        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                    break;
        }
        // Отправка почты!!!!
        console.log(this.state.order);
        new_order_ctrl.add_order(this.state.order, function(data) {
            console.log(data);
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
        order_ctrl.load();
    },
    errorSave: function() {
        this.setState({save: false});
    },
    resetMaster: function() {
        new_order_ctrl.resetMaster();
        this.setState({master_step: 0,
                        order: new model.order(),
                        save: null});
    },
    componentDidMount: function() {
        new_order_ctrl.setForma = this.setForma;
    },
    render: function() {
        var order = this.state.order;
        return (
                <div>
                    <div className="form-see" style={this.state.save == null ? {display: ''} : {display: 'none'}}>
                        <span className="grey-title">Заказчик</span>
                        <ul>
                            <li>{order.primary.company}</li>
                            <li>{order.primary.persone}</li>
                            <li>{order.primary.phone}</li>
                        </ul>
                        <div className="separator"></div>
                        <span className="grey-title">Заявка</span>
                        <ul>
                            <li>№ {order.system.key}</li>
                            <li>{order.primary.contract}</li>
                            <li>{order.primary.address}</li>
                            <li>{order.primary.type}</li>
                            <li className="blue_l_t">{order.primary.reason}</li>
                        </ul>
                        <div className="separator"></div> 
                        <div className="master-footer">
                            <a className="btn orange left" onClick={this.backStep}>Назад</a>
                            <a className="btn green right" onClick={this.saveOrder}>Отправить</a>
                        </div>
                    </div>
                    <div style={this.state.save == true ? {display: ''} : {display: 'none'}}>
                        <p>Заявка успешно отправлена!</p>
                        <div className="master-footer">
                            <a className="btn green right" onClick={this.resetMaster}>В начало</a>
                        </div>
                    </div>
                    <div style={this.state.save == false ? {display: ''} : {display: 'none'}}>
                        <p>Внимание заявка НЕ ОТПРАВЛЕНА!</p>
                    </div>
                </div>
        )
    }
});*/
AllOrder = React.createClass({displayName: "AllOrder",
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(AllOrderMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        React.createElement(AllOrderList, null), 
                        React.createElement(OrderElemEdit, null), 
                        React.createElement(OrderElemHistory, null)
                    )
                )
            )    
        )
    }
});



AllOrderMenu = React.createClass({displayName: "AllOrderMenu",
    getInitialState: function() {
        return {filter: "all",
                new_order: order_ctrl.data_count.new_order_count};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        order_ctrl.update(val);
    },
    setCount: function(val) {
        this.setState({new_order: val});
    },
    componentDidMount: function() {
        order_ctrl.setNewCount = this.setCount;
    },
    render: function() {
        var self = this;
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "ФИЛЬТР"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "new"), 
                            className: this.state.filter == "new" ? "new filter_active" : "new"}, 
                            React.createElement("span", null, "Новые"), React.createElement("span", {className: "count"}, this.state.new_order), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "job"), 
                            className: this.state.filter == "job" ? "car filter_active" : "car"}, 
                            React.createElement("span", null, "В работе"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "ок"), 
                            className: this.state.filter == "ок" ? "ckeck filter_active" : "ckeck"}, 
                            React.createElement("span", null, "Выполненые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "reject"), 
                            className: this.state.filter == "reject" ? "attention filter_active" : "attention"}, 
                            React.createElement("span", null, "Отклонённые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "close"), 
                            className: this.state.filter == "close" ? "close_0 filter_active" : "close_0"}, 
                            React.createElement("span", null, "Закрытые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "all"), 
                            className: this.state.filter == "all" ? "all filter_active" : "all"}, 
                            React.createElement("span", null, "Все"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});


AllOrderList = React.createClass({displayName: "AllOrderList",
    getInitialState: function() {
        return {data: order_ctrl.store,
                request: jQuery.extend(true, {}, order_ctrl.default_request_data),
                display: jQuery.extend(true, {}, order_ctrl.display_step),
                order_status: order_status};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        order_ctrl.update("change_limit");
    },
    stepPrev: function() {
        order_ctrl.update("step_prev");
    },
    stepNext: function() {
        order_ctrl.update("step_next");
    },
    add: function() {
        
    },
    edit: function(elem) {
        
    },
    componentDidMount: function() {
        order_ctrl.setViewList = this.update;
        order_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Заявки ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", href: "#/new_order", onClick: this.add}, "Добавить")
                ), 
                this.state.data.data.map(function(elem) {
                    return (
                        React.createElement(AllOrderElem, {key: elem.id, data: elem})
                    )
                }), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "Вперёд")
                    )
                )
            ) 
        )
    }
});

AllOrderElem = React.createClass({displayName: "AllOrderElem",
    getInitialState: function() {
        return {data: this.props.data,
                saveView: "none"};
    },
    bindStep: function(val) {
        this.state.data.doc.primary.status = val;
        // Системная история
        system_event[val].date = format.date_ms();
        this.state.data.doc.system_history.push(system_event[val]);
        // Уведомления
        switch(val) {
            case 3:
                root.send_mail(this.state.data.doc.primary.mail, "Ваша заявка №" + this.state.data.doc.system.key + " отклонена!");
                    break;
        }
        this.saveView();
    },
    saveView: function() {
        this.state.saveView = "";
        this.forceUpdate();
    },
    elemEdit: function() {
        order_ctrl.edit_order(this.state.data, function(data){
            if(data == 1) {
                order_ctrl.update('edit');
            }
        });
        this.setState({saveView: "none"});
    },
    setElem: function(elem) {
        this.setState({data: elem});
    },
    infoEdit: function(form) {
        order_ctrl.elem_edit(form, this.state.data, this.setElem);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var doc = this.state.data.doc, self = this;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "timeline-post"}, 
                    React.createElement("ul", null, 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 0), 
                            className: doc.primary.status == 0 ? "orange_active" : ""}, React.createElement("a", null, "Новая")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 1), 
                            className: doc.primary.status == 1 ? "green_active" : ""}, React.createElement("a", null, "В работе")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 2), 
                            className: doc.primary.status == 2 ? "blue_active" : ""}, React.createElement("a", null, "Выполнена")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 3), 
                            className: doc.primary.status == 3 ? "red_active" : ""}, React.createElement("a", null, "Отклонёна нами")), 
                        React.createElement("li", {onClick: this.bindStep.bind(self, 4), 
                            className: doc.primary.status == 4 ? "blue_active" : ""}, React.createElement("a", null, "Закрыта")), 
                        React.createElement("li", {className: "right m"}, React.createElement("a", {className: "white_t"}, format.sql_to_date_time(doc.system.add_date)))
                    ), 
                    React.createElement("div", {className: "info-block"}, 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view"}, doc.system.key), 
                            React.createElement("div", {className: "text_view"}, doc.primary.company), 
                            React.createElement("div", {className: "text_view"}, doc.primary.persone), 
                            React.createElement("div", {className: "text_view green_h_t"}, doc.primary.phone)
                        ), 
                        React.createElement("div", {className: "w250"}, 
                            React.createElement("div", {className: "text_view"}, doc.primary.type), 
                            React.createElement("div", {className: "text_view"}, doc.primary.address), 
                            React.createElement("div", {className: "text_view orange_h_t"}, doc.primary.reason)
                        ), 
                        React.createElement("div", {className: "textarea-i-wrapper"}, 
                            React.createElement("div", {className: "text_view"}, doc.master.fio)
                        )
                    ), 
                    React.createElement("div", {className: "options clearfix"}, 
                        React.createElement("ul", null, 
                            React.createElement("li", null, React.createElement("a", {onClick: this.infoEdit.bind(self, "info"), className: "info"})), 
                            React.createElement("li", null, React.createElement("a", {onClick: this.infoEdit.bind(self, "history"), className: "history"}))
                        ), 
                        React.createElement("a", {style: {display: this.state.saveView}, 
                            onClick: this.elemEdit}, "Сохранить изменения!")
                    )
                )
            )
        )
    }
});


OrderElemEdit = React.createClass({displayName: "OrderElemEdit",
    getInitialState: function() {
        return {data: {id: 0,
                        doc: new model.order()}
                };
    },
    setElem: function(elem, callback) {
        this.setState({data: elem});
        this.callbackSetElem = callback;
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        this.callbackSetElem(this.state.data);
        order_ctrl.edit_order(this.state.data);
        $('#OrderElemEditModal').modal('hide');
    },
    componentDidMount: function() {
        order_ctrl.setElemEditInfo = this.setElem;
    },
    render: function() {
        return (
            React.createElement("div", {id: "OrderElemEditModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w660"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, "Редактирование заявки")
                        ), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "textarea-i-wrapper"}, 
                                React.createElement("label", null, "Проблема"), 
                                React.createElement("textarea", {name: "reason", 
                                        onChange: this.inputBind, 
                                        value: this.state.data.doc.primary.reason})
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.save, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});



OrderElemHistory = React.createClass({displayName: "OrderElemHistory",
    getInitialState: function() {
        return {data: []};
    },
    setElem: function(elem) {
        this.setState({data: elem.doc.system_history});
    },
    componentDidMount: function() {
        order_ctrl.setElemHistory = this.setElem;
    },
    render: function() {
        return (
            React.createElement("div", {id: "OrderElemHistoryModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w900"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, "История заявки")
                        ), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("table", {className: "table simple"}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Дата"), 
                                        React.createElement("th", null, "Событие")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                    this.state.data.map(function(elem, index) {
                                        return (
                                            React.createElement("tr", {key: index}, 
                                                React.createElement("td", null, format.sql_to_date_time(elem.date)), 
                                                React.createElement("td", null, elem.name)
                                            )
                                        )
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )    
    }
});

var Router = ReactRouter; // or var Router = ReactRouter; in browsers
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

AppMain = React.createClass({displayName: "AppMain",
    render: function() {
        return (
            React.createElement(CompanyMenu, null)
        )
    }
});

var routes_main = (
  React.createElement(Route, {name: "app", path: "/", handler: AppMain}, 
    React.createElement(Route, {name: "all_order", path: "/all_order", handler: AllOrder}), 
    React.createElement(Route, {name: "new_order", path: "/new_order", handler: NewOrder}), 
    React.createElement(Route, {name: "company", path: "/company", handler: Company}), 
    React.createElement(Route, {name: "users", path: "/users", handler: MainUsers}), 
    React.createElement(Route, {name: "guide", path: "/guide", handler: MainGuide}), 
    React.createElement(DefaultRoute, {path: "/all_order", handler: AllOrder})
  )
);

/*    >*/

$(document).ready(function () {
    start();
});

function start() {
    see_cookie();
}

function auth() {
    React.render(React.createElement(Auth, null), document.body);
}

function see_cookie() {
    var key = localStorage.getItem('key');
    if(key == null) {
        auth(); 
    } else {
        root.send("auto_login", {"hash": key}, function(result) {
            if(result != 0) {
                root.set_user(JSON.parse(result.doc));
            } else {
                auth();
            }
        });
    }
}

function start_router() {
    Router.run(routes_main, function (Handler) {
        React.render(React.createElement(Handler, null), document.body);
    });
}
