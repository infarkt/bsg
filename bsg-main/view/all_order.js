AllOrder = React.createClass({
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-3">
                        <AllOrderMenu/>
                    </div>
                    <div className="col-md-9">
                        <AllOrderList/>
                        <OrderElemEdit/>
                        <OrderElemHistory/>
                    </div>
                </div>
            </div>    
        )
    }
});



AllOrderMenu = React.createClass({
    getInitialState: function() {
        return {filter: "all",
                new_order: order_ctrl.data_count.new_order_count};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        order_ctrl.update(val);
    },
    setCount: function(val) {
        this.setState({new_order: val});
    },
    componentDidMount: function() {
        order_ctrl.setNewCount = this.setCount;
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "new")} 
                            className={this.state.filter == "new" ? "new filter_active" : "new"}>
                            <span>Новые</span><span className="count">{this.state.new_order}</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "job")}
                            className={this.state.filter == "job" ? "car filter_active" : "car"}>
                            <span>В работе</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "ок")}
                            className={this.state.filter == "ок" ? "ckeck filter_active" : "ckeck"}>
                            <span>Выполненые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "reject")}
                            className={this.state.filter == "reject" ? "attention filter_active" : "attention"}>
                            <span>Отклонённые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "close")}
                            className={this.state.filter == "close" ? "close_0 filter_active" : "close_0"}>
                            <span>Закрытые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});


AllOrderList = React.createClass({
    getInitialState: function() {
        return {data: order_ctrl.store,
                request: jQuery.extend(true, {}, order_ctrl.default_request_data),
                display: jQuery.extend(true, {}, order_ctrl.display_step),
                order_status: order_status};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        order_ctrl.update("change_limit");
    },
    stepPrev: function() {
        order_ctrl.update("step_prev");
    },
    stepNext: function() {
        order_ctrl.update("step_next");
    },
    add: function() {
        
    },
    edit: function(elem) {
        
    },
    componentDidMount: function() {
        order_ctrl.setViewList = this.update;
        order_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Заявки {this.state.request.offset} - {(this.state.request.offset*1) + (this.state.request.limit*1)} из {this.state.data.count}
                    <a className="btn blue right" href="#/new_order" onClick={this.add}>Добавить</a>
                </h3>
                {this.state.data.data.map(function(elem) {
                    return (
                        <AllOrderElem key={elem.id} data={elem}/>
                    )
                })}
                <div className="table-footer">
                    <div className="wrap-input-button input-f-wrapper w170 left tcenter">
                        <input className="input-button"
                                type="text" 
                                name="limit"
                                onChange={this.limitSet} 
                                value={this.state.request.limit}/>
                        <a className="btn default" onClick={this.limitChange}>Показать</a>
                    </div>
                    <div className="table-step-div">
                        <a className="btn default middle left"
                            style={{display: this.state.display.prev_button}}
                            onClick={this.stepPrev}>Назад</a>
                        <a className="btn default middle right"
                            style={{display: this.state.display.next_button}}
                            onClick={this.stepNext}>Вперёд</a>
                    </div>
                </div>
            </div> 
        )
    }
});

AllOrderElem = React.createClass({
    getInitialState: function() {
        return {data: this.props.data,
                saveView: "none"};
    },
    bindStep: function(val) {
        this.state.data.doc.primary.status = val;
        // Системная история
        system_event[val].date = format.date_ms();
        this.state.data.doc.system_history.push(system_event[val]);
        // Уведомления
        switch(val) {
            case 3:
                root.send_mail(this.state.data.doc.primary.mail, "Ваша заявка №" + this.state.data.doc.system.key + " отклонена!");
                    break;
        }
        this.saveView();
    },
    saveView: function() {
        this.state.saveView = "";
        this.forceUpdate();
    },
    elemEdit: function() {
        order_ctrl.edit_order(this.state.data, function(data){
            if(data == 1) {
                order_ctrl.update('edit');
            }
        });
        this.setState({saveView: "none"});
    },
    setElem: function(elem) {
        this.setState({data: elem});
    },
    infoEdit: function(form) {
        order_ctrl.elem_edit(form, this.state.data, this.setElem);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var doc = this.state.data.doc, self = this;
        return (
            <div>
                <div className="timeline-post">
                    <ul>
                        <li onClick={this.bindStep.bind(self, 0)}
                            className={doc.primary.status == 0 ? "orange_active" : ""}><a>Новая</a></li>
                        <li onClick={this.bindStep.bind(self, 1)}
                            className={doc.primary.status == 1 ? "green_active" : ""}><a>В работе</a></li>
                        <li onClick={this.bindStep.bind(self, 2)}
                            className={doc.primary.status == 2 ? "blue_active" : ""}><a>Выполнена</a></li>
                        <li onClick={this.bindStep.bind(self, 3)}
                            className={doc.primary.status == 3 ? "red_active" : ""}><a>Отклонёна нами</a></li>
                        <li onClick={this.bindStep.bind(self, 4)}
                            className={doc.primary.status == 4 ? "blue_active" : ""}><a>Закрыта</a></li>
                        <li className="right m"><a className="white_t">{format.sql_to_date_time(doc.system.add_date)}</a></li>
                    </ul>
                    <div className="info-block">
                        <div className="w250">
                            <div className="text_view">{doc.system.key}</div>
                            <div className="text_view">{doc.primary.company}</div>
                            <div className="text_view">{doc.primary.persone}</div>
                            <div className="text_view green_h_t">{doc.primary.phone}</div>
                        </div>
                        <div className="w250">
                            <div className="text_view">{doc.primary.type}</div>
                            <div className="text_view">{doc.primary.address}</div>
                            <div className="text_view orange_h_t">{doc.primary.reason}</div>
                        </div>
                        <div className="textarea-i-wrapper">
                            <div className="text_view">{doc.master.fio}</div>
                        </div>
                    </div>
                    <div className="options clearfix">
                        <ul>
                            <li><a onClick={this.infoEdit.bind(self, "info")} className="info"></a></li>
                            <li><a onClick={this.infoEdit.bind(self, "history")} className="history"></a></li>
                        </ul>
                        <a style={{display: this.state.saveView}} 
                            onClick={this.elemEdit}>Сохранить изменения!</a>
                    </div>
                </div>
            </div>
        )
    }
});


OrderElemEdit = React.createClass({
    getInitialState: function() {
        return {data: {id: 0,
                        doc: new model.order()}
                };
    },
    setElem: function(elem, callback) {
        this.setState({data: elem});
        this.callbackSetElem = callback;
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data.doc.primary[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        this.callbackSetElem(this.state.data);
        order_ctrl.edit_order(this.state.data);
        $('#OrderElemEditModal').modal('hide');
    },
    componentDidMount: function() {
        order_ctrl.setElemEditInfo = this.setElem;
    },
    render: function() {
        return (
            <div id="OrderElemEditModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w660">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">Редактирование заявки</h4>
                        </div>
                        <div className="modal-body">
                            <div className="textarea-i-wrapper">
                                <label>Проблема</label>
                                <textarea name="reason"
                                        onChange={this.inputBind} 
                                        value={this.state.data.doc.primary.reason}/>
                            </div>       
                        </div>
                        <div className="modal-footer">
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});



OrderElemHistory = React.createClass({
    getInitialState: function() {
        return {data: []};
    },
    setElem: function(elem) {
        this.setState({data: elem.doc.system_history});
    },
    componentDidMount: function() {
        order_ctrl.setElemHistory = this.setElem;
    },
    render: function() {
        return (
            <div id="OrderElemHistoryModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w900">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">История заявки</h4>
                        </div>
                        <div className="modal-body">
                            <table className="table simple">
                                <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Событие</th>
                                    </tr>  
                                </thead>    
                                <tbody>
                                    {this.state.data.map(function(elem, index) {
                                        return (
                                            <tr key={index}>
                                                <td>{format.sql_to_date_time(elem.date)}</td>
                                                <td>{elem.name}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});