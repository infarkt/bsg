Sourse = React.createClass({
    render: function() {
        return (
            <div>
                <FormSourse/>
                <SourseList/>
            </div>    
        )
    }
});

FormSourse = React.createClass({
    getInitialState: function() {
        return {sourse: "",
                header: "Новый источник",
                type: "add",
                id: "",
                delButton: "none"};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                ctrl_sourse.add(this.state.sourse); 
                    break;
            case "edit":
                ctrl_sourse.edit(this.state.sourse, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({sourse: "", 
                        header: "Новый источник", 
                        type: "add",
                        delButton: "none"});
        $('#SourseModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({sourse: elem.doc.name, 
                        id: elem.id, 
                        header: "Редактирование источника", 
                        type: "edit",
                        delButton: ""});
        $('#SourseModal').modal('show');
    },
    del: function() {
        ctrl_sourse.del(this.state.id);
    },
    componentDidMount: function() {
        ctrl_sourse.addForm = this.addForm;
        ctrl_sourse.editForm = this.editForm;
    },
    render: function() {
        return (
            <div id="SourseModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w400">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.header}</h4>
                        </div>
                        <div className="modal-body">
                            <div className="input-f-wrapper">
                                <label>Название источника</label>
                                <input type="text" 
                                        name="sourse" 
                                        onChange={this.inputBind} 
                                        value={this.state.sourse}/>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={{display: this.state.delButton}} 
                                onClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

SourseList = React.createClass({
	getInitialState: function() {
        return {data: ctrl_sourse.store,
                request: {
                    name: "sourse", 
                    limit: root.options.limit,
                    offset: 0,
                    by: "name",
                },
                display: {
                    prev_button: "none",
                    next_botton: ""
                }};
    },
    ctrl: function() {
    	ctrl_sourse.setViewList = this.update;
        ctrl_sourse.request_data = this.state.request;
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        ctrl_sourse.update("change_limit");
    },
    stepPrev: function() {
        ctrl_sourse.update("step_prev");
    },
    stepNext: function() {
        ctrl_sourse.update("step_next");
    },
    add: function() {
        ctrl_sourse.addForm();
    },
    edit: function(elem) {
        ctrl_sourse.editForm(elem);
    },
    render: function() {
    	this.ctrl(); 
        var edit = this.edit, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Источники {this.state.request.offset} - {(this.state.request.offset*1) + (this.state.request.limit*1)} из {this.state.data.count}
                    <a className="btn blue right" onClick={this.add}>Добавить</a>
                </h3>
                <table className="table simple">
                    <thead>
                        <tr>
                            <th>Название источника</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.data.map(function(elem) {
                            return (
                                <tr key={elem.id} onClick={edit.bind(self, elem)}>
                                    <td>{elem.doc.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="table-footer">
                    <div className="wrap-input-button input-f-wrapper w170 left tcenter">
                        <input className="input-button"
                                type="text" 
                                name="limit"
                                onChange={this.limitSet} 
                                value={this.state.request.limit}/>
                        <a className="btn default" onClick={this.limitChange}>Показать</a>
                    </div>
                    <div className="table-step-div">
                        <a className="btn default middle left"
                            style={{display: this.state.display.prev_button}}
                            onClick={this.stepPrev}>Назад</a>
                        <a className="btn default middle right"
                            style={{display: this.state.display.next_button}}
                            onClick={this.stepNext}>В перёд</a>
                    </div>
                </div>
            </div> 
        )
    }
});