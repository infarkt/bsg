CompanyMenu = React.createClass({
    getInitialState: function() {
        return  {user: root.user.login};
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            <div>
                <nav className="navbar navbar-default" role="navigation">
                    <div className="navbar-header">
                        <a className="navbar-brand">
                            <img alt="" src="../styles/images/logo.png"/>
                            <span id="sidebar-exp"></span>
                        </a>
                    </div>
                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav navbar-right header-drop-right">
                            <li className="dropdown user-dropdown-one">
                                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                                    {this.state.user}
                                    <img className="img-user" alt="" src="../styles/images/user-img.png"/>
                                    <b className="caret"></b>
                                </a>
                                <ul className="dropdown-menu user-dropdown-two">
                                    <li>
                                        <a href="#/guide">Инструкции</a>
                                    </li>
                                    <li onClick={this.logOut} className="log-out">
                                        <a>Выход</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div id="wrapper" className="">
                    <div id="body-overlay"></div>
                    <div id="sidebar">
                        <ul className="drop-area">
                            <li className="no-back">
                                <a href="#/new_order">Новая заявка</a>
                            </li>
                            <li className="no-back">
                                <a href="#/all_order">Все заявки</a>
                            </li>
                            <li className="no-back">
                                <a href="#/company">Компании</a>
                            </li>
                            <li className="no-back">
                                <a href="#/users">Пользователи</a>
                            </li>
                            <li className="no-back">
                                <a href="#/list">Справочники</a>
                            </li>
                            <li className="no-back">
                                <a href="#/report">Отчёты</a>
                            </li>
                        </ul>
                    </div> 
                    <div id="wrap">
                        <div className="container">
                        {}
                        <RouteHandler/>
                        </div>
                    </div>   
                </div>
                <div id="footer">
                    <div className="row">
                        <div className="col-md-6 left">
                            <ul>
                                <li>
                                    <a href="#">Раз</a>
                                    <span>•</span>
                                </li>
                                <li>
                                    <a href="">Два</a>
                                    <span>•</span>
                                </li>
                                <li>
                                    <a href="#">Три</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-6 right">
                            <span className="pull-right">
                                Разработка
                                <a href="ordo-crm.ru">
                                    <strong> Ordo-CRM </strong>
                                </a>
                                2015
                            </span>
                        </div>
                    </div>
                </div>  
            </div>
        );
    }
});   
