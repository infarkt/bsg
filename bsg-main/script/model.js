var model = {
    main_user: function() {
        this.login = "",
        this.fio = "",
        this.phone = "+7",
        this.mail = "",
        this.role = "Администратор",
        this.pass = ""
    },
    company: function() {
        this.primary = {
            key: "",
            name: "",
            fio: "",
            phone1: "",
            phone2: "",
            mail: "",
            url: "",
            note: "",
        },
        this.status = {
            date_creat: format.date_ms(),
            step: 0,
            status: "Новая",
            reason_fail: '',
        },
        this.object = [],
        this.users = []
    },
    object: function() {
        this.object_name = "",
        this.contract_price = "",
        this.contract_date = "",
        this.works_type = ['','',''],
        this.address = []
    },
    address: function() {
        this.address = "",
        this.coords = ""
    },
    users: function() {
        this.key = "",
        this.login = "",
        this.fio = "",
        this.post = "",
        this.phone = "",
        this.mail = "",
        this.pass = "",
        this.my_address = []
    },
    order: function() {
        this.system = {
            key: "",
            company_key: "",
            master_key: "",
            persone_key: "",
            add_date: format.date_ms(),
        },
        this.primary = {
            status: 0,
            company: "",
            persone: "",
            mail: "",
            phone: "",
            contract: "",
            address: "",
            coords: "",
            type: "",
            reason: ""
        },
        this.master = {
            fio: "",
            master_note: ""
        },        
        this.system_history = [{
            date: format.date_ms(),
            name: "Создана администратором " + root.user.fio,
            alert: "",
            see: true
        }]
    }
};


var order_type = ["Техническое обслуживание",
                    "Вентиляция и кондиционирование",
                    "Система охраны и безопасности"];

var order_status = ["Новая","В работе","Выполнена","Отклонёна нами","Закрыта"];

function set_system_event() {
    system_event = [
        {
            date: "",
            name: "Переведена в Новая, администратором " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Принята в работу, администратором " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Помечена выполненой, администратором " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Отклонена администратором " + root.user.fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Закрыта администратором " + root.user.fio,
            alert: "",
            see: true
        },
    ]
}