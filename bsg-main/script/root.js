var root = {
	kladr: "http://kladr-api.ru/api.php",
	token: "540246cd7c5239667b8b47e7",
	subdomain: "",
	user: {},
	options: {
		limit: 10,
		update_time_order: 300000, // 300000 - 5 минут
	},
	load_data: {
		main_user_ctrl: false,
		main_company: false,
		order: false,
	},
	get_server: function(data, callback) {
		this.send("get_server", data, callback);
	}, 
	set_server: function(data) {
		this.server = data;
	},
	set_user: function(data) {
		this.user = data;
		localStorage.setItem('key', data.hash);
		set_system_event();
		this.app_entry();
	},
	app_entry: function() {
        this.pre_load();
	},
	out_user: function() {
		localStorage.removeItem('key');
		window.location.reload();
	},
	key: function(val) {
		var key = CryptoJS.MD5(val);
        return key.toString(CryptoJS.enc.Base64);
	},
	genPass: function() {
    	var length = 8;
        var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var retVal = "";
	    for (var i = 0, n = charset.length; i < length; ++i) {
	        retVal += charset.charAt(Math.floor(Math.random() * n));
	    }
    	return retVal;
	},
	send: function(url, data, callback) {
		$.ajax({
		  	type: "POST",
		  	url: '../server/' + url + '.php',
		  	dataType: "json",
		  	data: JSON.stringify(data), 
		  	success: callback
		});
	},
	send_mail: function(mail, content) {
		var data = {
			mail: mail,
			content: content
		}
		$.ajax({
		  	type: "POST",
		  	url: '../server/send_mail.php',
		  	dataType: "json",
		  	data: JSON.stringify(data)
		});
	},
	get_kladr: function(val, callback) {
		$.getJSON(this.kladr + "?callback=?",
            {token: this.token,
            	limit: 10,
            	contentType: "street",
            	cityId: "7800000000000",
            	query: val},
            callback
        );
	},
	geocode: function(address, callback) {
		$.ajax({
		  	url: 'http://geocode-maps.yandex.ru/1.x/',
		  	jsonp: "callback",
		  	dataType: "jsonp",
		  	data: {format: "json",
           			geocode: "Санкт-Петербург " + address}, 
		  	success: callback
		});
	},
	add_doc: function(name, doc, callback) {
		var data = {
			name: name,
			doc: doc
		}
		this.send("add_doc", data, callback);
	},
	edit_doc: function(name, elem, callback) {
		var data = {
			name: name,
			elem: elem
		}
		this.send("edit_doc", data, callback);
	},
	delete_doc: function(name, id, callback) {
		var data = {
			name: name,
			id: id
		}
		this.send("delete_doc", data, callback);
	},
	get_collection: function(data, callback) {
		this.send("get_collection", data, callback);
	},
	contract_gen: function(data) {
		this.send("gen/" + root.subdomain + "/generate_data", data, function() {
			window.open('http://' + root.subdomain + '.crm-kredit-broker.ru/server/gen/' + root.subdomain + '/see_contract.php');
		});
	},
	pre_load: function() {
		main_user_ctrl.load();
		main_company_ctrl.load();
		order_ctrl.load('load');
	},
	load: function() {
		if(this.load_data.main_user_ctrl == true && 
			this.load_data.main_company == true && 
			this.load_data.order == true) {
			start_router();
		}
	},
	get_orders: function(data, callback) {
		this.send("get_order", data, callback);
	}, 
}