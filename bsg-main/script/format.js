var format = {
    format_tel: function(num) {
        if(num != undefined) {
            num = num.replace(/\D/g, "");
            if(num.length <= 11) {
                switch(num.length) {
                    case 0:
                        var tel = "+7";
                            break;
                    case 1:
                        var tel = "+7";
                            break;
                    case 2:
                        var tel = "+7 " + num[1];
                            break;
                    case 3:
                        var tel = "+7 " + num[1] + num[2];
                            break;
                    case 4:
                        var tel = "+7 " + num[1] + num[2] + num[3];
                            break;
                    case 5:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4];
                            break;    
                    case 6:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5];
                            break;  
                    case 7:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6];
                            break;   
                    case 8:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7];
                            break; 
                    case 9:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8];
                            break; 
                    case 10:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9];
                            break;
                    case 11:
                        var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9] + num[10];
                            break;
                }
            } else {
               var tel = "+7 " + num[1] + num[2] + num[3] + " " + num[4] + num[5] + num[6] + "-" + num[7] + num[8] + "-" + num[9] + num[10];
            }
            return tel;
        }
    },
    sum: function(val) {
        return val.replace(/\D/g, "");
    },
    eng: function(val) {
        return val.replace(/\W/g, "");
    },
    date_ms: function() {
        var date = new Date();
        var date_ms = date.getTime();
        return date_ms;
    },
    start_date_ms: function() {
        var date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        var date_ms = date.getTime();
        return date_ms;
    },
    stop_date_ms: function() {
        var date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        var date_ms = date.getTime();
        return date_ms += 86400000;
    },
    mouth_text_to_num: function(mouth_str) {
        var mouth = 0;
        if (mouth_str == "Января"){mouth = 0}
        if (mouth_str == "Февраля"){mouth = 1}
        if (mouth_str == "Марта"){mouth = 2}
        if (mouth_str == "Апреля"){mouth = 3}
        if (mouth_str == "Мая"){mouth = 4}
        if (mouth_str == "Июня"){mouth = 5}
        if (mouth_str == "Июля"){mouth = 6}
        if (mouth_str == "Августа"){mouth = 7}
        if (mouth_str == "Сентября"){mouth = 8}
        if (mouth_str == "Октября"){mouth = 9}
        if (mouth_str == "Ноября"){mouth = 10}
        if (mouth_str == "Декабря"){mouth = 11}
        return mouth;
    },
    mouth_num_to_text: function(mouth) {
        var mouth_str = '';
        if (mouth == 0){mouth_str = "Январь"}
        if (mouth == 1){mouth_str = "Февраль"}
        if (mouth == 2){mouth_str = "Март"}
        if (mouth == 3){mouth_str = "Апрель"}
        if (mouth == 4){mouth_str = "Май"}
        if (mouth == 5){mouth_str = "Июнь"}
        if (mouth == 6){mouth_str = "Июль"}
        if (mouth == 7){mouth_str = "Август"}
        if (mouth == 8){mouth_str = "Сентябрь"}
        if (mouth == 9){mouth_str = "Октябрь"}
        if (mouth == 10){mouth_str = "Ноябрь"}
        if (mouth == 11){mouth_str = "Декабрь"} 
        return mouth_str;
    },
    get_year_month: function() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        return {year: year, month: month};
    },
    ms_to_date: function(date) {
        if (date == "0" || date == "" || date == null) {
            return "";
        } else {
            var ms_date = new Date();
            ms_date.setTime(date);
            var minutes = ms_date.getMinutes().toString();
            if(minutes < 10) {
                minutes = 0 + minutes;
            }
            var hours = ms_date.getHours().toString();
            if(hours < 10) {
                hours = 0 + hours;
            }
            var date = ms_date.getDate();
            var mouth = ms_date.getMonth();
            var mouth_str = "";
                if (mouth == 0){mouth_str = "Января"}
                if (mouth == 1){mouth_str = "Февраля"}
                if (mouth == 2){mouth_str = "Марта"}
                if (mouth == 3){mouth_str = "Апреля"}
                if (mouth == 4){mouth_str = "Мая"}
                if (mouth == 5){mouth_str = "Июня"}
                if (mouth == 6){mouth_str = "Июля"}
                if (mouth == 7){mouth_str = "Августа"}
                if (mouth == 8){mouth_str = "Сентября"}
                if (mouth == 9){mouth_str = "Октября"}
                if (mouth == 10){mouth_str = "Ноября"}
                if (mouth == 11){mouth_str = "Декабря"} 
            var full_year = ms_date.getFullYear().toString();
            var year = full_year.substring(2);
            return [minutes, hours, date, mouth_str, year];
        }
    },
    sql_to_date_time: function(date) {
        if (date == "0" || date == "" || date == null) {
            return "";
        } else {
            var ms_date = new Date();
            ms_date.setTime(date);
            var date = ms_date.getDate();
            var mouth = ms_date.getMonth();
            var mouth_str = "";
                if (mouth == 0){mouth_str = "Января"}
                if (mouth == 1){mouth_str = "Февраля"}
                if (mouth == 2){mouth_str = "Марта"}
                if (mouth == 3){mouth_str = "Апреля"}
                if (mouth == 4){mouth_str = "Мая"}
                if (mouth == 5){mouth_str = "Июня"}
                if (mouth == 6){mouth_str = "Июля"}
                if (mouth == 7){mouth_str = "Августа"}
                if (mouth == 8){mouth_str = "Сентября"}
                if (mouth == 10){mouth_str = "Ноября"}
                if (mouth == 9){mouth_str = "Октября"}
                if (mouth == 11){mouth_str = "Декабря"} 
            var year = ms_date.getFullYear();
            var hours = ms_date.getHours();
            if (hours < 10) { 
                hours = "0" + hours; 
            }
            var minutes = ms_date.getMinutes();
            if (minutes < 10) { 
                minutes = "0" + minutes; 
            }
            return(hours +":"+ minutes +" "+ date +" "+ mouth_str +" "+ year);
        }
    },
    format_text_date: function(data) {
        var date = data.replace(/\D/g, "");
        if(date.length <= 7) {
            switch(date.length) {
                case 0:
                    date = "";
                        break;
                case 1:
                    date = date[0];
                        break;
                case 2:
                    date = date[0] + date[1];
                        break;
                case 3:
                    date = date[0] + date[1] + "/" + date[2];
                        break;
                case 4:
                    date = date[0] + date[1] + "/" + date[2] + date[3];
                        break;
                case 5:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4];
                        break;
                case 6:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5];
                        break;
                case 7:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6];
                        break;
                case 7:
                    date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6] + date[7];
                        break;
            }   
        } else {
            date = date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5] + date[6] + date[7];
        }            
        return date;
    },
}

