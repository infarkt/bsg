// Поле тестовое
InputPhone = React.createClass({
    getInitialState: function() {
        return {tel: "+7"};
    },
    change: function(event) {
        var val = format.format_tel(event.target.value);
        this.setState({tel: val});
        event.target.value = val; // иначе берёт с евента последний символ
        this.props.bind(event);
    },
    render: function() {
        return (
            <div className="input-f-wrapper">
                <label>Телефон</label>
                <input type="text" 
                        name={this.props.name} 
                        onChange={this.change} 
                        value={this.props.val}/>
            </div>
        );
    }
});

// Применение
/*<InputPhone bind={this.inputBind}/>*/




    