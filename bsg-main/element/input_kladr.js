InputKladr = React.createClass({
    getInitialState: function() {
        return {street: []};
    },
    changeInput: function(event) {
        this.props.bind(event);
    },
    setSelect: function(elem) {
        var street = elem.type + " " +  elem.name;
        this.props.set(this.props.name, street);
    },
    up: function() {
        var setStreet = this.setStreet;
        root.get_kladr(this.props.val, function(data) {
            setStreet(data);
        });
    },
    setStreet: function(data) {
        this.setState({street: data.result});
    },
    render: function() {
        var set = this.setSelect, self = this;
        return (
            <div className="btn-group sg-button">
                <div className="input-g-wrapper dropdown-toggle" data-toggle="dropdown">
                    <label>Адрес</label>
                    <input onKeyUp={this.up} 
                            name={this.props.name}
                            onChange={this.changeInput} 
                            value={this.props.val}
                            type="text"/>
                </div>        
                <ul className="dropdown-menu relative" role="menu">
                    <div className="dropdown-content">
                        {this.state.street.map(function(elem) {
                            return (
                                <li key={elem.id} onClick={set.bind(self, elem)}>
                                    <a>{elem.type + " " +  elem.name}</a>
                                </li>
                            )
                        })}
                     </div>
                </ul> 
            </div>
        )
    }
});


/*<div className="btn-group sg-button">
                <button className="btn btn-default button-drop-large dropdown-toggle scroll-button" 
                    data-toggle="dropdown" type="button">
                    {this.props.val}
                    <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" role="menu">
                    <div className="dropdown-content">
                        {this.props.data.map(function(elem) {
                            return (
                                <li key={elem} onClick={set.bind(self, elem)}>
                                    <a>{elem}</a>
                                </li>
                            )
                        })}
                    </div>
                </ul>
            </div>*/

