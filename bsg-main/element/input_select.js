InputSelect = React.createClass({
    setSelect: function(elem) {
        this.props.bind(this.props.name, elem);
    },
    render: function() {
        var set = this.setSelect, self = this;
        return (
            <div className="btn-group sg-button">
                <button className="btn btn-default button-drop-large dropdown-toggle scroll-button" 
                    data-toggle="dropdown" type="button">
                    {this.props.val}
                    <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" role="menu">
                    <div className="dropdown-content">
                        {this.props.data.map(function(elem) {
                            return (
                                <li key={elem} onClick={set.bind(self, elem)}>
                                    <a>{elem}</a>
                                </li>
                            )
                        })}
                    </div>
                </ul>
            </div>           
                   
        )
    }
});



/*set active 
set visible*/


/*<div className="field">
            <label>{this.props.ph}</label>
                <div className="ui small selection dropdown"
                    ref="dropDown">
                    <input type="hidden"/>
                    <i className="dropdown icon"></i>
                    <div className="default text">{this.props.ph}</div>
                    <div className="menu">
                        {this.props.data.map(function(elem) {
                           return <div onClick={get} className="item" key={elem} accessKey={elem}>{elem}</div>;
                        })}
                    </div>
                </div>
            </div>*/
/*$(this.refs.dropDown.getDOMNode()).dropdown('get value');*/