var main_company_ctrl = {
	store: [],
	select: [],
	addForm: "",
	editForm: "",
	setFilter: "",
	setNewCount: "",
	data_count: {
		news: ""
	},
	setViewList: "",
	setMainCompanyInfo: "",
	addCompanyObject: "",
	editCompanyObject: "",
	addCompanyAddress: "",
	editCompanyAddress: "",
	addCompanyUsers: "",
	editCompanyUsers: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "name",
        name: "",
        address: "",
        step: "('1')",
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "name",
        name: "",
        address: "",
        step: "('1')",
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	load: function() {
		root.send("get_main_company", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				console.log(data);
				main_company_ctrl.store = data;
				main_company_ctrl.data_count.news = data.new_count;
				main_company_ctrl.set_display_step(data.count);
				/*main_company_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");*/
				root.load_data.main_company = true;
				root.load();
			}
		});
	},
	update: function(type) {
		switch(type) {
			case "add":
				this.request_data.offset = 0;
				this.request_data.name = "";
        		this.request_data.address = "";
        		this.request_data.step = "('0')";
        		main_company_ctrl.setFilter('new');
					break;
			case "search":
				this.request_data.offset = 0;
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "new":
				this.request_data.offset = 0;
				this.request_data.step = "('0')";
					break;		
			case "job":
				this.request_data.offset = 0;
				this.request_data.step = "('1')";
					break;
			case "archive":
				this.request_data.offset = 0;
				this.request_data.step = "('2')";
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.step = "('0','1','2')";
					break;

		}
		root.send("get_main_company", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_company_ctrl.setNewCount(data.new_count);
				main_company_ctrl.setViewList(main_company_ctrl.request_data, 
									data, 
									main_company_ctrl.set_display_step(data.count));
				/*main_company_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");*/
			} else {
				main_company_ctrl.setViewList(main_company_ctrl.request_data, 
												{count: 0, data: []}, 
												{prev_button: "none",
											     next_button: "none"});
			}
		});
	},
	add: function(doc) {
		root.add_doc("company", doc, function(data) {
    		if(data == 1) {
                $('#MainCompanyModal').modal('hide');
                main_company_ctrl.update("add");
            }
    	});
	},
	edit: function(elem) {
		root.edit_doc("company", elem, function(data) {
    		if(data == 1) {
    			// ??? как посмотреть роутер // 
    			if(window.location.hash == "company") {
    				main_company_ctrl.update("edit");
    			}
            }
    	});
	},
	del: function(id) {
		root.delete_doc("company", id, function(data){
    		if(data == 1) {
                $('#MainCompanyInfoModal').modal('hide');
                main_company_ctrl.update();
            }
    	});
	},
	info_edit: function(form, data, callback) {
		switch(form) {
            case "info":
            	this.setMainCompanyInfo(data, callback);
        		$('#MainCompanyInfoModal').modal('show');
            		break; 
        }
	}
}




/*save: function() {
        var _this = this;
    	
    },*/




/*save: function() {
        var _this = this;
    	
    },*/;var main_user_ctrl = {
	store: [],
	select: [],
	addForm: "",
	editForm: "",
	setViewList: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "fio",
        role: "%"
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "fio",
        role: "%"
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	load: function() {
		root.send("get_main_users", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_user_ctrl.store = data;
				main_user_ctrl.set_display_step(data.count);
				main_user_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");
				root.load_data.main_user_ctrl = true;
				root.load();
			}
		});
	},
	update: function(type) {
		switch(type) {
			case "add":
				this.request_data = jQuery.extend(true, {}, this.default_request_data);
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.role = "%";
					break;
			case "admin":
				this.request_data.offset = 0;
				this.request_data.role = "Администратор";
					break;
			case "master":
				this.request_data.offset = 0;
				this.request_data.role = "Инженер";
					break;
				
		}
		root.send("get_main_users", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_user_ctrl.setViewList(main_user_ctrl.request_data, 
									data, 
									main_user_ctrl.set_display_step(data.count));
				main_user_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");
			} else {
				main_user_ctrl.setViewList(main_user_ctrl.request_data, 
									data.data, 
									main_user_ctrl.set_display_step(data.count));
			}
		});
	},
	add: function(doc) {
		root.send("add_main_user", doc, function(data) {
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update("add");
            }
    	});
	},
	edit: function(doc, id) {
		root.send("edit_main_user", {id:id, doc:doc}, function(data) {
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update("add");
            }
    	});
	},
	del: function(id) {
		root.delete_doc("people", id, function(data){
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update();
            }
    	});
	}
}




/*save: function() {
        var _this = this;
    	
    },*/




/*save: function() {
        var _this = this;
    	
    },*/;var new_order_ctrl = {
	company_store: [],
	set_company_list: "",
	set_result: "",
	company_request: function(search) {
		root.send("get_new_order_company", {"search":search}, function(data) {
			if(data != "0") {
				for(var i = 0 in data) {
					data[i].doc = JSON.parse(data[i].doc);
				}
				new_order_ctrl.set_company_list(data);
			}
		});
	},
	get_contract_in_address: function(company, address) {
		this.company = company;
		this.map_object = [];
		this.map_address = [];
        for(var i in this.company.object) {
            for(var j in this.company.object[i].address) {
                var element = {
                    object_name: this.company.object[i].object_name,
                    works_type: this.company.object[i].works_type,
                    address: this.company.object[i].address[j].address
                }
                this.map_object.push(element);
            }
        }
        for(var k in this.map_object) {
            if(this.map_object[k].address == address) {
                this.map_address.push(this.map_object[k]);
            }
        }
        return this.map_address;
	},
	get_contract_type: function(type) {
		var text_type;
		for(var i in type) {
			if(type[i] == 'checked') {
				text_type = order_type[i];
			}
		}
		return text_type;
	},
	get_order_user_number: function(key) {
		root.send("get_order_user_number", {"key":key}, function(data) {
			new_order_ctrl.set_result(data);
		});
	},
	add_order: function(doc, callback) {
		root.add_doc("orders", doc, callback);
	},
};var order_ctrl = {
	store: [],
	data_count: {
		new_order_count: 0
	},
	setNewCount: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        step: "('0','1','2','3','4')",
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        step: "('0','1','2','3','4')",
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
    setViewList: "",
    setElemEditInfo: "",
    setElemHistory: "",
	load: function(type) {
		root.send("get_order", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.store = data;
				order_ctrl.data_count.new_order_count = data.new_order_count;
				order_ctrl.set_display_step(data.count);
				if(type == "load") {
					root.load_data.order = true;
					root.load();
				}
			}
			order_ctrl.interval_update();
		});
	},
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	update: function(type) {
		switch(type) {
			case "edit":
				/*this.request_data = jQuery.extend(true, {}, this.default_request_data);*/
				this.request_data.offset = 0;
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "new":
				this.request_data.offset = 0;
				this.request_data.step = "('0')";
					break;		
			case "job":
				this.request_data.offset = 0;
				this.request_data.step = "('1')";
					break;
			case "ок":
				this.request_data.offset = 0;
				this.request_data.step = "('2')";
					break;
			case "reject":
				this.request_data.offset = 0;
				this.request_data.step = "('3')";
					break;
			case "close":
				this.request_data.offset = 0;
				this.request_data.step = "('4')";
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.step = "('0','1','2','3','4')";
					break;

		}
		root.send("get_order", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.setNewCount(data.new_order_count);
				order_ctrl.setViewList(order_ctrl.request_data, 
									data, 
									order_ctrl.set_display_step(data.count));
			}
		});
	},
	interval_update: function() {
		setInterval(function() {
			if(Router.HashLocation.getCurrentPath() == "/" || Router.HashLocation.getCurrentPath() == "/all_order") {
				order_ctrl.update();
			};
		}, root.options.update_time_order);
	},
	edit_order: function(elem, callback) {
		root.send("edit_order", elem, callback);
	},
	elem_edit: function(form, data, callback) {
		switch(form) {
            case "info":
            	this.setElemEditInfo(data, callback);
        		$('#OrderElemEditModal').modal('show');
            		break; 
            case "history":
            	this.setElemHistory(data);
        		$('#OrderElemHistoryModal').modal('show');
            		break;
        }
	}
}