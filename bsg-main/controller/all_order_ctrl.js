var order_ctrl = {
	store: [],
	data_count: {
		new_order_count: 0
	},
	setNewCount: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        step: "('0','1','2','3','4')",
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        step: "('0','1','2','3','4')",
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
    setViewList: "",
    setElemEditInfo: "",
    setElemHistory: "",
	load: function(type) {
		root.send("get_order", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.store = data;
				order_ctrl.data_count.new_order_count = data.new_order_count;
				order_ctrl.set_display_step(data.count);
				if(type == "load") {
					root.load_data.order = true;
					root.load();
				}
			}
			order_ctrl.interval_update();
		});
	},
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	update: function(type) {
		switch(type) {
			case "edit":
				/*this.request_data = jQuery.extend(true, {}, this.default_request_data);*/
				this.request_data.offset = 0;
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "new":
				this.request_data.offset = 0;
				this.request_data.step = "('0')";
					break;		
			case "job":
				this.request_data.offset = 0;
				this.request_data.step = "('1')";
					break;
			case "ок":
				this.request_data.offset = 0;
				this.request_data.step = "('2')";
					break;
			case "reject":
				this.request_data.offset = 0;
				this.request_data.step = "('3')";
					break;
			case "close":
				this.request_data.offset = 0;
				this.request_data.step = "('4')";
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.step = "('0','1','2','3','4')";
					break;

		}
		root.send("get_order", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.setNewCount(data.new_order_count);
				order_ctrl.setViewList(order_ctrl.request_data, 
									data, 
									order_ctrl.set_display_step(data.count));
			}
		});
	},
	interval_update: function() {
		setInterval(function() {
			if(Router.HashLocation.getCurrentPath() == "/" || Router.HashLocation.getCurrentPath() == "/all_order") {
				order_ctrl.update();
			};
		}, root.options.update_time_order);
	},
	edit_order: function(elem, callback) {
		root.send("edit_order", elem, callback);
	},
	elem_edit: function(form, data, callback) {
		switch(form) {
            case "info":
            	this.setElemEditInfo(data, callback);
        		$('#OrderElemEditModal').modal('show');
            		break; 
            case "history":
            	this.setElemHistory(data);
        		$('#OrderElemHistoryModal').modal('show');
            		break;
        }
	}
}