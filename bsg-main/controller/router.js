
var Router = ReactRouter; // or var Router = ReactRouter; in browsers
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

AppMain = React.createClass({
    render: function() {
        return (
            <CompanyMenu/>
        )
    }
});

var routes_main = (
  <Route name="app" path="/" handler={AppMain}>
    <Route name="all_order" path="/all_order" handler={AllOrder}/>
    <Route name="new_order" path="/new_order" handler={NewOrder}/>
    <Route name="company" path="/company" handler={Company}/>
    <Route name="users" path="/users" handler={MainUsers}/>
    <Route name="guide" path="/guide" handler={MainGuide}/>
    <DefaultRoute path="/all_order" handler={AllOrder}/>
  </Route>
);

/*    >*/

$(document).ready(function () {
    start();
});

function start() {
    see_cookie();
}

function auth() {
    React.render(<Auth/>, document.body);
}

function see_cookie() {
    var key = localStorage.getItem('key');
    if(key == null) {
        auth(); 
    } else {
        root.send("auto_login", {"hash": key}, function(result) {
            if(result != 0) {
                root.set_user(JSON.parse(result.doc));
            } else {
                auth();
            }
        });
    }
}

function start_router() {
    Router.run(routes_main, function (Handler) {
        React.render(<Handler/>, document.body);
    });
}
