var new_order_ctrl = {
	company_store: [],
	set_company_list: "",
	set_result: "",
	company_request: function(search) {
		root.send("get_new_order_company", {"search":search}, function(data) {
			if(data != "0") {
				for(var i = 0 in data) {
					data[i].doc = JSON.parse(data[i].doc);
				}
				new_order_ctrl.set_company_list(data);
			}
		});
	},
	get_contract_in_address: function(company, address) {
		this.company = company;
		this.map_object = [];
		this.map_address = [];
        for(var i in this.company.object) {
            for(var j in this.company.object[i].address) {
                var element = {
                    object_name: this.company.object[i].object_name,
                    works_type: this.company.object[i].works_type,
                    address: this.company.object[i].address[j].address
                }
                this.map_object.push(element);
            }
        }
        for(var k in this.map_object) {
            if(this.map_object[k].address == address) {
                this.map_address.push(this.map_object[k]);
            }
        }
        return this.map_address;
	},
	get_contract_type: function(type) {
		var text_type;
		for(var i in type) {
			if(type[i] == 'checked') {
				text_type = order_type[i];
			}
		}
		return text_type;
	},
	get_order_user_number: function(key) {
		root.send("get_order_user_number", {"key":key}, function(data) {
			new_order_ctrl.set_result(data);
		});
	},
	add_order: function(doc, callback) {
		root.add_doc("orders", doc, callback);
	},
}