var main_company_ctrl = {
	store: [],
	select: [],
	addForm: "",
	editForm: "",
	setFilter: "",
	setNewCount: "",
	data_count: {
		news: ""
	},
	setViewList: "",
	setMainCompanyInfo: "",
	addCompanyObject: "",
	editCompanyObject: "",
	addCompanyAddress: "",
	editCompanyAddress: "",
	addCompanyUsers: "",
	editCompanyUsers: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "name",
        name: "",
        address: "",
        step: "('1')",
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "name",
        name: "",
        address: "",
        step: "('1')",
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	load: function() {
		root.send("get_main_company", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				console.log(data);
				main_company_ctrl.store = data;
				main_company_ctrl.data_count.news = data.new_count;
				main_company_ctrl.set_display_step(data.count);
				/*main_company_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");*/
				root.load_data.main_company = true;
				root.load();
			}
		});
	},
	update: function(type) {
		switch(type) {
			case "add":
				this.request_data.offset = 0;
				this.request_data.name = "";
        		this.request_data.address = "";
        		this.request_data.step = "('0')";
        		main_company_ctrl.setFilter('new');
					break;
			case "search":
				this.request_data.offset = 0;
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "new":
				this.request_data.offset = 0;
				this.request_data.step = "('0')";
					break;		
			case "job":
				this.request_data.offset = 0;
				this.request_data.step = "('1')";
					break;
			case "archive":
				this.request_data.offset = 0;
				this.request_data.step = "('2')";
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.step = "('0','1','2')";
					break;

		}
		root.send("get_main_company", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_company_ctrl.setNewCount(data.new_count);
				main_company_ctrl.setViewList(main_company_ctrl.request_data, 
									data, 
									main_company_ctrl.set_display_step(data.count));
				/*main_company_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");*/
			} else {
				main_company_ctrl.setViewList(main_company_ctrl.request_data, 
												{count: 0, data: []}, 
												{prev_button: "none",
											     next_button: "none"});
			}
		});
	},
	add: function(doc) {
		root.add_doc("company", doc, function(data) {
    		if(data == 1) {
                $('#MainCompanyModal').modal('hide');
                main_company_ctrl.update("add");
            }
    	});
	},
	edit: function(elem) {
		root.edit_doc("company", elem, function(data) {
    		if(data == 1) {
    			// ??? как посмотреть роутер // 
    			if(window.location.hash == "company") {
    				main_company_ctrl.update("edit");
    			}
            }
    	});
	},
	del: function(id) {
		root.delete_doc("company", id, function(data){
    		if(data == 1) {
                $('#MainCompanyInfoModal').modal('hide');
                main_company_ctrl.update();
            }
    	});
	},
	info_edit: function(form, data, callback) {
		switch(form) {
            case "info":
            	this.setMainCompanyInfo(data, callback);
        		$('#MainCompanyInfoModal').modal('show');
            		break; 
        }
	}
}




/*save: function() {
        var _this = this;
    	
    },*/




/*save: function() {
        var _this = this;
    	
    },*/