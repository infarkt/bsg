var main_user_ctrl = {
	store: [],
	select: [],
	addForm: "",
	editForm: "",
	setViewList: "",
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "fio",
        role: "%"
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "fio",
        role: "%"
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	load: function() {
		root.send("get_main_users", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_user_ctrl.store = data;
				main_user_ctrl.set_display_step(data.count);
				main_user_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");
				root.load_data.main_user_ctrl = true;
				root.load();
			}
		});
	},
	update: function(type) {
		switch(type) {
			case "add":
				this.request_data = jQuery.extend(true, {}, this.default_request_data);
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.role = "%";
					break;
			case "admin":
				this.request_data.offset = 0;
				this.request_data.role = "Администратор";
					break;
			case "master":
				this.request_data.offset = 0;
				this.request_data.role = "Инженер";
					break;
				
		}
		root.send("get_main_users", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				main_user_ctrl.setViewList(main_user_ctrl.request_data, 
									data, 
									main_user_ctrl.set_display_step(data.count));
				main_user_ctrl.select = _.pluck(_.pluck(data.data, "doc"), "fio");
			} else {
				main_user_ctrl.setViewList(main_user_ctrl.request_data, 
									data.data, 
									main_user_ctrl.set_display_step(data.count));
			}
		});
	},
	add: function(doc) {
		root.send("add_main_user", doc, function(data) {
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update("add");
            }
    	});
	},
	edit: function(doc, id) {
		root.send("edit_main_user", {id:id, doc:doc}, function(data) {
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update("add");
            }
    	});
	},
	del: function(id) {
		root.delete_doc("people", id, function(data){
    		if(data == 1) {
                $('#MainUsersModal').modal('hide');
                main_user_ctrl.update();
            }
    	});
	}
}




/*save: function() {
        var _this = this;
    	
    },*/




/*save: function() {
        var _this = this;
    	
    },*/