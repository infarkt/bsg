<?php
header('content-encoding: gzip');
require "base.php";

$data = json_decode(file_get_contents('php://input'), true);

$by = $data["by"];
$limit = $data["limit"];
$offset = $data["offset"];
$user_key = $data["user_key"];
$company_key = $data["company_key"];
$step = $data["step"];

$dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

$query_count = "SELECT * FROM orders WHERE (doc->'system'->>'persone_key') = '$user_key'";
$result_count = pg_query($query_count); 
$result_query["count"] = pg_affected_rows($result_count);

$query_all_company_count = "SELECT * FROM orders WHERE (doc->'system'->>'company_key') = '$company_key'";
$result_all_company_count = pg_query($query_all_company_count); 
if(pg_affected_rows($result_all_company_count) != 0) {
	$result_query["all_company_count"] = pg_affected_rows($result_all_company_count);
} else {
	$result_query["all_company_count"] = 0;
};

$query = "SELECT * FROM orders WHERE (doc->'system'->>'persone_key') = '$user_key'
								AND (doc->'primary'->>'status') IN $step
								ORDER BY (doc->'system'->>'$by') DESC LIMIT $limit OFFSET $offset";
$result = pg_query($query); 
echo pg_last_error();
if(pg_affected_rows($result) != 0) {
	$result_query["data"] = pg_fetch_all($result);
} else {
	$result_query["data"] = [];
};
echo pg_last_error();
pg_close($dbconn);

$result_json = json_encode($result_query);
$result_zip = gzencode($result_json, $zip_compress);
echo $result_zip;
?>