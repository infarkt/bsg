// Поле чек
InputCheck = React.createClass({displayName: "InputCheck",
    click: function() {
        var val = this.props.val == "" ? "checked" : "";
        this.props.bind(this.props.num_check, val, this.props.head);
    },
    render: function() {
        return (
            React.createElement("li", {className: ""}, 
                React.createElement("div", {className: "clearfix prettycheckbox labelright blue"}, 
                    React.createElement("a", {onClick: this.click, className: this.props.val})
                ), 
                React.createElement("span", null, this.props.head)
            )
        );
    }
});


/*<div className="input-f-wrapper">
                <label>{this.props.head}</label>
                <input type="text" 
                        name={this.props.name}
                        onChange={this.change} 
                        value={this.props.val}/>
            </div>*/
// Поле только английский
InputEng = React.createClass({displayName: "InputEng",
    change: function(event) {
        event.target.value = format.eng(event.target.value);
        this.props.bind(event);
    },
    render: function() {
        return (
            React.createElement("div", {className: "input-f-wrapper"}, 
                React.createElement("label", null, this.props.head), 
                React.createElement("input", {type: "text", 
                        name: this.props.name, 
                        onChange: this.change, 
                        value: this.props.val})
            )
        );
    }
});

/*<InputEng
    head="Дата заключения договора"
    name="contract_date"
    val={this.state.data.contract_date}
    bind={this.inputBind}/>

    */
// Поле пароля с иконкой
InputPassIcon = React.createClass({displayName: "InputPassIcon",
    render: function() {
        return (
            React.createElement("div", {className: "field"}, 
                React.createElement("div", {className: "ui icon input"}, 
                    React.createElement("input", {type: "password", 
                        value: this.props.val, 
                        name: this.props.name, 
                        placeholder: "Пароль", 
                        onChange: this.props.bind}), 
                    React.createElement("i", {className: "lock icon"})
                )
            )
        );
    }
});

// Применение
/*<InputPassIcon name="pass" 
    val={this.state.pass}
    bind={this.inputBind}/>*/
// Поле тестовое
InputPhone = React.createClass({displayName: "InputPhone",
    getInitialState: function() {
        return {tel: "+7"};
    },
    change: function(event) {
        var val = format.format_tel(event.target.value);
        this.setState({tel: val});
        event.target.value = val; // иначе берёт с евента последний символ
        this.props.bind(event);
    },
    render: function() {
        return (
            React.createElement("div", {className: "input-f-wrapper"}, 
                React.createElement("label", null, "Телефон"), 
                React.createElement("input", {type: "text", 
                        name: this.props.name, 
                        onChange: this.change, 
                        value: this.props.val})
            )
        );
    }
});

// Применение
/*<InputPhone bind={this.inputBind}/>*/




    
InputSelect = React.createClass({displayName: "InputSelect",
    setSelect: function(elem) {
        this.props.bind(this.props.name, elem);
    },
    render: function() {
        var set = this.setSelect, self = this;
        return (
            React.createElement("div", {className: "btn-group sg-button"}, 
                React.createElement("button", {className: "btn btn-default button-drop-large dropdown-toggle scroll-button", 
                    "data-toggle": "dropdown", type: "button"}, 
                    this.props.val, 
                    React.createElement("span", {className: "caret"})
                ), 
                React.createElement("ul", {className: "dropdown-menu", role: "menu"}, 
                    React.createElement("div", {className: "dropdown-content"}, 
                        this.props.data.map(function(elem) {
                            return (
                                React.createElement("li", {key: elem, onClick: set.bind(self, elem)}, 
                                    React.createElement("a", null, elem)
                                )
                            )
                        })
                    )
                )
            )           
                   
        )
    }
});



/*set active 
set visible*/


/*<div className="field">
            <label>{this.props.ph}</label>
                <div className="ui small selection dropdown"
                    ref="dropDown">
                    <input type="hidden"/>
                    <i className="dropdown icon"></i>
                    <div className="default text">{this.props.ph}</div>
                    <div className="menu">
                        {this.props.data.map(function(elem) {
                           return <div onClick={get} className="item" key={elem} accessKey={elem}>{elem}</div>;
                        })}
                    </div>
                </div>
            </div>*/
/*$(this.refs.dropDown.getDOMNode()).dropdown('get value');*/
// Поле тестовое
InputSum = React.createClass({displayName: "InputSum",
    change: function(event) {
        event.target.value = format.sum(event.target.value);
        this.props.bind(event);
    },
    render: function() {
        return (
            React.createElement("div", {className: "input-f-wrapper"}, 
                React.createElement("label", null, this.props.head), 
                React.createElement("input", {type: "text", 
                        name: this.props.name, 
                        onChange: this.change, 
                        value: this.props.val})
            )
        );
    }
});




     
// Поле дата ручное
InputTextDate = React.createClass({displayName: "InputTextDate",
    change: function(event) {
        event.target.value = format.format_text_date(event.target.value);
        this.props.bind(event);
    },
    render: function() {
        return (
            React.createElement("div", {className: "input-f-wrapper"}, 
                React.createElement("label", null, this.props.head), 
                React.createElement("input", {type: "text", 
                        name: this.props.name, 
                        onChange: this.change, 
                        value: this.props.val})
            )
        );
    }
});


/*
<InputTextDate
    head="Дата заключения договора"
    name="contract_date"
    val={this.state.data.contract_date}
    bind={this.inputBind}/>

    */
// Поле тестовое с иконкой
InputTextIcon = React.createClass({displayName: "InputTextIcon",
    render: function() {
        return (
            React.createElement("div", {className: "field"}, 
                React.createElement("div", {className: "ui icon input"}, 
                    React.createElement("input", {type: "text", 
                        value: this.props.val, 
                        name: this.props.name, 
                        placeholder: this.props.ph, 
                        onChange: this.props.bind}), 
                    React.createElement("i", {className: this.props.icon})
                )
            )
        );
    }
});

// Применение
/*<InputTextIcon name="login" 
    ph="Логин" 
    val={this.state.login}
    bind={this.inputBind} 
    icon="user icon"/>*/
// Время дата скролл точ
NextReminderDate = React.createClass({displayName: "NextReminderDate",
    eWheel: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.name) {
            case "Hours":
                if(event.deltaY > 0) {
                    this.props.val += 3600000;
                } else {
                    this.props.val -= 3600000;
                }
                    break;
            case "Minutes":
                if(event.deltaY > 0) {
                    this.props.val += 300000;
                } else {
                    this.props.val -= 300000;
                }
                    break;
            case "Day":
                if(event.deltaY > 0) {
                    this.props.val += 86400000;
                } else {
                    this.props.val -= 86400000;
                }
                    break;
            case "Month":
                if(event.deltaY > 0) {
                    this.props.val += 2592000000;
                } else {
                    this.props.val -= 2592000000;
                }
                    break;
            case "Year":
                if(event.deltaY > 0) {
                    this.props.val += 31536000000;
                } else {
                    this.props.val -= 31536000000;
                }
                    break;   
        }
        this.props.bind(this.props.val);
    },
    clickMouse: function(event) {
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val += 3600000;
                    break;
            case "Minutes":
                this.props.val += 300000;
                    break;
            case "Day":
                this.props.val += 86400000;
                    break;
            case "Month":
                this.props.val += 2592000000;
                    break;
            case "Year":
                this.props.val += 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    contextMenu: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val -= 3600000;
                    break;
            case "Minutes":
                this.props.val -= 300000;
                    break;
            case "Day":
                this.props.val -= 86400000;
                    break;
            case "Month":
                this.props.val -= 2592000000;
                    break;
            case "Year":
                this.props.val -= 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    render: function() {
        return (
            React.createElement("div", {className: "input-f-wrapper"}, 
                React.createElement("label", null, "Напомнить"), 
                React.createElement("div", {className: "remin-btn-group"}, 
                    React.createElement("a", {className: "btn green middle", 
                        name: "Hours", 
                        onContextMenu: this.contextMenu, 
                        onClick: this.clickMouse, 
                        onWheel: this.eWheel}, format.ms_to_date(this.props.val)[1]), 
                    React.createElement("div", {className: "w2"}), 
                    React.createElement("a", {className: "btn green middle", 
                        name: "Minutes", 
                        onContextMenu: this.contextMenu, 
                        onClick: this.clickMouse, 
                        onWheel: this.eWheel}, format.ms_to_date(this.props.val)[0]), 
                    React.createElement("div", {className: "w10"}), 
                    React.createElement("a", {className: "btn blue middle", 
                        name: "Day", 
                        onContextMenu: this.contextMenu, 
                        onClick: this.clickMouse, 
                        onWheel: this.eWheel}, format.ms_to_date(this.props.val)[2]), 
                    React.createElement("div", {className: "w2"}), 
                    React.createElement("a", {className: "btn blue middle", 
                        name: "Month", 
                        onContextMenu: this.contextMenu, 
                        onClick: this.clickMouse, 
                        onWheel: this.eWheel}, format.ms_to_date(this.props.val)[3]), 
                    React.createElement("div", {className: "w2"}), 
                    React.createElement("a", {className: "btn blue middle", 
                        name: "Year", 
                        onContextMenu: this.contextMenu, 
                        onClick: this.clickMouse, 
                        onWheel: this.eWheel}, format.ms_to_date(this.props.val)[4])
                )
            )    
        );
    }
});



NextReminderDateBlock = React.createClass({displayName: "NextReminderDateBlock",
    eWheel: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                if(event.deltaY > 0) {
                    this.props.val += 3600000;
                } else {
                    this.props.val -= 3600000;
                }
                    break;
            case "Minutes":
                if(event.deltaY > 0) {
                    this.props.val += 300000;
                } else {
                    this.props.val -= 300000;
                }
                    break;
            case "Day":
                if(event.deltaY > 0) {
                    this.props.val += 86400000;
                } else {
                    this.props.val -= 86400000;
                }
                    break;
            case "Month":
                if(event.deltaY > 0) {
                    this.props.val += 2592000000;
                } else {
                    this.props.val -= 2592000000;
                }
                    break;
            case "Year":
                if(event.deltaY > 0) {
                    this.props.val += 31536000000;
                } else {
                    this.props.val -= 31536000000;
                }
                    break;   
        }
        this.props.bind(this.props.val);
    },
    clickMouse: function(event) {
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val += 3600000;
                    break;
            case "Minutes":
                this.props.val += 300000;
                    break;
            case "Day":
                this.props.val += 86400000;
                    break;
            case "Month":
                this.props.val += 2592000000;
                    break;
            case "Year":
                this.props.val += 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    contextMenu: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val -= 3600000;
                    break;
            case "Minutes":
                this.props.val -= 300000;
                    break;
            case "Day":
                this.props.val -= 86400000;
                    break;
            case "Month":
                this.props.val -= 2592000000;
                    break;
            case "Year":
                this.props.val -= 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    seeTime: function() {   
        // цикл
        
    },
    render: function() {
        return (
            React.createElement("div", {className: "nrdb"}, 
                React.createElement("div", {accessKey: "Hours", 
                    className: "w35 " + this.props.view_class, 
                    onContextMenu: this.contextMenu, 
                    onClick: this.clickMouse, 
                    onWheel: this.eWheel}, 
                    React.createElement("span", {accessKey: "Hours"}, format.ms_to_date(this.props.val)[1]), 
                    React.createElement("span", {className: "tw5"}, ":")
                ), 
                React.createElement("div", {accessKey: "Minutes", 
                    className: "w40 " + this.props.view_class, 
                    onContextMenu: this.contextMenu, 
                    onClick: this.clickMouse, 
                    onWheel: this.eWheel}, 
                    React.createElement("span", {accessKey: "Minutes"}, format.ms_to_date(this.props.val)[0]), 
                    React.createElement("span", {className: "tw10"})
                ), 
                React.createElement("div", {accessKey: "Day", 
                    className: "w25 " + this.props.view_class, 
                    onContextMenu: this.contextMenu, 
                    onClick: this.clickMouse, 
                    onWheel: this.eWheel}, 
                    React.createElement("span", {accessKey: "Day"}, format.ms_to_date(this.props.val)[2]), 
                    React.createElement("span", {className: "tw5"})
                ), 
                React.createElement("div", {accessKey: "Month", 
                    className: "w80 " + this.props.view_class, 
                    onContextMenu: this.contextMenu, 
                    onClick: this.clickMouse, 
                    onWheel: this.eWheel}, 
                    React.createElement("span", {accessKey: "Month"}, format.ms_to_date(this.props.val)[3]), 
                    React.createElement("span", {className: "tw5"})
                ), 
                React.createElement("div", {accessKey: "Year", 
                    className: "w25 " + this.props.view_class, 
                    onContextMenu: this.contextMenu, 
                    onClick: this.clickMouse, 
                    onWheel: this.eWheel}, 
                    React.createElement("span", {accessKey: "Year"}, format.ms_to_date(this.props.val)[4])
                )
            )    
        );
    }
});


