// Время дата скролл точ
NextReminderDate = React.createClass({
    eWheel: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.name) {
            case "Hours":
                if(event.deltaY > 0) {
                    this.props.val += 3600000;
                } else {
                    this.props.val -= 3600000;
                }
                    break;
            case "Minutes":
                if(event.deltaY > 0) {
                    this.props.val += 300000;
                } else {
                    this.props.val -= 300000;
                }
                    break;
            case "Day":
                if(event.deltaY > 0) {
                    this.props.val += 86400000;
                } else {
                    this.props.val -= 86400000;
                }
                    break;
            case "Month":
                if(event.deltaY > 0) {
                    this.props.val += 2592000000;
                } else {
                    this.props.val -= 2592000000;
                }
                    break;
            case "Year":
                if(event.deltaY > 0) {
                    this.props.val += 31536000000;
                } else {
                    this.props.val -= 31536000000;
                }
                    break;   
        }
        this.props.bind(this.props.val);
    },
    clickMouse: function(event) {
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val += 3600000;
                    break;
            case "Minutes":
                this.props.val += 300000;
                    break;
            case "Day":
                this.props.val += 86400000;
                    break;
            case "Month":
                this.props.val += 2592000000;
                    break;
            case "Year":
                this.props.val += 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    contextMenu: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val -= 3600000;
                    break;
            case "Minutes":
                this.props.val -= 300000;
                    break;
            case "Day":
                this.props.val -= 86400000;
                    break;
            case "Month":
                this.props.val -= 2592000000;
                    break;
            case "Year":
                this.props.val -= 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    render: function() {
        return (
            <div className="input-f-wrapper">
                <label>Напомнить</label>
                <div className="remin-btn-group">
                    <a className="btn green middle" 
                        name="Hours" 
                        onContextMenu={this.contextMenu}
                        onClick={this.clickMouse}
                        onWheel={this.eWheel}>{format.ms_to_date(this.props.val)[1]}</a>
                    <div className="w2"></div>
                    <a className="btn green middle" 
                        name="Minutes" 
                        onContextMenu={this.contextMenu}
                        onClick={this.clickMouse}
                        onWheel={this.eWheel}>{format.ms_to_date(this.props.val)[0]}</a>
                    <div className="w10"></div>
                    <a className="btn blue middle" 
                        name="Day" 
                        onContextMenu={this.contextMenu}
                        onClick={this.clickMouse}
                        onWheel={this.eWheel}>{format.ms_to_date(this.props.val)[2]}</a>
                    <div className="w2"></div>
                    <a className="btn blue middle"
                        name="Month" 
                        onContextMenu={this.contextMenu}
                        onClick={this.clickMouse}
                        onWheel={this.eWheel}>{format.ms_to_date(this.props.val)[3]}</a>
                    <div className="w2"></div>
                    <a className="btn blue middle" 
                        name="Year" 
                        onContextMenu={this.contextMenu}
                        onClick={this.clickMouse}
                        onWheel={this.eWheel}>{format.ms_to_date(this.props.val)[4]}</a>
                </div>    
            </div>    
        );
    }
});



NextReminderDateBlock = React.createClass({
    eWheel: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                if(event.deltaY > 0) {
                    this.props.val += 3600000;
                } else {
                    this.props.val -= 3600000;
                }
                    break;
            case "Minutes":
                if(event.deltaY > 0) {
                    this.props.val += 300000;
                } else {
                    this.props.val -= 300000;
                }
                    break;
            case "Day":
                if(event.deltaY > 0) {
                    this.props.val += 86400000;
                } else {
                    this.props.val -= 86400000;
                }
                    break;
            case "Month":
                if(event.deltaY > 0) {
                    this.props.val += 2592000000;
                } else {
                    this.props.val -= 2592000000;
                }
                    break;
            case "Year":
                if(event.deltaY > 0) {
                    this.props.val += 31536000000;
                } else {
                    this.props.val -= 31536000000;
                }
                    break;   
        }
        this.props.bind(this.props.val);
    },
    clickMouse: function(event) {
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val += 3600000;
                    break;
            case "Minutes":
                this.props.val += 300000;
                    break;
            case "Day":
                this.props.val += 86400000;
                    break;
            case "Month":
                this.props.val += 2592000000;
                    break;
            case "Year":
                this.props.val += 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    contextMenu: function(event) {
        event.nativeEvent.preventDefault();
        switch(event.target.accessKey) {
            case "Hours":
                this.props.val -= 3600000;
                    break;
            case "Minutes":
                this.props.val -= 300000;
                    break;
            case "Day":
                this.props.val -= 86400000;
                    break;
            case "Month":
                this.props.val -= 2592000000;
                    break;
            case "Year":
                this.props.val -= 31536000000;
                    break;   
        }
        this.props.bind(this.props.val);
    },
    seeTime: function() {   
        // цикл
        
    },
    render: function() {
        return (
            <div className="nrdb">
                <div accessKey="Hours" 
                    className={"w35 " + this.props.view_class} 
                    onContextMenu={this.contextMenu}
                    onClick={this.clickMouse}
                    onWheel={this.eWheel}>
                    <span accessKey="Hours">{format.ms_to_date(this.props.val)[1]}</span>
                    <span className="tw5">:</span>
                </div>
                <div accessKey="Minutes" 
                    className={"w40 " + this.props.view_class} 
                    onContextMenu={this.contextMenu}
                    onClick={this.clickMouse}
                    onWheel={this.eWheel}>
                    <span accessKey="Minutes">{format.ms_to_date(this.props.val)[0]}</span>
                    <span className="tw10"></span>
                </div>   
                <div accessKey="Day" 
                    className={"w25 " + this.props.view_class} 
                    onContextMenu={this.contextMenu}
                    onClick={this.clickMouse}
                    onWheel={this.eWheel}>
                    <span accessKey="Day">{format.ms_to_date(this.props.val)[2]}</span>
                    <span className="tw5"></span>
                </div>
                <div accessKey="Month" 
                    className={"w80 " + this.props.view_class} 
                    onContextMenu={this.contextMenu}
                    onClick={this.clickMouse}
                    onWheel={this.eWheel}>
                    <span accessKey="Month">{format.ms_to_date(this.props.val)[3]}</span>
                    <span className="tw5"></span>
                </div>
                <div accessKey="Year" 
                    className={"w25 " + this.props.view_class} 
                    onContextMenu={this.contextMenu}
                    onClick={this.clickMouse}
                    onWheel={this.eWheel}>
                    <span accessKey="Year">{format.ms_to_date(this.props.val)[4]}</span>
                </div>    
            </div>    
        );
    }
});


