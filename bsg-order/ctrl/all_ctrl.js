var new_order_ctrl = {
	setForma: "",
	backStep: "",
	resetMaster: "",
	map_object: [],
	double_address: [],
	edit: function(elem) {
		root.edit_company(elem, function(data) {
    		if(data == 1) {
               console.log(data);
            }
    	});
	},
	get_order_type: function(elem) {
        console.log(1);
		this.map_object = [];
		this.double_address = [];
        for(var i in root.company.object) {
            for(var j in root.company.object[i].address) {
                var element = {
                    object_name: root.company.object[i].object_name,
                    works_type: root.company.object[i].works_type,
                    address: root.company.object[i].address[j].address
                }
                this.map_object.push(element);
            }
        }
        for(var k in this.map_object) {
            if(this.map_object[k].address == elem) {
                this.double_address.push(this.map_object[k]);
            }
        }
        var result_type = [];
        for(var l in this.double_address) {
        	for(var n in this.double_address[l].works_type) {
        		if(this.double_address[l].works_type[n] == "checked") {
	       			result_type.push(order_type[n]);
	       		}
        	}
        }
        return result_type;
	},
	add_order: function(doc, callback) {
		root.add_doc("orders", doc, callback);
	},
    addUsers: "",
    editUsers: "",
};var order_ctrl = {
	store: [],
	data_count: {
		all_company_count: 0
	},
	default_request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        user_key: "",
        company_key: "",
        step: "('0','1','2','3','4')",
	},
	request_data: {
        limit: root.options.limit,
        offset: 0,
        by: "add_date",
        user_key: "",
        company_key: "",
        step: "('0','1','2','3','4')",
	},
	display_step: {
        prev_button: "none",
        next_button: ""
    },
    setViewList: "",
	load: function(type) {
		root.send("get_order", this.default_request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.store = data;
				order_ctrl.data_count.all_company_count = data.all_company_count;
				order_ctrl.data_count.my_count = data.count;
				order_ctrl.set_display_step(data.count);
				if(type == "load") {
					root.load_data.order = true;
					root.load();
				}
			}
			order_ctrl.interval_update();
		});
	},
	set_display_step: function(count) {
		if(this.request_data.offset == 0) {
			this.display_step.prev_button = "none";
		} else {
			this.display_step.prev_button = "";
		}
		if((this.request_data.offset + (this.request_data.limit*1)) >= count) {
			this.display_step.next_button = "none";
		} else {
			this.display_step.next_button = "";
		}
		return this.display_step;

	},
	update: function(type) {
		switch(type) {
			case "add":
				this.request_data = jQuery.extend(true, {}, this.default_request_data);
					break;
			case "change_limit":
				this.request_data.offset = 0;
					break;
			case "step_next":
				this.request_data.offset += (this.request_data.limit*1);
					break;
			case "step_prev":
				this.request_data.offset -= this.request_data.limit;
				if(this.request_data.offset < 0) {
					this.request_data.offset = 0;
				}
					break;
			case "process":
				this.request_data.offset = 0;
				this.request_data.step = "('0')";
					break;		
			case "job":
				this.request_data.offset = 0;
				this.request_data.step = "('1')";
					break;
			case "ок":
				this.request_data.offset = 0;
				this.request_data.step = "('2')";
					break;
			case "reject":
				this.request_data.offset = 0;
				this.request_data.step = "('3')";
					break;
			case "close":
				this.request_data.offset = 0;
				this.request_data.step = "('4')";
					break;
			case "all":
				this.request_data.offset = 0;
				this.request_data.step = "('0','1','2','3','4')";
					break;

		}
		root.send("get_order", this.request_data, function(data) {
			if(data != "0") {
				for(var i = 0 in data.data) {
					data.data[i].doc = JSON.parse(data.data[i].doc);
				}
				order_ctrl.data_count.all_company_count = data.all_company_count;
				order_ctrl.data_count.my_count = data.count;
				order_ctrl.setViewList(order_ctrl.request_data, 
									data, 
									order_ctrl.set_display_step(data.count));
			}
		});
	},
	interval_update: function() {
		setInterval(function() {
			if(Router.HashLocation.getCurrentPath() == "/order") {
				order_ctrl.update();
			};
		}, root.options.update_time_order);
	},
	edit_order: function(elem) {
		root.send("edit_order", elem, function(){
			order_ctrl.update();
		});
	},
}