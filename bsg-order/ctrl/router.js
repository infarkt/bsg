var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

AppMain = React.createClass({
    render: function() {
        return (
            <Menu/>
        )
    }
});

var routes_main = (
  <Route name="app" path="/" handler={AppMain}>
    <Route name="new_order" path="/new_order" handler={NewOrder}/>
    <Route name="order" path="/order" handler={Order}/>
    <Route name="users" path="/users" handler={Users}/>
    <DefaultRoute handler={NewOrder}/>
  </Route>
);

function start() {
    see_cookie();
}

$(document).ready(function () {
    start();
});


function auth() {
    React.render(<Auth/>, document.body);
}

function see_cookie() {
    var key = localStorage.getItem('key');
    if(key == null) {
        auth(); 
    } else {
        root.send("auto_login", {"key": key}, function(result) {
            if(result != 0) {
                var doc = JSON.parse(result.doc);
                var users = _.pluck(doc.users, 'key');
                root.user_index = users.indexOf(key);
                root.company_index = result.id;
                root.set_company(doc);
            } else {
                auth();
            }
        });
    }
}

function start_router() {
    Router.run(routes_main, function (Handler) {
        React.render(<Handler/>, document.body);
    });
}

