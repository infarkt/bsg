Menu = React.createClass({
    getInitialState: function() {
    return  {user: root.company.users[root.user_index].login};
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            <div>
                <nav className="navbar navbar-default" role="navigation">
                    <div className="navbar-header">
                        <a className="navbar-brand">
                            <img alt="" src="../styles/images/logo.png"/>
                            <span id="sidebar-exp"></span>
                        </a>
                    </div>
                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav navbar-right header-drop-right">
                            <li className="dropdown user-dropdown-one">
                                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                                    {this.state.user}
                                    <img className="img-user" alt="" src="../styles/images/user-img.png"/>
                                    <b className="caret"></b>
                                </a>
                                <ul className="dropdown-menu user-dropdown-two">
                                    <li onClick={this.logOut} className="log-out">
                                        <a>Выход</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div id="wrapper" className="">
                    <div id="body-overlay"></div>
                    <div id="sidebar">
                        <ul className="drop-area">
                            <li className="no-back">
                                <a href="#/new_order">Новая заявка</a>
                            </li>
                            <li className="no-back">
                                <a href="#/order">Все заявки</a>
                            </li>
                            <li className="no-back">
                                <a href="#/users">Пользователи</a>
                            </li>
                        </ul>
                    </div> 
                    <div id="wrap">
                        <div className="container">
                        {}
                        <RouteHandler/>
                        </div>
                    </div>   
                </div>
                <div id="footer">
                    <div className="row">
                        <div className="col-md-6 left">
                            <ul>
                                <li>
                                    <a href="#">Раз</a>
                                    <span>•</span>
                                </li>
                                <li>
                                    <a href="">Два</a>
                                    <span>•</span>
                                </li>
                                <li>
                                    <a href="#">Три</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-6 right">
                            <span className="pull-right">
                                Разработка
                                <a href="ordo-crm.ru">
                                    <strong> Ordo-CRM </strong>
                                </a>
                                2015
                            </span>
                        </div>
                    </div>
                </div>  
            </div>
        );
    }
});   


/*
    <ul className="header-notifications pull-right">
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>
*/

/*
Суб меню
<li class="">
UI Elements
<ul style="display: none;" class="">
<li>
<span class="icn icon-190"></span>
<a href="ui-sliders.html">UI Sliders</a>
</li>

*/ 

/*
поиск
<div class="search-block clearfix">
<span></span>
<input type="text" placeholder="Are you looking for something?">
</div>

*/


/*<ul className="header-notifications pull-right">
                            <li>
                                <a className="clock" href="#"></a>
                            </li>
                            <li>
                                <a className="task" href="#"></a>
                            </li>
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>*/

/*
<div id="main" className="z-depth-1">
              <div id="header">
              </div>
              <div className="ui inverted db1 menu">
                <a onClick={this.setClass} className={this.state.class_menu["Заявки"]} href="#/order">Заявки</a>
                <a onClick={this.setClass} className={this.state.class_menu["Отчёты"]}>Отчёты</a>
                <a onClick={this.setClass} className={this.state.class_menu["Справочники"]} href="#/lists">Справочники</a>
                <div className="right menu">
                    <div className="item blue">{this.state.user}</div>
                    <div onClick={this.logOut} 
                        className="item piont"><i className="power icon orange"></i></div>
                </div>
              </div>
              {}
              <RouteHandler/>
            </div>
*/