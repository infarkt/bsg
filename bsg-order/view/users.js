Users = React.createClass({
    componentDidMount: function() {

    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-1">
                        
                    </div>
                    <div className="col-md-10">
                        <UsersForma/>
                        <UserList/>
                    </div>
                    <div className="col-md-1">
                        
                    </div>
                </div>
            </div>    
        )
    }
});

/*<UsersList/>*/

/*UsersMenu = React.createClass({
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_user_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "admin")} 
                            className={this.state.filter == "admin" ? "admin filter_active" : "admin"}>
                            <span>Администраторы</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "master")}
                            className={this.state.filter == "master" ? "master filter_active" : "master"}>
                            <span>Инженеры</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});*/


UsersForma = React.createClass({
    getInitialState: function() {
        return {data: new model.users(),
                head: "Добавить сотрудника",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(num_check, val) {
        this.state.data.works_type[num_check] = val;
        this.forceUpdate();
    },
    add: function(callback) {
        this.setState({data: new model.users(),
                        head: "Добавить сотрудника",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        index: ""});
        this.callbackSetElem = callback;
        $('#UsersModal').modal('show');
    },
    edit: function(index, callback) {
        this.setState({data: root.company.users[index],
                        head: "Редактировать сотрудника",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        index: index});
        this.callbackSetElem = callback;
        $('#UsersModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.login == '' || this.state.data.mail == '') {
            this.setState({veri_view_content: "Нужно указать пользователя и почту!"});
            this.setState({veri_view: true});
        } else {
            this.state.data.key = root.key(this.state.data.login + this.state.data.mail);
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                root.company.users.push(this.state.data);
                    break;
            case "edit":   
                root.company.users[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem();
        new_order_ctrl.edit(root.company);
        $('#UsersModal').modal('hide');
    },
    del: function() {
        root.company.users.splice(this.state.index, 1);
        this.callbackSetElem();
        new_order_ctrl.edit(root.company);
        $('#UsersModal').modal('hide');
    },
    componentDidMount: function() {
        new_order_ctrl.addUsers = this.add;
        new_order_ctrl.editUsers = this.edit;
    },
    render: function() {
        return (
            <div id="UsersModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w660">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.head}</h4>
                        </div>
                        <div className="veri-modal" style={this.state.veri_view == true ? {display: ''} : {display: 'none'}}>
                            {this.state.veri_view_content}</div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputEng head="Пользователь"
                                        name="login"
                                        val={this.state.data.login}
                                        bind={this.inputBind}/>
                                    <div className="input-f-wrapper">
                                        <label>ФИО</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.data.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Должность</label>
                                        <input type="text" 
                                                name="post"
                                                onChange={this.inputBind} 
                                                value={this.state.data.post}/>
                                    </div>
                                </div>
                                <div className="col-md-6">  
                                    <div className="input-f-wrapper">
                                        <label>Телефон</label>
                                        <input type="text" 
                                                name="phone"
                                                onChange={this.inputBind} 
                                                value={this.state.data.phone}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.data.mail}/>
                                    </div>
                                </div>     
                            </div>      
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={this.state.view_del == true ? {display: ''} : {display: 'none'}} 
                                onDoubleClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.veriForm} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
});


UserList = React.createClass({
    getInitialState: function() {
        return {data: root.company.users};
    },
    setDoc: function() {
        this.setState({data: root.company.users});
    },
    add: function() {
        new_order_ctrl.addUsers(this.setDoc);
    },
    edit: function(index) {
        new_order_ctrl.editUsers(index, this.setDoc);
    },
    componentDidMount: function() {

    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Сотрудники {this.state.data.length}
                    <a className="btn blue right" onClick={this.add}>Добавить сотрудника</a>
                </h3>
                <table className="table simple">
                    <thead>
                        <tr>
                            <th>ФИО</th>
                            <th>Пользователь</th>
                            <th>Почта</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map(function(elem, index) {
                            return (
                                <tr onClick={edit.bind(self, index)} 
                                    key={elem.key}>
                                    <td>{elem.fio}</td>
                                    <td>{elem.login}</td>
                                    <td>{elem.mail}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div> 
        )
    }
});