Auth = React.createClass({
    getInitialState: function() {
        return {error_view: "none",
                login: "",
                pass: ""};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    set_view_error: function(val) {
        this.setState({error_view: val});
    },
    login: function() {
        var state = this.state;
        var _set_view_error = this.set_view_error;
        root.send("login", this.state, function(result) {
            if(result != 0) {
                _set_view_error('none');
                console.log(result);
                var doc = JSON.parse(result.doc);
                var users = _.pluck(doc.users, 'key');
                root.user_index = users.indexOf(root.key(state.login + state.pass));
                root.company_index = result.id;
                root.set_company(doc);
            } else {
                _set_view_error('block');
            }
        });
    },
    render: function() {
        return (
            <div id="login">
                <div className="logo">
                    <a href="index.html">
                        <img alt="" src="../styles/images/logo-crm.png"/>
                    </a>
                </div>
                <div className="wrapper-login">
                    <input className="user" 
                            type="text" 
                            name="login" 
                            onChange={this.inputBind} 
                            value={this.state.login}/>
                    <input className="password" 
                            type="password" 
                            name="pass" 
                            onChange={this.inputBind} 
                            value={this.state.pass}/>
                </div>
                <div className="message" style={{display: this.state.error_view}}><h4>Ошибка!</h4></div>
                <a className="login-button" onClick={this.login}>Вход</a>
                <span>
                    <a href="#">Восстановить пароль</a>
                </span>
            </div>
        );
    } 
});