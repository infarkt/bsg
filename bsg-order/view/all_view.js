Auth = React.createClass({displayName: "Auth",
    getInitialState: function() {
        return {error_view: "none",
                login: "",
                pass: ""};
    },
    inputBind: function(event) {
        this.state[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    set_view_error: function(val) {
        this.setState({error_view: val});
    },
    login: function() {
        var state = this.state;
        var _set_view_error = this.set_view_error;
        root.send("login", this.state, function(result) {
            if(result != 0) {
                _set_view_error('none');
                console.log(result);
                var doc = JSON.parse(result.doc);
                var users = _.pluck(doc.users, 'key');
                root.user_index = users.indexOf(root.key(state.login + state.pass));
                root.company_index = result.id;
                root.set_company(doc);
            } else {
                _set_view_error('block');
            }
        });
    },
    render: function() {
        return (
            React.createElement("div", {id: "login"}, 
                React.createElement("div", {className: "logo"}, 
                    React.createElement("a", {href: "index.html"}, 
                        React.createElement("img", {alt: "", src: "../styles/images/logo-crm.png"})
                    )
                ), 
                React.createElement("div", {className: "wrapper-login"}, 
                    React.createElement("input", {className: "user", 
                            type: "text", 
                            name: "login", 
                            onChange: this.inputBind, 
                            value: this.state.login}), 
                    React.createElement("input", {className: "password", 
                            type: "password", 
                            name: "pass", 
                            onChange: this.inputBind, 
                            value: this.state.pass})
                ), 
                React.createElement("div", {className: "message", style: {display: this.state.error_view}}, React.createElement("h4", null, "Ошибка!")), 
                React.createElement("a", {className: "login-button", onClick: this.login}, "Вход"), 
                React.createElement("span", null, 
                    React.createElement("a", {href: "#"}, "Восстановить пароль")
                )
            )
        );
    } 
});
Menu = React.createClass({displayName: "Menu",
    getInitialState: function() {
    return  {user: root.company.users[root.user_index].login};
    },
    logOut: function() {
        root.out_user();
    },
    componentDidMount: function() {
        $("body").css("background-color","#f0f1f0");
        animate.init_sidebar();
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("nav", {className: "navbar navbar-default", role: "navigation"}, 
                    React.createElement("div", {className: "navbar-header"}, 
                        React.createElement("a", {className: "navbar-brand"}, 
                            React.createElement("img", {alt: "", src: "../styles/images/logo.png"}), 
                            React.createElement("span", {id: "sidebar-exp"})
                        )
                    ), 
                    React.createElement("div", {className: "collapse navbar-collapse navbar-ex1-collapse"}, 
                        React.createElement("ul", {className: "nav navbar-nav navbar-right header-drop-right"}, 
                            React.createElement("li", {className: "dropdown user-dropdown-one"}, 
                                React.createElement("a", {className: "dropdown-toggle", "data-toggle": "dropdown", href: "#"}, 
                                    this.state.user, 
                                    React.createElement("img", {className: "img-user", alt: "", src: "../styles/images/user-img.png"}), 
                                    React.createElement("b", {className: "caret"})
                                ), 
                                React.createElement("ul", {className: "dropdown-menu user-dropdown-two"}, 
                                    React.createElement("li", {onClick: this.logOut, className: "log-out"}, 
                                        React.createElement("a", null, "Выход")
                                    )
                                )
                            )
                        )
                    )
                ), 
                React.createElement("div", {id: "wrapper", className: ""}, 
                    React.createElement("div", {id: "body-overlay"}), 
                    React.createElement("div", {id: "sidebar"}, 
                        React.createElement("ul", {className: "drop-area"}, 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/new_order"}, "Новая заявка")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/order"}, "Все заявки")
                            ), 
                            React.createElement("li", {className: "no-back"}, 
                                React.createElement("a", {href: "#/users"}, "Пользователи")
                            )
                        )
                    ), 
                    React.createElement("div", {id: "wrap"}, 
                        React.createElement("div", {className: "container"}, 
                        
                        React.createElement(RouteHandler, null)
                        )
                    )
                ), 
                React.createElement("div", {id: "footer"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-6 left"}, 
                            React.createElement("ul", null, 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Раз"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: ""}, "Два"), 
                                    React.createElement("span", null, "•")
                                ), 
                                React.createElement("li", null, 
                                    React.createElement("a", {href: "#"}, "Три")
                                )
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6 right"}, 
                            React.createElement("span", {className: "pull-right"}, 
                                "Разработка", 
                                React.createElement("a", {href: "ordo-crm.ru"}, 
                                    React.createElement("strong", null, " Ordo-CRM ")
                                ), 
                                "2015"
                            )
                        )
                    )
                )
            )
        );
    }
});   


/*
    <ul className="header-notifications pull-right">
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>
*/

/*
Суб меню
<li class="">
UI Elements
<ul style="display: none;" class="">
<li>
<span class="icn icon-190"></span>
<a href="ui-sliders.html">UI Sliders</a>
</li>

*/ 

/*
поиск
<div class="search-block clearfix">
<span></span>
<input type="text" placeholder="Are you looking for something?">
</div>

*/


/*<ul className="header-notifications pull-right">
                            <li>
                                <a className="clock" href="#"></a>
                            </li>
                            <li>
                                <a className="task" href="#"></a>
                            </li>
                            <li className="last">
                                <a className="globe" href="#"></a>
                                <span className="lbl">12</span>
                            </li>
                        </ul>*/

/*
<div id="main" className="z-depth-1">
              <div id="header">
              </div>
              <div className="ui inverted db1 menu">
                <a onClick={this.setClass} className={this.state.class_menu["Заявки"]} href="#/order">Заявки</a>
                <a onClick={this.setClass} className={this.state.class_menu["Отчёты"]}>Отчёты</a>
                <a onClick={this.setClass} className={this.state.class_menu["Справочники"]} href="#/lists">Справочники</a>
                <div className="right menu">
                    <div className="item blue">{this.state.user}</div>
                    <div onClick={this.logOut} 
                        className="item piont"><i className="power icon orange"></i></div>
                </div>
              </div>
              {}
              <RouteHandler/>
            </div>
*/
NewOrder = React.createClass({displayName: "NewOrder",
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-2"}), 
                    React.createElement("div", {className: "col-md-8"}, 
                        React.createElement(NewOrderMaster, null)
                    ), 
                    React.createElement("div", {className: "col-md-2"})
                )
            )    
        )
    }
});

NewOrderMaster = React.createClass({displayName: "NewOrderMaster",
    getInitialState: function() {
        return {master_step: 0,
                company: root.company,
                user_index: root.user_index,
                view_all_address: false,
                reason: "",
                result_type: []};
    },
    toogle_all_address: function() {
        this.setState({view_all_address: !this.state.view_all_address});
    },
    addAddress: function(elem) {
        this.state.company.users[this.state.user_index].my_address.push(elem);
        this.forceUpdate();
        new_order_ctrl.edit(this.state.company);
    },
    deleteAddress: function(elem) {
        this.state.company.users[this.state.user_index].my_address.splice(this.state.company.users[this.state.user_index].my_address.indexOf(elem),1);
        this.forceUpdate();
        new_order_ctrl.edit(this.state.company);
    },
    selectAddress: function(elem) {
        var full_address = _.uniq(_.flatten(_.pluck(this.state.company.object, 'address')));
        var all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.company.object, 'address')), 'address'));
        var coords = "";
        for(var i in this.state.full_address) {
            if(this.state.full_address[i].address == elem) {
                coords = this.state.company_full_address[i].coords;
            }
        }
        this.state.result_type = new_order_ctrl.get_order_type(elem);
        new_order_ctrl.setForma('address', elem);
        new_order_ctrl.setForma('coords', coords);
        this.stepMaster(1);
        this.forceUpdate();
    },
    stepMaster: function(step) {
        this.setState({master_step: step});
    },
    bindReason: function(event) {
        this.setState({reason: event.target.value});
    },
    setReason: function() {
        new_order_ctrl.setForma('reason', this.state.reason);
        if(this.state.result_type.length > 1) {
            this.setState({master_step: 2});
        } else {
            this.setContract(0);
        }
    },
    setContract: function(index) {
        new_order_ctrl.setForma('contract', new_order_ctrl.double_address[index].object_name);
        new_order_ctrl.setForma('type', this.state.result_type[index]);
        this.see_result();
    },
    see_result: function() {
        this.setState({master_step: 3});
    },
    backStep: function() {
        if(this.state.result_type.length > 1) {
            this.setState({master_step: 2});
        } else {
            this.setState({master_step: 1});
        }
    },
    resetMaster: function() {
        this.setState({master_step: 0,
                        company: root.company,
                        user_index: root.user_index,
                        view_all_address: false,
                        reason: "",
                        result_type: []});
    },
    componentDidMount: function() {
        new_order_ctrl.backStep = this.backStep;
        new_order_ctrl.resetMaster = this.resetMaster;
    },
    render: function() {
        var my_address = this.state.company.users[this.state.user_index].my_address;
        var all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.company.object, 'address')), 'address')),
        self = this, addAddress = this.addAddress, deleteAddress = this.deleteAddress, 
        selectAddress = this.selectAddress, setContract = this.setContract;
        return (
            React.createElement("div", {className: "panel-style space m-top-0 ml510"}, 
                React.createElement("h3", {className: "heading-title"}, "Создание новой заявки"), 
                React.createElement("div", {className: "type-four-tab-wrapper mh200"}, 
                    React.createElement("ul", {className: "nav nav-tabs easy-style type-four"}, 
                        React.createElement("li", {className: this.state.master_step == 0 ? "active" : ""}, 
                            React.createElement("a", null, "1. Выберете адрес")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 1 ? "active" : ""}, 
                            React.createElement("a", null, "2. Опишите проблему")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 2 ? "active" : "", 
                            style: this.state.result_type.length > 1 ? {display: ''} : {display: 'none'}}, 
                            React.createElement("a", null, "3. Укажите тип работ")
                        ), 
                        React.createElement("li", {className: this.state.master_step == 3 ? "active" : ""}, 
                            React.createElement("a", null, "Результат")
                        )
                    ), 

                    React.createElement("div", {className: "tab-content easy-style type-four"}, 
                        React.createElement("div", {className: this.state.master_step == 0 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("p", {style: my_address.length == 0 ? {display: ''} : {display: 'none'}}, 
                                "Сейчас нет закреплённых за вами адресов, вы можете выбрать их ниже из зарегестрированных адресов."), 
                            React.createElement("table", {className: "table simple", style: my_address.length != 0 ? {display: ''} : {display: 'none'}}, 
                                React.createElement("tbody", null, 
                                    my_address.map(function(elem, index) {
                                        return (
                                            React.createElement("tr", {key: elem}, 
                                                React.createElement("td", null, elem, 
                                                    React.createElement("a", {className: "btn simple green_h_t right", 
                                                        onClick: selectAddress.bind(self, elem)}, "Выбрать")
                                                )
                                            )
                                        )
                                    })
                                )
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 1 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("div", {className: "textarea-i-wrapper"}, 
                                React.createElement("textarea", {onChange: this.bindReason, 
                                        value: this.state.reason})
                            ), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 0)}, "Назад"), 
                                React.createElement("a", {className: "btn green right", onClick: this.setReason, 
                                    style: this.state.reason != "" ? {display: ''} : {display: 'none'}}, "Дальше")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 2 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement("table", {className: "table simple"}, 
                                React.createElement("tbody", null, 
                                    this.state.result_type.map(function(elem, index) {
                                        return (
                                            React.createElement("tr", {key: elem}, 
                                                React.createElement("td", null, elem, 
                                                    React.createElement("a", {className: "btn simple green_h_t right", 
                                                        onClick: setContract.bind(self, index)}, "Выбрать")
                                                )
                                            )
                                        )
                                    })
                                )
                            ), 
                            React.createElement("div", {className: "master-footer"}, 
                                React.createElement("a", {className: "btn orange left", onClick: this.stepMaster.bind(self, 1)}, "Назад")
                            )
                        ), 
                        React.createElement("div", {className: this.state.master_step == 3 ? "tab-pane active" : "tab-pane"}, 
                            React.createElement(SeeOrderMaster, null)
                        )
                    )
                ), 
                React.createElement("div", {className: "panel-group type-two accordion", style: this.state.master_step == 0 ? {display: ''} : {display: 'none'}}, 
                    React.createElement("div", {className: "panel-heading"}, 
                        React.createElement("h4", {className: "panel-title"}, 
                            React.createElement("a", {onClick: this.toogle_all_address, className: this.state.view_all_address == true ? 'collapsed' : ''}, 
                                "Все зарегестрированные адреса")
                        )
                    ), 
                    React.createElement("div", {className: "panel-collapse", style: this.state.view_all_address == true ? {display: ''} : {display: 'none'}}, 
                        React.createElement("p", {style: all_address.length == 0 ? {display: ''} : {display: 'none'}}, 
                            "У вас нет зарегестрированных адресов, обратитесь к вашему менеджеру."), 
                        React.createElement("table", {className: "table simple", style: all_address.length != 0 ? {display: ''} : {display: 'none'}}, 
                            React.createElement("tbody", null, 
                                all_address.map(function(elem, index) {
                                    return (
                                        React.createElement("tr", {key: elem}, 
                                            React.createElement("td", null, elem, 
                                                React.createElement("a", {className: "btn simple green_h_t right", 
                                                    style: my_address.indexOf(elem) == -1 ? {display: ''} : {display: 'none'}, 
                                                    onClick: addAddress.bind(self, elem)}, "Добаить в мои адреса"), 
                                                React.createElement("a", {className: "btn simple red_t right", 
                                                    style: my_address.indexOf(elem) != -1 ? {display: ''} : {display: 'none'}, 
                                                    onClick: deleteAddress.bind(self, elem)}, "Удалить из моих адресов")
                                            )
                                        )
                                    )
                                })
                            )
                        )
                    )
                )
            )     
        )
    }
});


// Компонет вложенный но выполнен выделенно!
SeeOrderMaster = React.createClass({displayName: "SeeOrderMaster",
    getInitialState: function() {
        return {master_step: 0,
                order: new model.order(),
                save: null};
    },
    setForma: function(name, val) {
        this.state.order.primary[name] =  val;
        this.forceUpdate();
    },
    backStep: function() {
        new_order_ctrl.backStep();
    },
    saveOrder: function() {
        var self = this;
        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                root.send_mail("9219505184@mail.ru", "Новая заявка");
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                root.send_mail("9312323303@mail.ru", "Новая заявка");
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                root.send_mail("9213005737@mail.ru", "Новая заявка");
                    break;
        }
        new_order_ctrl.add_order(this.state.order, function(data) {
            console.log(data);
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
        order_ctrl.load();
    },
    errorSave: function() {
        this.setState({save: false});
    },
    resetMaster: function() {
        new_order_ctrl.resetMaster();
        this.setState({master_step: 0,
                        order: new model.order(),
                        save: null});
    },
    componentDidMount: function() {
        new_order_ctrl.setForma = this.setForma;
    },
    render: function() {
        var order = this.state.order;
        return (
                React.createElement("div", null, 
                    React.createElement("div", {className: "form-see", style: this.state.save == null ? {display: ''} : {display: 'none'}}, 
                        React.createElement("span", {className: "grey-title"}, "Заказчик"), 
                        React.createElement("ul", null, 
                            React.createElement("li", null, order.primary.company), 
                            React.createElement("li", null, order.primary.persone), 
                            React.createElement("li", null, order.primary.phone)
                        ), 
                        React.createElement("div", {className: "separator"}), 
                        React.createElement("span", {className: "grey-title"}, "Заявка"), 
                        React.createElement("ul", null, 
                            React.createElement("li", null, "№ ", order.system.key), 
                            React.createElement("li", null, order.primary.contract), 
                            React.createElement("li", null, order.primary.address), 
                            React.createElement("li", null, order.primary.type), 
                            React.createElement("li", {className: "blue_l_t"}, order.primary.reason)
                        ), 
                        React.createElement("div", {className: "separator"}), 
                        React.createElement("div", {className: "master-footer"}, 
                            React.createElement("a", {className: "btn orange left", onClick: this.backStep}, "Назад"), 
                            React.createElement("a", {className: "btn green right", onClick: this.saveOrder}, "Отправить")
                        )
                    ), 
                    React.createElement("div", {style: this.state.save == true ? {display: ''} : {display: 'none'}}, 
                        React.createElement("p", null, "Заявка успешно отправлена!"), 
                        React.createElement("div", {className: "master-footer"}, 
                            React.createElement("a", {className: "btn green right", onClick: this.resetMaster}, "В начало")
                        )
                    ), 
                    React.createElement("div", {style: this.state.save == false ? {display: ''} : {display: 'none'}}, 
                        React.createElement("p", null, "Внимание заявка НЕ ОТПРАВЛЕНА!")
                    )
                )
        )
    }
});
Order = React.createClass({displayName: "Order",
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-3"}, 
                        React.createElement(OrderMenu, null)
                    ), 
                    React.createElement("div", {className: "col-md-9"}, 
                        React.createElement(OrderList, null)
                    )
                )
            )    
        )
    }
});



OrderMenu = React.createClass({displayName: "OrderMenu",
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        order_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            React.createElement("div", {className: "panel-style space custom-menu no-pad-r m-bot-30 m-top-0"}, 
                React.createElement("span", {className: "grey-title"}, "ФИЛЬТР"), 
                React.createElement("ul", null, 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "process"), 
                            className: this.state.filter == "process" ? "pause filter_active" : "pause"}, 
                            React.createElement("span", null, "В обработке"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "job"), 
                            className: this.state.filter == "job" ? "car filter_active" : "car"}, 
                            React.createElement("span", null, "В работе"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "ок"), 
                            className: this.state.filter == "ок" ? "ckeck filter_active" : "ckeck"}, 
                            React.createElement("span", null, "Выполненые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "reject"), 
                            className: this.state.filter == "reject" ? "attention filter_active" : "attention"}, 
                            React.createElement("span", null, "Отклонённые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "close"), 
                            className: this.state.filter == "close" ? "close_0 filter_active" : "close_0"}, 
                            React.createElement("span", null, "Закрытые"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    ), 
                    React.createElement("li", null, 
                        React.createElement("a", {onClick: this.setFilter.bind(self, "all"), 
                            className: this.state.filter == "all" ? "all filter_active" : "all"}, 
                            React.createElement("span", null, "Все"), 
                            React.createElement("span", {className: "focus-custom-menu"})
                        )
                    )
                )
            )
        )
    }
});


OrderList = React.createClass({displayName: "OrderList",
    getInitialState: function() {
        return {data: order_ctrl.store,
                request: jQuery.extend(true, {}, order_ctrl.default_request_data),
                display: jQuery.extend(true, {}, order_ctrl.display_step),
                order_status: order_status};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        order_ctrl.update("change_limit");
    },
    stepPrev: function() {
        order_ctrl.update("step_prev");
    },
    stepNext: function() {
        order_ctrl.update("step_next");
    },
    add: function() {
        
    },
    edit: function(elem) {
        order_ctrl.edit_order(elem);
    },
    orderClose: function(elem) {
        elem.doc.primary.status = 4;
        // Запись в историю
        system_event[4].date = format.date_ms();
        elem.doc.system_history.push(system_event[4]);
        this.edit(elem);
    },
    componentDidMount: function() {
        order_ctrl.setViewList = this.update;
        order_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, orderClose = this.orderClose, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Заявки ", this.state.request.offset, " - ", (this.state.request.offset*1) + (this.state.request.limit*1), " из ", this.state.data.count, 
                    React.createElement("a", {className: "btn blue right", href: "#/new_order", onClick: this.add}, "Добавить")
                ), 
                React.createElement("table", {className: "table simple"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "Дата заявки"), 
                            React.createElement("th", null, "Номер заявки"), 
                            React.createElement("th", null, "Содержание"), 
                            React.createElement("th", null, "Статус"), 
                            React.createElement("th", null, "Действие")
                        )
                    ), 
                    React.createElement("tbody", null, 
                        this.state.data.data.map(function(elem) {
                            return (
                                React.createElement("tr", {key: elem.id}, 
                                    React.createElement("td", null, format.sql_to_date_time(elem.doc.system.add_date)), 
                                    React.createElement("td", null, elem.doc.system.key), 
                                    React.createElement("td", null, elem.doc.primary.reason), 
                                    React.createElement("td", null, self.state.order_status[elem.doc.primary.status]), 
                                    React.createElement("td", null, React.createElement("a", {className: "btn simple green_h_t right", style: elem.doc.primary.status == 2 ? {display: ''} : {display: 'none'}, 
                                        onClick: orderClose.bind(self, elem)}, "Закрыть"))
                                )
                            )
                        })
                    )
                ), 
                React.createElement("div", {className: "table-footer"}, 
                    React.createElement("div", {className: "wrap-input-button input-f-wrapper w170 left tcenter"}, 
                        React.createElement("input", {className: "input-button", 
                                type: "text", 
                                name: "limit", 
                                onChange: this.limitSet, 
                                value: this.state.request.limit}), 
                        React.createElement("a", {className: "btn default", onClick: this.limitChange}, "Показать")
                    ), 
                    React.createElement("div", {className: "table-step-div"}, 
                        React.createElement("a", {className: "btn default middle left", 
                            style: {display: this.state.display.prev_button}, 
                            onClick: this.stepPrev}, "Назад"), 
                        React.createElement("a", {className: "btn default middle right", 
                            style: {display: this.state.display.next_button}, 
                            onClick: this.stepNext}, "Вперёд")
                    )
                )
            ) 
        )
    }
});


/*MainUsersForm = React.createClass({
    getInitialState: function() {
        return {doc: new model.main_user(),
                header: "Новый пользователь",
                type: "add",
                id: "",
                delButton: "none",
                role: select.role};
    },
    inputBind: function(event) {
        this.state.doc[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        console.log(val);
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                main_user_ctrl.add(this.state.doc); 
                    break;
            case "edit":
                main_user_ctrl.edit(this.state.doc, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({doc: new model.main_user(), 
                        header: "Новый пользователь", 
                        type: "add",
                        delButton: "none"});
        $('#MainUsersModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({doc: elem.doc, 
                        id: elem.id, 
                        header: "Редактирование пользователя", 
                        type: "edit",
                        delButton: ""});
        $('#MainUsersModal').modal('show');
    },
    del: function() {
        main_user_ctrl.del(this.state.id);
    },
    componentDidMount: function() {
        main_user_ctrl.addForm = this.addForm;
        main_user_ctrl.editForm = this.editForm;
    },
    render: function() {
        return (
            <div id="MainUsersModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w700">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.header}</h4>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputEng name="login"
                                                head="Логин"
                                                bind={this.inputBind}
                                                val={this.state.doc.login}/>
                                    <div className="input-f-wrapper">
                                        <label>ФИО</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Роль</label>
                                            <InputSelect name="role"
                                                        val={this.state.doc.role}
                                                        bind={this.selectBind}
                                                        data={this.state.role}/>
                                    </div>
                                    
                                </div>
                                <div className="col-md-6">   
                                    <InputPhone name="phone"
                                                head="Телефон"
                                                bind={this.inputBind}
                                                val={this.state.doc.phone}/>              
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.mail}/>
                                    </div>
                                    <InputEng name="pass"
                                                head="Логин"
                                                bind={this.inputBind}
                                                val={this.state.doc.pass}/>
                                    
                                </div>  
                            </div>    
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={{display: this.state.delButton}} 
                                onClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});*/


Users = React.createClass({displayName: "Users",
    componentDidMount: function() {

    },
    render: function() {
        return (
            React.createElement("div", {className: "wrapper-content"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-1"}
                        
                    ), 
                    React.createElement("div", {className: "col-md-10"}, 
                        React.createElement(UsersForma, null), 
                        React.createElement(UserList, null)
                    ), 
                    React.createElement("div", {className: "col-md-1"}
                        
                    )
                )
            )    
        )
    }
});

/*<UsersList/>*/

/*UsersMenu = React.createClass({
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        main_user_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "admin")} 
                            className={this.state.filter == "admin" ? "admin filter_active" : "admin"}>
                            <span>Администраторы</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "master")}
                            className={this.state.filter == "master" ? "master filter_active" : "master"}>
                            <span>Инженеры</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});*/


UsersForma = React.createClass({displayName: "UsersForma",
    getInitialState: function() {
        return {data: new model.users(),
                head: "Добавить сотрудника",
                view_del: false,
                veri_view: false,
                veri_view_content: "",
                save_type: "add",
                index: ""};
    },
    callbackSetElem: "",
    inputBind: function(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    checkGroupBind: function(num_check, val) {
        this.state.data.works_type[num_check] = val;
        this.forceUpdate();
    },
    add: function(callback) {
        this.setState({data: new model.users(),
                        head: "Добавить сотрудника",
                        view_del: false,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "add",
                        index: ""});
        this.callbackSetElem = callback;
        $('#UsersModal').modal('show');
    },
    edit: function(index, callback) {
        this.setState({data: root.company.users[index],
                        head: "Редактировать сотрудника",
                        view_del: true,
                        veri_view: false,
                        veri_view_content: "",
                        save_type: "edit",
                        index: index});
        this.callbackSetElem = callback;
        $('#UsersModal').modal('show');
    },
    veriForm: function() {
        if(this.state.data.login == '' || this.state.data.mail == '') {
            this.setState({veri_view_content: "Нужно указать пользователя и почту!"});
            this.setState({veri_view: true});
        } else {
            this.state.data.key = root.key(this.state.data.login + this.state.data.mail);
            this.setState({veri_view: false});
            this.save();
        }
    },
    save: function() {
        switch(this.state.save_type) {
            case "add":
                root.company.users.push(this.state.data);
                    break;
            case "edit":   
                root.company.users[this.state.index] = this.state.data;
                    break;      
        }
        this.callbackSetElem();
        new_order_ctrl.edit(root.company);
        $('#UsersModal').modal('hide');
    },
    del: function() {
        root.company.users.splice(this.state.index, 1);
        this.callbackSetElem();
        new_order_ctrl.edit(root.company);
        $('#UsersModal').modal('hide');
    },
    componentDidMount: function() {
        new_order_ctrl.addUsers = this.add;
        new_order_ctrl.editUsers = this.edit;
    },
    render: function() {
        return (
            React.createElement("div", {id: "UsersModal", className: "modal fade", "aria-hidden": "true", "aria-labelledby": "myModalLabel", role: "dialog"}, 
                React.createElement("div", {className: "modal-dialog w660"}, 
                    React.createElement("div", {className: "modal-content"}, 
                        React.createElement("div", {className: "modal-header"}, 
                            React.createElement("button", {className: "close", "aria-hidden": "true", "data-dismiss": "modal", type: "button"}, "×"), 
                            React.createElement("h4", {id: "myModalLabel", className: "modal-title"}, this.state.head)
                        ), 
                        React.createElement("div", {className: "veri-modal", style: this.state.veri_view == true ? {display: ''} : {display: 'none'}}, 
                            this.state.veri_view_content), 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement(InputEng, {head: "Пользователь", 
                                        name: "login", 
                                        val: this.state.data.login, 
                                        bind: this.inputBind}), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "ФИО"), 
                                        React.createElement("input", {type: "text", 
                                                name: "fio", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.fio})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Должность"), 
                                        React.createElement("input", {type: "text", 
                                                name: "post", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.post})
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-6"}, 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Телефон"), 
                                        React.createElement("input", {type: "text", 
                                                name: "phone", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.phone})
                                    ), 
                                    React.createElement("div", {className: "input-f-wrapper"}, 
                                        React.createElement("label", null, "Почта"), 
                                        React.createElement("input", {type: "text", 
                                                name: "mail", 
                                                onChange: this.inputBind, 
                                                value: this.state.data.mail})
                                    )
                                )
                            )
                        ), 
                        React.createElement("div", {className: "modal-footer"}, 
                            React.createElement("button", {className: "btn red left", 
                                style: this.state.view_del == true ? {display: ''} : {display: 'none'}, 
                                onDoubleClick: this.del, type: "button"}, "Удалить"), 
                            React.createElement("button", {className: "btn default", "data-dismiss": "modal", type: "button"}, "Отмена"), 
                            React.createElement("button", {className: "btn green", onClick: this.veriForm, type: "button"}, "Сохранить")
                        )
                    )
                )
            )
        )    
    }
});


UserList = React.createClass({displayName: "UserList",
    getInitialState: function() {
        return {data: root.company.users};
    },
    setDoc: function() {
        this.setState({data: root.company.users});
    },
    add: function() {
        new_order_ctrl.addUsers(this.setDoc);
    },
    edit: function(index) {
        new_order_ctrl.editUsers(index, this.setDoc);
    },
    componentDidMount: function() {

    },
    render: function() {
        var edit = this.edit, self = this;
        return (
            React.createElement("div", {className: "panel-style space m-top-0"}, 
                React.createElement("h3", {className: "heading-title"}, "Сотрудники ", this.state.data.length, 
                    React.createElement("a", {className: "btn blue right", onClick: this.add}, "Добавить сотрудника")
                ), 
                React.createElement("table", {className: "table simple"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "ФИО"), 
                            React.createElement("th", null, "Пользователь"), 
                            React.createElement("th", null, "Почта")
                        )
                    ), 
                    React.createElement("tbody", null, 
                        this.state.data.map(function(elem, index) {
                            return (
                                React.createElement("tr", {onClick: edit.bind(self, index), 
                                    key: elem.key}, 
                                    React.createElement("td", null, elem.fio), 
                                    React.createElement("td", null, elem.login), 
                                    React.createElement("td", null, elem.mail)
                                )
                            )
                        })
                    )
                )
            ) 
        )
    }
});
var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

AppMain = React.createClass({displayName: "AppMain",
    render: function() {
        return (
            React.createElement(Menu, null)
        )
    }
});

var routes_main = (
  React.createElement(Route, {name: "app", path: "/", handler: AppMain}, 
    React.createElement(Route, {name: "new_order", path: "/new_order", handler: NewOrder}), 
    React.createElement(Route, {name: "order", path: "/order", handler: Order}), 
    React.createElement(Route, {name: "users", path: "/users", handler: Users}), 
    React.createElement(DefaultRoute, {handler: NewOrder})
  )
);

function start() {
    see_cookie();
}

$(document).ready(function () {
    start();
});


function auth() {
    React.render(React.createElement(Auth, null), document.body);
}

function see_cookie() {
    var key = localStorage.getItem('key');
    if(key == null) {
        auth(); 
    } else {
        root.send("auto_login", {"key": key}, function(result) {
            if(result != 0) {
                var doc = JSON.parse(result.doc);
                var users = _.pluck(doc.users, 'key');
                root.user_index = users.indexOf(key);
                root.company_index = result.id;
                root.set_company(doc);
            } else {
                auth();
            }
        });
    }
}

function start_router() {
    Router.run(routes_main, function (Handler) {
        React.render(React.createElement(Handler, null), document.body);
    });
}

