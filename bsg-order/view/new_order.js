NewOrder = React.createClass({
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-2"></div>
                    <div className="col-md-8">
                        <NewOrderMaster/>
                    </div>
                    <div className="col-md-2"></div>
                </div>
            </div>    
        )
    }
});

NewOrderMaster = React.createClass({
    getInitialState: function() {
        return {master_step: 0,
                company: root.company,
                user_index: root.user_index,
                view_all_address: false,
                reason: "",
                result_type: []};
    },
    toogle_all_address: function() {
        this.setState({view_all_address: !this.state.view_all_address});
    },
    addAddress: function(elem) {
        this.state.company.users[this.state.user_index].my_address.push(elem);
        this.forceUpdate();
        new_order_ctrl.edit(this.state.company);
    },
    deleteAddress: function(elem) {
        this.state.company.users[this.state.user_index].my_address.splice(this.state.company.users[this.state.user_index].my_address.indexOf(elem),1);
        this.forceUpdate();
        new_order_ctrl.edit(this.state.company);
    },
    selectAddress: function(elem) {
        var full_address = _.uniq(_.flatten(_.pluck(this.state.company.object, 'address')));
        var all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.company.object, 'address')), 'address'));
        var coords = "";
        for(var i in this.state.full_address) {
            if(this.state.full_address[i].address == elem) {
                coords = this.state.company_full_address[i].coords;
            }
        }
        this.state.result_type = new_order_ctrl.get_order_type(elem);
        new_order_ctrl.setForma('address', elem);
        new_order_ctrl.setForma('coords', coords);
        this.stepMaster(1);
        this.forceUpdate();
    },
    stepMaster: function(step) {
        this.setState({master_step: step});
    },
    bindReason: function(event) {
        this.setState({reason: event.target.value});
    },
    setReason: function() {
        new_order_ctrl.setForma('reason', this.state.reason);
        if(this.state.result_type.length > 1) {
            this.setState({master_step: 2});
        } else {
            this.setContract(0);
        }
    },
    setContract: function(index) {
        new_order_ctrl.setForma('contract', new_order_ctrl.double_address[index].object_name);
        new_order_ctrl.setForma('type', this.state.result_type[index]);
        this.see_result();
    },
    see_result: function() {
        this.setState({master_step: 3});
    },
    backStep: function() {
        if(this.state.result_type.length > 1) {
            this.setState({master_step: 2});
        } else {
            this.setState({master_step: 1});
        }
    },
    resetMaster: function() {
        this.setState({master_step: 0,
                        company: root.company,
                        user_index: root.user_index,
                        view_all_address: false,
                        reason: "",
                        result_type: []});
    },
    componentDidMount: function() {
        new_order_ctrl.backStep = this.backStep;
        new_order_ctrl.resetMaster = this.resetMaster;
    },
    render: function() {
        var my_address = this.state.company.users[this.state.user_index].my_address;
        var all_address = _.uniq(_.pluck(_.flatten(_.pluck(this.state.company.object, 'address')), 'address')),
        self = this, addAddress = this.addAddress, deleteAddress = this.deleteAddress, 
        selectAddress = this.selectAddress, setContract = this.setContract;
        return (
            <div className="panel-style space m-top-0 ml510">
                <h3 className="heading-title">Создание новой заявки</h3>
                <div className="type-four-tab-wrapper mh200">
                    <ul className="nav nav-tabs easy-style type-four">
                        <li className={this.state.master_step == 0 ? "active" : ""}>
                            <a>1. Выберете адрес</a>
                        </li>
                        <li className={this.state.master_step == 1 ? "active" : ""}>
                            <a>2. Опишите проблему</a>
                        </li>
                        <li className={this.state.master_step == 2 ? "active" : ""} 
                            style={this.state.result_type.length > 1 ? {display: ''} : {display: 'none'}}>
                            <a>3. Укажите тип работ</a>
                        </li>
                        <li className={this.state.master_step == 3 ? "active" : ""}>
                            <a>Результат</a>
                        </li>
                    </ul>

                    <div className="tab-content easy-style type-four">
                        <div className={this.state.master_step == 0 ? "tab-pane active" : "tab-pane"}>
                            <p style={my_address.length == 0 ? {display: ''} : {display: 'none'}}>
                                Сейчас нет закреплённых за вами адресов, вы можете выбрать их ниже из зарегестрированных адресов.</p>
                            <table className="table simple" style={my_address.length != 0 ? {display: ''} : {display: 'none'}}>
                                <tbody>
                                    {my_address.map(function(elem, index) {
                                        return (
                                            <tr key={elem}>
                                                <td>{elem}
                                                    <a className="btn simple green_h_t right" 
                                                        onClick={selectAddress.bind(self, elem)}>Выбрать</a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                        <div className={this.state.master_step == 1 ? "tab-pane active" : "tab-pane"}>
                            <div className="textarea-i-wrapper">
                                <textarea onChange={this.bindReason} 
                                        value={this.state.reason}/>
                            </div>
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 0)}>Назад</a>
                                <a className="btn green right" onClick={this.setReason} 
                                    style={this.state.reason != "" ? {display: ''} : {display: 'none'}}>Дальше</a>
                            </div>
                        </div>
                        <div className={this.state.master_step == 2 ? "tab-pane active" : "tab-pane"}>
                            <table className="table simple">
                                <tbody>
                                    {this.state.result_type.map(function(elem, index) {
                                        return (
                                            <tr key={elem}>
                                                <td>{elem}
                                                    <a className="btn simple green_h_t right" 
                                                        onClick={setContract.bind(self, index)}>Выбрать</a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                            <div className="master-footer">
                                <a className="btn orange left" onClick={this.stepMaster.bind(self, 1)}>Назад</a>
                            </div>
                        </div>
                        <div className={this.state.master_step == 3 ? "tab-pane active" : "tab-pane"}>
                            <SeeOrderMaster/>
                        </div>
                    </div>
                </div>  
                <div className="panel-group type-two accordion" style={this.state.master_step == 0 ? {display: ''} : {display: 'none'}}>
                    <div className="panel-heading">
                        <h4 className="panel-title">
                            <a onClick={this.toogle_all_address} className={this.state.view_all_address == true ? 'collapsed' : ''}>
                                Все зарегестрированные адреса</a>
                        </h4>
                    </div>
                    <div className="panel-collapse" style={this.state.view_all_address == true ? {display: ''} : {display: 'none'}}>
                        <p style={all_address.length == 0 ? {display: ''} : {display: 'none'}}>
                            У вас нет зарегестрированных адресов, обратитесь к вашему менеджеру.</p>
                        <table className="table simple" style={all_address.length != 0 ? {display: ''} : {display: 'none'}}>
                            <tbody>
                                {all_address.map(function(elem, index) {
                                    return (
                                        <tr key={elem}>
                                            <td>{elem}
                                                <a className="btn simple green_h_t right" 
                                                    style={my_address.indexOf(elem) == -1 ? {display: ''} : {display: 'none'}}
                                                    onClick={addAddress.bind(self, elem)}>Добаить в мои адреса</a>
                                                <a className="btn simple red_t right" 
                                                    style={my_address.indexOf(elem) != -1 ? {display: ''} : {display: 'none'}}
                                                    onClick={deleteAddress.bind(self, elem)}>Удалить из моих адресов</a>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>    
            </div>     
        )
    }
});


// Компонет вложенный но выполнен выделенно!
SeeOrderMaster = React.createClass({
    getInitialState: function() {
        return {master_step: 0,
                order: new model.order(),
                save: null};
    },
    setForma: function(name, val) {
        this.state.order.primary[name] =  val;
        this.forceUpdate();
    },
    backStep: function() {
        new_order_ctrl.backStep();
    },
    saveOrder: function() {
        var self = this;
        switch(this.state.order.primary.type) {
            case "Техническое обслуживание":
                this.state.order.system.master_key = "e5179603b02e6bf44ffae3ea28da98e4ee1531c0";
                this.state.order.master.fio = "Литаш Сергей Владимирович";
                root.send_mail("9219505184@mail.ru", "Новая заявка");
                    break;

            case "Вентиляция и кондиционирование":
                this.state.order.system.master_key = "4d37fc39389b69a467dca02f221e15841104e3a2";
                this.state.order.master.fio = "Бондарь Виталий Алексеевич";
                root.send_mail("9312323303@mail.ru", "Новая заявка");
                    break;

            case "Система охраны и безопасности":
                this.state.order.system.master_key = "5b9083781aeee485eda10d935ee0a38c1b6d4e92";
                this.state.order.master.fio = "Волчик Игорь Ярославович";
                root.send_mail("9213005737@mail.ru", "Новая заявка");
                    break;
        }
        new_order_ctrl.add_order(this.state.order, function(data) {
            console.log(data);
            if(data == "1") {
                self.succesSave();
            } else {
                self.errorSave();
            }
        });
    },
    succesSave: function() {
        this.setState({save: true});
        order_ctrl.load();
    },
    errorSave: function() {
        this.setState({save: false});
    },
    resetMaster: function() {
        new_order_ctrl.resetMaster();
        this.setState({master_step: 0,
                        order: new model.order(),
                        save: null});
    },
    componentDidMount: function() {
        new_order_ctrl.setForma = this.setForma;
    },
    render: function() {
        var order = this.state.order;
        return (
                <div>
                    <div className="form-see" style={this.state.save == null ? {display: ''} : {display: 'none'}}>
                        <span className="grey-title">Заказчик</span>
                        <ul>
                            <li>{order.primary.company}</li>
                            <li>{order.primary.persone}</li>
                            <li>{order.primary.phone}</li>
                        </ul>
                        <div className="separator"></div>
                        <span className="grey-title">Заявка</span>
                        <ul>
                            <li>№ {order.system.key}</li>
                            <li>{order.primary.contract}</li>
                            <li>{order.primary.address}</li>
                            <li>{order.primary.type}</li>
                            <li className="blue_l_t">{order.primary.reason}</li>
                        </ul>
                        <div className="separator"></div> 
                        <div className="master-footer">
                            <a className="btn orange left" onClick={this.backStep}>Назад</a>
                            <a className="btn green right" onClick={this.saveOrder}>Отправить</a>
                        </div>
                    </div>
                    <div style={this.state.save == true ? {display: ''} : {display: 'none'}}>
                        <p>Заявка успешно отправлена!</p>
                        <div className="master-footer">
                            <a className="btn green right" onClick={this.resetMaster}>В начало</a>
                        </div>
                    </div>
                    <div style={this.state.save == false ? {display: ''} : {display: 'none'}}>
                        <p>Внимание заявка НЕ ОТПРАВЛЕНА!</p>
                    </div>
                </div>
        )
    }
});