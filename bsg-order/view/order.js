Order = React.createClass({
    componentDidMount: function() {
        /*$('#sidebar-exp').click();*/
    },
    render: function() {
        return (
            <div className="wrapper-content">
                <div className="row">
                    <div className="col-md-3">
                        <OrderMenu/>
                    </div>
                    <div className="col-md-9">
                        <OrderList/>
                    </div>
                </div>
            </div>    
        )
    }
});



OrderMenu = React.createClass({
    getInitialState: function() {
        return {filter: "all"};
    },
    setFilter: function(val) {
        this.setState({filter: val});
        order_ctrl.update(val);
    },
    componentDidMount: function() {
        
    },
    render: function() {
        var self = this;
        return (
            <div className="panel-style space custom-menu no-pad-r m-bot-30 m-top-0">
                <span className="grey-title">ФИЛЬТР</span>
                <ul>
                    <li>
                        <a onClick={this.setFilter.bind(self, "process")} 
                            className={this.state.filter == "process" ? "pause filter_active" : "pause"}>
                            <span>В обработке</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "job")}
                            className={this.state.filter == "job" ? "car filter_active" : "car"}>
                            <span>В работе</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "ок")}
                            className={this.state.filter == "ок" ? "ckeck filter_active" : "ckeck"}>
                            <span>Выполненые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "reject")}
                            className={this.state.filter == "reject" ? "attention filter_active" : "attention"}>
                            <span>Отклонённые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "close")}
                            className={this.state.filter == "close" ? "close_0 filter_active" : "close_0"}>
                            <span>Закрытые</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                    <li>
                        <a onClick={this.setFilter.bind(self, "all")}
                            className={this.state.filter == "all" ? "all filter_active" : "all"}>
                            <span>Все</span>
                            <span className="focus-custom-menu"></span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
});


OrderList = React.createClass({
    getInitialState: function() {
        return {data: order_ctrl.store,
                request: jQuery.extend(true, {}, order_ctrl.default_request_data),
                display: jQuery.extend(true, {}, order_ctrl.display_step),
                order_status: order_status};
    },
    update: function(request, data, display) {
        this.setState({data: data, request: request, display: display});
    },
    limitSet: function(event) {
        this.state.request[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    limitChange: function() {
        order_ctrl.update("change_limit");
    },
    stepPrev: function() {
        order_ctrl.update("step_prev");
    },
    stepNext: function() {
        order_ctrl.update("step_next");
    },
    add: function() {
        
    },
    edit: function(elem) {
        order_ctrl.edit_order(elem);
    },
    orderClose: function(elem) {
        elem.doc.primary.status = 4;
        // Запись в историю
        system_event[4].date = format.date_ms();
        elem.doc.system_history.push(system_event[4]);
        this.edit(elem);
    },
    componentDidMount: function() {
        order_ctrl.setViewList = this.update;
        order_ctrl.request_data = this.state.request;
    },
    render: function() {
        var edit = this.edit, orderClose = this.orderClose, self = this;
        return (
            <div className="panel-style space m-top-0">
                <h3 className="heading-title">Заявки {this.state.request.offset} - {(this.state.request.offset*1) + (this.state.request.limit*1)} из {this.state.data.count}
                    <a className="btn blue right" href="#/new_order" onClick={this.add}>Добавить</a>
                </h3>
                <table className="table simple">
                    <thead>
                        <tr>
                            <th>Дата заявки</th>
                            <th>Номер заявки</th>
                            <th>Содержание</th>
                            <th>Статус</th>
                            <th>Действие</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.data.map(function(elem) {
                            return (
                                <tr key={elem.id}>
                                    <td>{format.sql_to_date_time(elem.doc.system.add_date)}</td>
                                    <td>{elem.doc.system.key}</td>
                                    <td>{elem.doc.primary.reason}</td>
                                    <td>{self.state.order_status[elem.doc.primary.status]}</td>
                                    <td><a className="btn simple green_h_t right" style={elem.doc.primary.status == 2 ? {display: ''} : {display: 'none'}}
                                        onClick={orderClose.bind(self, elem)}>Закрыть</a></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="table-footer">
                    <div className="wrap-input-button input-f-wrapper w170 left tcenter">
                        <input className="input-button"
                                type="text" 
                                name="limit"
                                onChange={this.limitSet} 
                                value={this.state.request.limit}/>
                        <a className="btn default" onClick={this.limitChange}>Показать</a>
                    </div>
                    <div className="table-step-div">
                        <a className="btn default middle left"
                            style={{display: this.state.display.prev_button}}
                            onClick={this.stepPrev}>Назад</a>
                        <a className="btn default middle right"
                            style={{display: this.state.display.next_button}}
                            onClick={this.stepNext}>Вперёд</a>
                    </div>
                </div>
            </div> 
        )
    }
});


/*MainUsersForm = React.createClass({
    getInitialState: function() {
        return {doc: new model.main_user(),
                header: "Новый пользователь",
                type: "add",
                id: "",
                delButton: "none",
                role: select.role};
    },
    inputBind: function(event) {
        this.state.doc[event.target.name] = event.target.value;
        this.forceUpdate();
    },
    selectBind: function(name, val) {
        console.log(val);
        this.state.doc[name] = val;
        this.forceUpdate();
    },
    save: function() {
        switch(this.state.type) {
            case "add":
                main_user_ctrl.add(this.state.doc); 
                    break;
            case "edit":
                main_user_ctrl.edit(this.state.doc, this.state.id); 
                    break;  
        }
    },
    addForm: function() {
        this.setState({doc: new model.main_user(), 
                        header: "Новый пользователь", 
                        type: "add",
                        delButton: "none"});
        $('#MainUsersModal').modal('show');
    },
    editForm: function(elem) {
        this.setState({doc: elem.doc, 
                        id: elem.id, 
                        header: "Редактирование пользователя", 
                        type: "edit",
                        delButton: ""});
        $('#MainUsersModal').modal('show');
    },
    del: function() {
        main_user_ctrl.del(this.state.id);
    },
    componentDidMount: function() {
        main_user_ctrl.addForm = this.addForm;
        main_user_ctrl.editForm = this.editForm;
    },
    render: function() {
        return (
            <div id="MainUsersModal" className="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog">
                <div className="modal-dialog w700">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                            <h4 id="myModalLabel" className="modal-title">{this.state.header}</h4>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputEng name="login"
                                                head="Логин"
                                                bind={this.inputBind}
                                                val={this.state.doc.login}/>
                                    <div className="input-f-wrapper">
                                        <label>ФИО</label>
                                        <input type="text" 
                                                name="fio"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.fio}/>
                                    </div>
                                    <div className="input-f-wrapper">
                                        <label>Роль</label>
                                            <InputSelect name="role"
                                                        val={this.state.doc.role}
                                                        bind={this.selectBind}
                                                        data={this.state.role}/>
                                    </div>
                                    
                                </div>
                                <div className="col-md-6">   
                                    <InputPhone name="phone"
                                                head="Телефон"
                                                bind={this.inputBind}
                                                val={this.state.doc.phone}/>              
                                    <div className="input-f-wrapper">
                                        <label>Почта</label>
                                        <input type="text" 
                                                name="mail"
                                                onChange={this.inputBind} 
                                                value={this.state.doc.mail}/>
                                    </div>
                                    <InputEng name="pass"
                                                head="Логин"
                                                bind={this.inputBind}
                                                val={this.state.doc.pass}/>
                                    
                                </div>  
                            </div>    
                        </div>
                        <div className="modal-footer">
                            <button className="btn red left" 
                                style={{display: this.state.delButton}} 
                                onClick={this.del} type="button">Удалить</button>
                            <button className="btn default" data-dismiss="modal" type="button">Отмена</button>
                            <button className="btn green" onClick={this.save} type="button">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});*/

