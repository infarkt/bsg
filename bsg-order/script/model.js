var model = {
    users: function() {
        this.key = "",
        this.login = "",
        this.fio = "",
        this.post = "",
        this.phone = "",
        this.mail = "",
        this.pass = "",
        this.my_address = []
    },
    order: function() {
        this.system = {
            key: root.company_index + "/" + Number(root.user_index + 1) + "/" + Number(order_ctrl.data_count.my_count + 1),
            company_key: root.company.primary.key,
            master_key: "",
            persone_key: root.company.users[root.user_index].key,
            add_date: format.date_ms(),
        },
        this.primary = {
            status: 0,
            company: root.company.primary.name,
            persone: root.company.users[root.user_index].fio,
            phone: root.company.users[root.user_index].phone,
            mail: root.company.users[root.user_index].mail,
            contract: "",
            address: "",
            coords: "",
            type: "",
            reason: ""
        },
        this.master = {
            fio: "",
            master_note: ""
        },        
        this.system_history = [{
            date: format.date_ms(),
            name: "Создана заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        }]
    }
};

function set_system_event() {
    system_event = [
        {
            date: "",
            name: "Переведена в Новая заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Принята в работу заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Помечена выполненой, заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Отклонена заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        },{
            date: "",
            name: "Закрыта заказчиком " + root.company.users[root.user_index].fio,
            alert: "",
            see: true
        },
    ]
}

var order_type = ["Техническое обслуживание",
                    "Вентиляция и кондиционирование",
                    "Система охраны и безопасности"]

var order_status = ["В обработке","В работе","Выполнена","Отклонёна","Закрыта"];