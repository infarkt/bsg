var root = {
	user_index: "",
	company_index: "",
	company: {},
	orders: [],
	options: {
		limit: 10,
		update_time_order: 300000, // 300000 - 5 минут
	},
	load_data: {
		order: false,
	},
	set_company: function(data) {
		this.company = data;
		localStorage.setItem('key', this.company.users[this.user_index].key);
		order_ctrl.default_request_data.user_key = this.company.users[this.user_index].key;
		order_ctrl.default_request_data.company_key = this.company.primary.key;
		set_system_event();
		this.app_entry();
	},
	app_entry: function() {
        this.pre_load();
	},
	key: function(val) {
		var key = CryptoJS.MD5(val);
        return key.toString(CryptoJS.enc.Base64);
	},
	send: function(url, data, callback) {
		$.ajax({
		  	type: "POST",
		  	url: '../server/' + url + '.php',
		  	dataType: "json",
		  	data: JSON.stringify(data), 
		  	success: callback
		});
	},
	send_mail: function(mail, content) {
		var data = {
			mail: mail,
			content: content
		}
		$.ajax({
		  	type: "POST",
		  	url: '../server/send_mail.php',
		  	dataType: "json",
		  	data: JSON.stringify(data)
		});
	},
	pre_load: function() {
		order_ctrl.load("load");
	},
	load: function() {
		if(this.load_data.order == true) {
			start_router();
		}
	},
	out_user: function() {
		localStorage.removeItem('key');
		window.location.reload();
	},
	edit_company: function(data, callback) {
		this.send("edit_company", data, callback);
	},
	
	genPass: function() {
    	var length = 8;
        var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var retVal = "";
	    for (var i = 0, n = charset.length; i < length; ++i) {
	        retVal += charset.charAt(Math.floor(Math.random() * n));
	    }
    	return retVal;
	},
	add_doc: function(name, doc, callback) {
		var data = {
			name: name,
			doc: doc
		}
		this.send("add_doc", data, callback);
	},
	edit_doc: function(name, elem, callback) {
		var data = {
			name: name,
			elem: elem
		}
		this.send("edit_doc", data, callback);
	},
	delete_doc: function(name, id, callback) {
		var data = {
			name: name,
			id: id
		}
		this.send("delete_doc", data, callback);
	},
	get_collection: function(data, callback) {
		this.send("get_collection", data, callback);
	},
	get_orders: function(data, callback) {
		this.send("get_order", data, callback);
	}, 
}