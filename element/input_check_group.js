// Поле чек
InputCheck = React.createClass({
    click: function() {
        var val = this.props.val == "" ? "checked" : "";
        this.props.bind(this.props.num_check, val, this.props.head);
    },
    render: function() {
        return (
            <li className="">
                <div className="clearfix prettycheckbox labelright blue">
                    <a onClick={this.click} className={this.props.val}></a>
                </div>
                <span>{this.props.head}</span>
            </li>
        );
    }
});


/*<div className="input-f-wrapper">
                <label>{this.props.head}</label>
                <input type="text" 
                        name={this.props.name}
                        onChange={this.change} 
                        value={this.props.val}/>
            </div>*/