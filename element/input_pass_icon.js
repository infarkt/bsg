// Поле пароля с иконкой
InputPassIcon = React.createClass({
    render: function() {
        return (
            <div className="field">
                <div className="ui icon input">
                    <input type="password"  
                        value={this.props.val}
                        name={this.props.name}
                        placeholder="Пароль"
                        onChange={this.props.bind}/>
                    <i className="lock icon"></i>
                </div>
            </div>
        );
    }
});

// Применение
/*<InputPassIcon name="pass" 
    val={this.state.pass}
    bind={this.inputBind}/>*/