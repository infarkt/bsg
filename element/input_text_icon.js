// Поле тестовое с иконкой
InputTextIcon = React.createClass({
    render: function() {
        return (
            <div className="field">
                <div className="ui icon input">
                    <input type="text"  
                        value={this.props.val}
                        name={this.props.name}
                        placeholder={this.props.ph}
                        onChange={this.props.bind}/>
                    <i className={this.props.icon}></i>
                </div>
            </div>
        );
    }
});

// Применение
/*<InputTextIcon name="login" 
    ph="Логин" 
    val={this.state.login}
    bind={this.inputBind} 
    icon="user icon"/>*/